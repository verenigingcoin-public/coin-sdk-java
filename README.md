# COIN SDKs for Java

[![CI Status](https://gitlab.com/verenigingcoin-public/coin-sdk-java/badges/master/pipeline.svg)](https://gitlab.com/verenigingcoin-public/coin-sdk-java/-/pipelines/latest)

| Api                                                                   | SDK Version                     | Api Version                                          | Changelog                          |
|-----------------------------------------------------------------------|---------------------------------|------------------------------------------------------|------------------------------------|
| [number-portability](https://coin.nl/en/services/nummerportabiliteit) | 3.0.0 +<br/>2.0.0 +<br/>1.4.0 + | [v3](https://api.coin.nl/docs/number-portability/v3) | [here](CHANGELOG.md#version-1.5.0) |
|                                                                       | 0.0.2 - 1.3.1                   | [v1](https://api.coin.nl/docs/number-portability/v1) | -                                  |
| [bundle-switching](https://coin.nl/en/services/overstappen)           | 3.0.0 +<br/>2.0.0 +<br/>1.6.0 + | [v5](https://api.coin.nl/docs/bundle-switching/v5)   | [here](CHANGELOG.md#version-1.6.0) |
|                                                                       | 0.0.3 - 1.5.1                   | [v4](https://api.coin.nl/docs/bundle-switching/v4)   | -                                  |
| weas-migration                                                        | 3.1.3 +                         | [v1](https://api.coin.nl/docs/weas-migration/v1)     | -                                  |
| mobile-connect                                                        | 3.2.0 +                         | [v3](https://api.coin.nl/docs/mobile-connect/v3)     | -                                  |

This project contains SDKs for various COIN APIs.
- [Number Portability](number-portability-sdk/README.md)
- [Bundle Switching](bundle-switching-sdk/README.md)
- [WEAS Migration](weas-migration-sdk/README.md)
- [Mobile Connect](mobile-connect-sdk/README.md)
- For other APIs you can use [Common](common-sdk/README.md) to add correct credentials to your requests

To use an SDK, you need to [configure a consumer](https://gitlab.com/verenigingcoin-public/consumer-configuration/-/blob/master/README.md).

## Support
If you need support, feel free to send an email to the [COIN devops team](mailto:devops@coin.nl).
