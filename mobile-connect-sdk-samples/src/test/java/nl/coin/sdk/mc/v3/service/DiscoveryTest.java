package nl.coin.sdk.mc.v3.service;

import nl.coin.sdk.mc.v3.MobileConnectSamplesApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {MobileConnectSamplesApplication.class})
public class DiscoveryTest {

    @Autowired
    private IMobileConnectService mobileConnectService;

    @Test
    public void sendDiscoveryRequest() {
        var response = mobileConnectService.sendDiscoveryRequest("31610101010");
        System.out.println(response);
    }
}
