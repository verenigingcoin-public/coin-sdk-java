package nl.coin.sdk.mc.v3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
public class MobileConnectSamplesApplication {

    private static final Logger LOG = LoggerFactory.getLogger(MobileConnectSamplesApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(MobileConnectSamplesApplication.class, args);
        LOG.debug("Starting Mobile Connect SDK Samples Project");
    }
}

@Configuration
@ImportAutoConfiguration({MobileConnectAutoConfiguration.class})
class MobileConnectSamplesConfiguration {
}
