package nl.coin.sdk.common.crypto;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.Base64;

/**
 * HMAC SHA1 signer
 */
public class HmacSha256Signer {
  private static final String HMAC_SHA_ALGORITHM = "HMacSHA256";
  private final Mac signer;

  public HmacSha256Signer(@NotNull String key) {
    try {
      SecretKey sk = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), HMAC_SHA_ALGORITHM);
      signer = Mac.getInstance(HMAC_SHA_ALGORITHM);
      signer.init(sk);
    } catch (NoSuchAlgorithmException | InvalidKeyException e) {
      throw new IllegalArgumentException(e);
    }
  }

  public String sign(@NotNull String data) {
    byte[] signature;
    byte[] bytes = data.getBytes(StandardCharsets.UTF_8);
    try {
      Mac clone = (Mac)signer.clone();
      signature = clone.doFinal(bytes);
    } catch (CloneNotSupportedException e) {
      synchronized(this) {
        signature = signer.doFinal(bytes);
      }
    }
    return new String(Base64.getEncoder().encode(signature));
  }

  public static HmacSha256Signer fromEncryptedBase64EncodedSecretFile(@NotNull String filename, @NotNull PrivateKey privateKey) {
    try {
      String encryptedBase64EncodedHmacSecret = FileUtils.readFileToString(new File(filename), StandardCharsets.US_ASCII).trim();
      return new HmacSha256Signer(decryptedHmacSecret(encryptedBase64EncodedHmacSecret, privateKey));
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  public static HmacSha256Signer fromEncryptedBase64EncodedSecret(@NotNull InputStream is, @NotNull PrivateKey privateKey) {
    try {
      String encryptedBase64EncodedHmacSecret = IOUtils.toString(is, StandardCharsets.US_ASCII).trim();
      return new HmacSha256Signer(decryptedHmacSecret(encryptedBase64EncodedHmacSecret, privateKey));
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }


  /**
   * decrypts the encrypted sharedkey from `filename` using `privateKey`
   */
  private static String decryptedHmacSecret(String encryptedBase64EncodedHmacSecret, PrivateKey privateKey) {

    try {
      byte[] encryptedSharedKey = Base64.getDecoder().decode(encryptedBase64EncodedHmacSecret);
      Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
      cipher.init(Cipher.DECRYPT_MODE, privateKey);
      return new String(cipher.doFinal(encryptedSharedKey), StandardCharsets.US_ASCII);
    } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException e) {
      throw new IllegalArgumentException(e);
    }
  }
}
