package nl.coin.sdk.common.client;

public interface IOffsetPersister {
    void persistOffset(String offset);
    String getPersistedOffset();
}
