package nl.coin.sdk.common.crypto;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.interfaces.RSAPrivateKey;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CtpApiClientUtil {

    public static final int DEFAULT_VALID_PERIOD_IN_SECS = 30;

    private CtpApiClientUtil() {
    }

    public enum HmacSignatureType {
        DATE,
        X_DATE_AND_DIGEST
    }

    private static final Pattern REQUEST_LINE_PATTERN = Pattern.compile("^([A-Z][A-Z]* )[^:]*://[^/]*", Pattern.DOTALL);
    private static final String HMAC_HEADER_FORMAT = "hmac username=\"%s\", algorithm=\"hmac-sha256\", headers=\"%s request-line\", signature=\"%s\"";


    /**
     * adds the JWT token as Bearer token in the Authorization header.
     * the token is signed with the private key and contains both the
     * issuer (iss), expiration time (exp) and not-before (nbf) time claims.
     */
    public static String createJWT(@NotNull PrivateKey privateKey, @NotNull String consumerName, int validPeriodInSeconds) {
        Calendar cal = Calendar.getInstance();
        Algorithm algorithm = Algorithm.RSA256(null, (RSAPrivateKey) privateKey);

        cal.add(Calendar.SECOND, -30);
        Date nbf = cal.getTime();

        cal.add(Calendar.SECOND, 30 + validPeriodInSeconds);
        Date exp = cal.getTime();

        return JWT.create()
                .withIssuer(consumerName)
                .withExpiresAt(exp)
                .withNotBefore(nbf)
                .sign(algorithm);
    }

    /**
     * The date in HMAC digest should be in RFC1123 format
     */
    public static String getCurrentDateInHttpHeaderFormat() {
        return DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss O").withLocale(Locale.ENGLISH).format(ZonedDateTime.now(ZoneOffset.UTC));
    }

    private static String generateHmacMessage(Map<String, String> headers, String requestLine) {
        Optional<String> messageOpt = headers.entrySet().stream().map(e -> e.getKey() + ": " + e.getValue()).reduce((a, b) -> a + "\n" + b);
        assert (messageOpt.isPresent());
        return messageOpt.get() + "\n" + formatRequestLine(requestLine);
    }

    public static String calculateHttpRequestHmac(@NotNull HmacSha256Signer signer, @NotNull String consumerName, @NotNull Map<String, String> headers, @NotNull String requestLine) {
        String message = generateHmacMessage(headers, requestLine);
        String signature = signer.sign(message);

        Optional<String> headersParamOpt = headers.keySet().stream().reduce((a, b) -> a + " " + b);
        assert (headersParamOpt.isPresent());
        return String.format(HMAC_HEADER_FORMAT, consumerName, headersParamOpt.get(), signature);
    }

    /**
     * Remove the protocol://<host> prefix from the request line. That is not passed to the server
     */
    private static String formatRequestLine(String requestLine) {
        Matcher matcher = REQUEST_LINE_PATTERN.matcher(requestLine);
        if (matcher.find()) {
            return matcher.replaceAll(matcher.group(1));
        }
        return requestLine;

    }

    /**
     * reads the RSA key pair from disk
     *
     * @param file containing the private key
     * @return the PrivateKey contained.
     */
    public static PrivateKey readPrivateKeyFile(@NotNull File file) {
        try {
            return readPrivateKey(new FileInputStream(file));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * reads the RSA key pair from string
     *
     * @param input is string containing the private key
     * @return the PrivateKey contained.
     */
    public static PrivateKey readPrivateKey(@NotNull String input) {
        return readPrivateKey(new ByteArrayInputStream(input.getBytes()));
    }

    /**
     * reads the RSA key pair from input stream
     *
     * @param is InputStream containing the private key
     * @return the PrivateKey contained.
     */
    public static PrivateKey readPrivateKey(@NotNull InputStream is) {
        PrivateKeyInfo pKeyInfo = readPrivateKeyInfo(is);
        try {
            if (Security.getProvider("BC") == null) {
                Security.addProvider(new BouncyCastleProvider());
            }
            return new JcaPEMKeyConverter().setProvider("BC").getPrivateKey(pKeyInfo);
        } catch (IOException e) {
            throw new IllegalArgumentException("Failed to load private key");
        }
    }

    private static PrivateKeyInfo readPrivateKeyInfo(InputStream is) {
        try {
            if (Security.getProvider("BC") == null) {
                Security.addProvider(new BouncyCastleProvider());
            }

            PEMParser parser = new PEMParser(new InputStreamReader(is));
            Object parsed = parser.readObject();
            if (parsed instanceof PEMKeyPair)
                return ((PEMKeyPair) parsed).getPrivateKeyInfo();
            if (parsed instanceof PrivateKeyInfo)
                return (PrivateKeyInfo) parsed;

            throw new IllegalArgumentException("Unsupported key format");
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static Map<String, String> getHmacHeaders(@NotNull HmacSignatureType hmacSignatureType, String body) {
        return getHmacHeaders(hmacSignatureType, body != null ? body.getBytes() : new byte[0]);
    }

    public static Map<String, String> getHmacHeaders(@NotNull HmacSignatureType hmacSignatureType, byte [] body) {
        Map<String, String> hmacHeaders = new HashMap<>();
        switch (hmacSignatureType) {
            case DATE:
                hmacHeaders.put("date", CtpApiClientUtil.getCurrentDateInHttpHeaderFormat());
                break;
            case X_DATE_AND_DIGEST:
                hmacHeaders.put("x-date", CtpApiClientUtil.getCurrentDateInHttpHeaderFormat());
                hmacHeaders.put("digest", CtpApiClientUtil.digestBody(body != null ? body : new byte[0]));
                break;
        }
        return hmacHeaders;
    }


    /**
     * digests the bytes in the input stream to a SHA-256 digest
     *
     * @param stream containing the body
     * @return the digest as string
     */
    public static String digestBody(@NotNull InputStream stream) throws IOException {
        return digestBody(IOUtils.toByteArray(stream));
    }

    /**
     * digests the byte to a SHA-256 digest
     *
     * @param body containing the body
     * @return the digest as string
     */
    public static String digestBody(byte[] body) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(body);
            return "SHA-256=" + new String(Base64.getEncoder().encode(hash));
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
