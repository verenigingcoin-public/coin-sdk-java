package nl.coin.sdk.common;

public class SdkInfo {
    // NOTE: this version is maintained by the makefile, do not modify or move to another file
    public static final String userAgent = "coin-sdk-java-3.2.2-SNAPSHOT";
}
