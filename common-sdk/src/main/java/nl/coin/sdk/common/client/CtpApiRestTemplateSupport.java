package nl.coin.sdk.common.client;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.coin.sdk.common.SdkInfo;
import nl.coin.sdk.common.crypto.CtpApiClientUtil;
import nl.coin.sdk.common.crypto.HmacSha256Signer;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URI;
import java.security.PrivateKey;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class CtpApiRestTemplateSupport extends AbstractCtpApiSupport {

    protected final ObjectMapper mapper;

    protected final RestTemplate restTemplate = new RestTemplate(new BufferingClientHttpRequestFactory(createRestTemplateFactory()));
    private static final Logger LOG = LoggerFactory.getLogger(CtpApiRestTemplateSupport.class);

    public static SimpleClientHttpRequestFactory createRestTemplateFactory() {
        Optional<String> proxyHost = getEnv("HTTP_PROXY_HOST").or(() -> getEnv("HTTPS_PROXY_HOST"));
        Optional<String> proxyPort = getEnv("HTTP_PROXY_PORT");
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        if (proxyHost.isPresent() && proxyPort.isPresent()) {
            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost.get(), proxyPort.map(Integer::parseInt).get()));
            requestFactory.setProxy(proxy);
        } else {
            requestFactory.setProxy(Proxy.NO_PROXY);
        }

        return requestFactory;
    }

    private static Optional<String> getEnv(String name) {
        return Optional.ofNullable(System.getenv(name));
    }

    public CtpApiRestTemplateSupport(@NotNull String consumerName, @NotNull HmacSha256Signer signer, @NotNull PrivateKey privateKey, int validPeriodInSeconds, @NotNull CtpApiClientUtil.HmacSignatureType hmacSignatureType) {
        this(consumerName, signer, privateKey, validPeriodInSeconds, hmacSignatureType, new ObjectMapper());
    }

    public CtpApiRestTemplateSupport(@NotNull String consumerName, @NotNull HmacSha256Signer signer, @NotNull PrivateKey privateKey, @NotNull CtpApiClientUtil.HmacSignatureType hmacSignatureType) {
        this(consumerName, signer, privateKey, hmacSignatureType, new ObjectMapper());
    }

    public CtpApiRestTemplateSupport(@NotNull String consumerName, @NotNull HmacSha256Signer signer, @NotNull PrivateKey privateKey) {
        this(consumerName, signer, privateKey, new ObjectMapper());
    }

    public CtpApiRestTemplateSupport(@NotNull String consumerName, @NotNull HmacSha256Signer signer, @NotNull PrivateKey privateKey, int validPeriodInSeconds, @NotNull CtpApiClientUtil.HmacSignatureType hmacSignatureType, @NotNull ObjectMapper mapper) {
        super(consumerName, signer, privateKey, validPeriodInSeconds, hmacSignatureType);
        this.mapper = mapper;
        initRestTemplate(restTemplate);
    }

    public CtpApiRestTemplateSupport(@NotNull String consumerName, @NotNull HmacSha256Signer signer, @NotNull PrivateKey privateKey, @NotNull CtpApiClientUtil.HmacSignatureType hmacSignatureType, @NotNull ObjectMapper mapper) {
        super(consumerName, signer, privateKey, hmacSignatureType);
        this.mapper = mapper;
        initRestTemplate(restTemplate);
    }

    public CtpApiRestTemplateSupport(@NotNull String consumerName, @NotNull HmacSha256Signer signer, @NotNull PrivateKey privateKey, @NotNull ObjectMapper mapper) {
        super(consumerName, signer, privateKey, CtpApiClientUtil.HmacSignatureType.X_DATE_AND_DIGEST);
        this.mapper = mapper;
        initRestTemplate(restTemplate);
    }

    private void initRestTemplate(RestTemplate restTemplate) {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        restTemplate.setRequestFactory(factory);
        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        mapper.setVisibility(PropertyAccessor.IS_GETTER, JsonAutoDetect.Visibility.NONE);

        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        mappingJackson2HttpMessageConverter.setPrettyPrint(false);
        mappingJackson2HttpMessageConverter.setObjectMapper(mapper);
        mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));

        StringHttpMessageConverter stringConverter = new StringHttpMessageConverter();
        stringConverter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));

        List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
        messageConverters.removeIf(m -> m.getClass().isAssignableFrom(MappingJackson2HttpMessageConverter.class));
        messageConverters.add(mappingJackson2HttpMessageConverter);
        messageConverters.add(stringConverter);
    }

    public <T> T sendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull String url) {
        return sendWithToken(outputType, method, url, null, createJwt());
    }

    public <T> T sendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull String url) {
        return sendWithToken(outputType, method, url, null, createJwt());
    }

    public <T, I> T sendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull String url, I request) {
        return sendWithToken(outputType, method, url, request, createJwt());
    }

    public <T, I> T sendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull String url, I request) {
        return sendWithToken(outputType, method, url, request, createJwt());
    }

    public <T> T sendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull String url, @NotNull String jwt) {
        return sendWithToken(outputType, method, new HttpHeaders(), url, null, jwt);
    }

    public <T> T sendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull String url, @NotNull String jwt) {
        return sendWithToken(outputType, method, new HttpHeaders(), url, null, jwt);
    }

    public <T, I> T sendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull String url, I request, @NotNull String jwt) {
        return sendWithToken(outputType, method, new HttpHeaders(), url, request, jwt);
    }

    public <T, I> T sendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull String url, I request, @NotNull String jwt) {
        return sendWithToken(outputType, method, new HttpHeaders(), url, request, jwt);
    }

    public <T> T sendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url) {
        return sendWithToken(outputType, method, headers, url, null, createJwt());
    }

    public <T> T sendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url) {
        return sendWithToken(outputType, method, headers, url, null, createJwt());
    }

    public <T, I> T sendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, I request) {
        return sendWithToken(outputType, method, headers, url, request, createJwt());
    }

    public <T, I> T sendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, I request) {
        return sendWithToken(outputType, method, headers, url, request, createJwt());
    }

    public <T> T sendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, @NotNull String jwt) {
        return doSendWithToken(outputType, method, headers, url, null, jwt).getBody();
    }

    public <T> T sendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, @NotNull String jwt) {
        return doSendWithToken(outputType, method, headers, url, null, jwt).getBody();
    }

    public <T, I> T sendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, I request, @NotNull String jwt) {
        return doSendWithToken(outputType, method, headers, url, request, jwt).getBody();
    }

    public <T, I> T sendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, I request, @NotNull String jwt) {
        return doSendWithToken(outputType, method, headers, url, request, jwt).getBody();
    }

    public <T> ResponseEntity<T> doSendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull String url) {
        return doSendWithToken(outputType, method, url, null, createJwt());
    }

    public <T> ResponseEntity<T> doSendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull String url) {
        return doSendWithToken(outputType, method, url, null, createJwt());
    }

    public <T, I> ResponseEntity<T> doSendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull String url, I request) {
        return doSendWithToken(outputType, method, url, request, createJwt());
    }

    public <T, I> ResponseEntity<T> doSendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull String url, I request) {
        return doSendWithToken(outputType, method, url, request, createJwt());
    }

    public <T> ResponseEntity<T> doSendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull String url, @NotNull String jwt) {
        return doSendWithToken(outputType, method, new HttpHeaders(), url, null, jwt);
    }

    public <T> ResponseEntity<T> doSendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull String url, @NotNull String jwt) {
        return doSendWithToken(outputType, method, new HttpHeaders(), url, null, jwt);
    }

    public <T, I> ResponseEntity<T> doSendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull String url, I request, @NotNull String jwt) {
        return doSendWithToken(outputType, method, new HttpHeaders(), url, request, jwt);
    }

    public <T, I> ResponseEntity<T> doSendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull String url, I request, @NotNull String jwt) {
        return doSendWithToken(outputType, method, new HttpHeaders(), url, request, jwt);
    }

    public <T> ResponseEntity<T> doSendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url) {
        return doSendWithToken(outputType, method, headers, url, null, createJwt());
    }

    public <T> ResponseEntity<T> doSendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url) {
        return doSendWithToken(outputType, method, headers, url, null, createJwt());
    }

    public <T, I> ResponseEntity<T> doSendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, I request) {
        return doSendWithToken(outputType, method, headers, url, request, createJwt());
    }

    public <T, I> ResponseEntity<T> doSendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, I request) {
        return doSendWithToken(outputType, method, headers, url, request, createJwt());
    }

    public <T, I> ResponseEntity<T> doSendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, @NotNull String jwt) {
        return doSendWithToken(outputType, method, headers, url, null, jwt);
    }

    public <T, I> ResponseEntity<T> doSendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, @NotNull String jwt) {
        return doSendWithToken(outputType, method, headers, url, null, jwt);
    }

    public <T, I> ResponseEntity<T> doSendWithToken(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, I request, @NotNull String jwt) {
        try {
            final byte[] bodyAsString = request == null ? null : mapper.writeValueAsBytes(request);
            return doSendWithTokenRaw(outputType, method, headers, url, bodyAsString, jwt);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public <T, I> ResponseEntity<T> doSendWithToken(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, I request, @NotNull String jwt) {
        try {
            final byte[] bodyAsString = request == null ? null : mapper.writeValueAsBytes(request);
            return doSendWithTokenRaw(outputType, method, headers, url, bodyAsString, jwt);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull String url) {
        return doSendWithTokenRaw(outputType, method, url, null, createJwt());
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull String url) {
        return doSendWithTokenRaw(outputType, method, url, null, createJwt());
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull String url, byte[] bodyAsString) {
        return doSendWithTokenRaw(outputType, method, url, bodyAsString, createJwt());
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull String url, byte[] bodyAsString) {
        return doSendWithTokenRaw(outputType, method, url, bodyAsString, createJwt());
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull String url, @NotNull String jwt) {
        return doSendWithTokenRaw(outputType, method, new HttpHeaders(), url, null, jwt);
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull String url, @NotNull String jwt) {
        return doSendWithTokenRaw(outputType, method, new HttpHeaders(), url, null, jwt);
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull String url, byte[] bodyAsString, @NotNull String jwt) {
        return doSendWithTokenRaw(outputType, method, new HttpHeaders(), url, bodyAsString, jwt);
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull String url, byte[] bodyAsString, @NotNull String jwt) {
        return doSendWithTokenRaw(outputType, method, new HttpHeaders(), url, bodyAsString, jwt);
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url) {
        return doSendWithTokenRaw(outputType, method, headers, url, null, createJwt());
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url) {
        return doSendWithTokenRaw(outputType, method, headers, url, null, createJwt());
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, byte[] bodyAsString) {
        return doSendWithTokenRaw(outputType, method, headers, url, bodyAsString, createJwt());
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, byte[] bodyAsString) {
        return doSendWithTokenRaw(outputType, method, headers, url, bodyAsString, createJwt());
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, @NotNull String jwt) {
        return doSendWithTokenRaw(outputType, method, headers, url, null, jwt);
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, @NotNull String jwt) {
        return doSendWithTokenRaw(outputType, method, headers, url, null, jwt);
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull Class<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, byte[] bodyAsString, @NotNull String jwt) {
        return doSendWithTokenRaw(method, headers, url, bodyAsString, jwt, (URI uri, HttpEntity<byte[]> entity) -> restTemplate.exchange(uri, method, entity, outputType));
    }

    public <T> ResponseEntity<T> doSendWithTokenRaw(@NotNull ParameterizedTypeReference<T> outputType, @NotNull HttpMethod method, @NotNull HttpHeaders headers, @NotNull String url, byte[] bodyAsString, @NotNull String jwt) {
        return doSendWithTokenRaw(method, headers, url, bodyAsString, jwt, (URI uri, HttpEntity<byte[]> entity) -> restTemplate.exchange(uri, method, entity, outputType));
    }

    private <T> ResponseEntity<T> doSendWithTokenRaw(HttpMethod method, HttpHeaders headers, String url, byte[] bodyAsString, String jwt, Exchange<T> exchange) {
        String requestLine = String.format("%s %s HTTP/1.1", method, url);

        Map<String, String> hmacHeaders = CtpApiClientUtil.getHmacHeaders(hmacSignatureType, bodyAsString);

        headers.add("authorization", CtpApiClientUtil.calculateHttpRequestHmac(signer, consumerName, hmacHeaders, requestLine));
        headers.add("Cookie", String.format("jwt=%s", jwt));
        headers.add("User-Agent", SdkInfo.userAgent);
        if (headers.getContentType() == null) headers.setContentType(MediaType.APPLICATION_JSON);
        if (headers.getAccept().isEmpty()) headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        hmacHeaders.forEach(headers::add);
        HttpEntity<byte[]> entity = bodyAsString == null ? new HttpEntity<>(headers) : new HttpEntity<>(bodyAsString, headers);
        try {
            return exchange.exchange(URI.create(url), entity);
        } catch (Exception exception) {
            byte[] body = entity.getBody();
            if (body == null) body = new byte[0];
            LOG.error(MessageFormat.format("exception:\n{0}\nrequest headers:\n{1}\nrequest body:\n{2}\n", exception.getMessage(), entity.getHeaders(), new String(body)));
            throw exception;
        }
    }

    @FunctionalInterface
    private interface Exchange<T> {
        ResponseEntity<T> exchange(URI uri, HttpEntity<byte[]> entity);
    }
}
