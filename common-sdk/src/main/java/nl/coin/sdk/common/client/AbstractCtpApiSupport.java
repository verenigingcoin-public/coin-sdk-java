package nl.coin.sdk.common.client;

import nl.coin.sdk.common.crypto.CtpApiClientUtil;
import nl.coin.sdk.common.crypto.HmacSha256Signer;
import org.jetbrains.annotations.NotNull;

import java.security.PrivateKey;

import static nl.coin.sdk.common.crypto.CtpApiClientUtil.DEFAULT_VALID_PERIOD_IN_SECS;

public abstract class AbstractCtpApiSupport {

    protected final HmacSha256Signer signer;
    protected final PrivateKey privateKey;
    protected final String consumerName;
    protected final int validPeriodInSeconds;
    protected final CtpApiClientUtil.HmacSignatureType hmacSignatureType;

    public AbstractCtpApiSupport(@NotNull String consumerName, @NotNull HmacSha256Signer signer, @NotNull PrivateKey privateKey, int validPeriodInSeconds, @NotNull CtpApiClientUtil.HmacSignatureType hmacSignatureType) {
        this.signer = signer;
        this.privateKey = privateKey;
        this.consumerName = consumerName;
        this.validPeriodInSeconds = validPeriodInSeconds;
        this.hmacSignatureType = hmacSignatureType;
    }

    public AbstractCtpApiSupport(@NotNull String consumerName, @NotNull HmacSha256Signer signer, @NotNull PrivateKey privateKey, @NotNull CtpApiClientUtil.HmacSignatureType hmacSignatureType) {
        this.signer = signer;
        this.privateKey = privateKey;
        this.consumerName = consumerName;
        this.validPeriodInSeconds = DEFAULT_VALID_PERIOD_IN_SECS;
        this.hmacSignatureType = hmacSignatureType;
    }

    public AbstractCtpApiSupport(@NotNull String consumerName, @NotNull HmacSha256Signer signer, @NotNull PrivateKey privateKey) {
        this(consumerName, signer, privateKey, CtpApiClientUtil.HmacSignatureType.X_DATE_AND_DIGEST);
    }

    public String createJwt() {
        return CtpApiClientUtil.createJWT(privateKey, consumerName, validPeriodInSeconds);
    }
}
