package nl.coin.sdk.common.client;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.StampedLock;

class SimpleLock {
    private final String name;

    SimpleLock(String name) {
        this.name = name;
    }

    private final StampedLock lock = new StampedLock();

    long acquire() {
        return acquire(30);
    }

    long acquireNow() {
        return acquire(0);
    }

    long acquire(long seconds) {
        try {
            long stamp = lock.tryWriteLock(seconds, TimeUnit.SECONDS);
            if (stamp == 0) {
                throw new RuntimeException("Could not acquire lock for " + name);
            } else return stamp;
        } catch (InterruptedException exception) {
            throw new RuntimeException("Thread interrupted while acquiring lock for " + name, exception);
        }
    }

    void tryRelease(long stamp) {
        try {
            lock.unlockWrite(stamp);
        } catch (IllegalMonitorStateException ignored) {
        }
    }

    void waitUntilUnlocked() {
        tryRelease(acquire());
    }

    boolean isLocked() {
        return lock.isWriteLocked();
    }
}
