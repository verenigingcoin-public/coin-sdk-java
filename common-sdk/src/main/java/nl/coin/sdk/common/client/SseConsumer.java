package nl.coin.sdk.common.client;

import jakarta.annotation.PreDestroy;
import nl.coin.sdk.common.SdkInfo;
import nl.coin.sdk.common.crypto.CtpApiClientUtil;
import nl.coin.sdk.common.crypto.HmacSha256Signer;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriBuilder;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;
import reactor.netty.http.client.HttpClient;
import reactor.util.annotation.Nullable;

import java.net.URI;
import java.security.PrivateKey;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntBinaryOperator;

@Service
public class SseConsumer {
    private static final Logger LOG = LoggerFactory.getLogger(SseConsumer.class);
    private static final String DEFAULT_OFFSET = "-1";

    protected final HmacSha256Signer signer;
    protected final PrivateKey privateKey;
    protected final String consumerName;
    private final String sseUri;
    private final Duration bufferTime;
    private final int bufferSize;
    private final int numberOfRetries;
    private final int backOffPeriod;
    private Disposable eventStreamSubscription;
    private final SimpleLock streamLock = new SimpleLock("stream consumption");
    private final SimpleLock processingLock = new SimpleLock("event processing");
    private Consumer<HttpHeaders> addHeaders = HttpHeaders -> {};
    private final TransactionTemplate transactionTemplate;
    private final WebClient.Builder webClientBuilder;

    public SseConsumer(
        @Value("${sse.url}") String sseUri,
        @Value("${consumer.name}") String consumerName,
        @Value("${stream.buffer.size:1}") int bufferSize,
        @Value("${stream.buffer.time:5s}") Duration bufferTime,
        @Value("${listener.numberOfRetries}") int numberOfRetries,
        @Value("${listener.backOffPeriod}") int backOffPeriod,
        HmacSha256Signer signer,
        PrivateKey privateKey,
        @Nullable
        TransactionTemplate transactionTemplate,
        WebClient.Builder webClientBuilder
    ) {
        this.signer = signer;
        this.privateKey = privateKey;
        this.consumerName = consumerName;
        this.sseUri = sseUri;
        this.bufferSize = bufferSize;
        this.bufferTime = bufferTime;
        this.numberOfRetries = numberOfRetries;
        this.backOffPeriod = backOffPeriod;
        this.transactionTemplate = transactionTemplate;
        this.webClientBuilder = webClientBuilder;
    }

    public void startConsumingUnconfirmed(@NotNull List<String> messageTypes, @NotNull MultiValueMap<String, String> params, @NotNull Function<List<ServerSentEvent<String>>, String> handleSSEs, @NotNull Runnable doAfterAllRetriesFailed) {
        startConsuming(ConfirmationStatus.UNCONFIRMED, DEFAULT_OFFSET, null, messageTypes, params, handleSSEs, doAfterAllRetriesFailed);
    }

    public void startConsumingUnconfirmedWithOffsetPersistence(IOffsetPersister offsetPersister, @NotNull List<String> messageTypes, @NotNull MultiValueMap<String, String> params, @NotNull Function<List<ServerSentEvent<String>>, String> handleSSEs, @NotNull Runnable doAfterAllRetriesFailed) {
        startConsumingUnconfirmedWithOffsetPersistence(offsetPersister != null ? offsetPersister.getPersistedOffset() : DEFAULT_OFFSET, offsetPersister, messageTypes, params, handleSSEs, doAfterAllRetriesFailed);
    }

    public void startConsumingUnconfirmedWithOffsetPersistence(String offset, IOffsetPersister offsetPersister, @NotNull List<String> messageTypes, @NotNull MultiValueMap<String, String> params, @NotNull Function<List<ServerSentEvent<String>>, String> handleSSEs, @NotNull Runnable doAfterAllRetriesFailed) {
        startConsuming(ConfirmationStatus.UNCONFIRMED, offset, offsetPersister, messageTypes, params, handleSSEs, doAfterAllRetriesFailed);
    }

    public void startConsumingAll(IOffsetPersister offsetPersister, @NotNull List<String> messageTypes, @NotNull MultiValueMap<String, String> params, @NotNull Function<List<ServerSentEvent<String>>, String> handleSSEs, @NotNull Runnable doAfterAllRetriesFailed) {
        startConsumingAll(offsetPersister != null ? offsetPersister.getPersistedOffset() : DEFAULT_OFFSET, offsetPersister, messageTypes, params, handleSSEs, doAfterAllRetriesFailed);
    }

    public void startConsumingAll(String offset, IOffsetPersister offsetPersister, @NotNull List<String> messageTypes, @NotNull MultiValueMap<String, String> params, @NotNull Function<List<ServerSentEvent<String>>, String> handleSSEs, @NotNull Runnable doAfterAllRetriesFailed) {
        startConsuming(ConfirmationStatus.ALL, offset, offsetPersister, messageTypes, params, handleSSEs, doAfterAllRetriesFailed);
    }

    @PreDestroy
    public void stopConsuming() {
        if (eventStreamSubscription != null && !eventStreamSubscription.isDisposed()) {
            eventStreamSubscription.dispose();
        }
        streamLock.waitUntilUnlocked();
        processingLock.waitUntilUnlocked();
    }

    public void setAddHeaders(@NotNull Consumer<HttpHeaders> addHeaders) {
        this.addHeaders = addHeaders;
    }

    private enum ConfirmationStatus {
        UNCONFIRMED("Unconfirmed"),
        ALL("All");

        final private String status;

        ConfirmationStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }

    private void startConsuming(
        ConfirmationStatus confirmationStatus,
        String offset,
        IOffsetPersister offsetPersister,
        List<String> messageTypes,
        MultiValueMap<String, String> otherParams,
        Function<List<ServerSentEvent<String>>, String> handleSSEs,
        Runnable doAfterAllRetriesFailed
    ) {
        stopConsuming();
        consume(confirmationStatus, offset, offsetPersister, new AtomicInteger(numberOfRetries), null, messageTypes, handleSSEs, otherParams, doAfterAllRetriesFailed);
    }

    private URI buildUrl(UriBuilder uriBuilder, ConfirmationStatus confirmationStatus, String offset, List<String> messageTypes, MultiValueMap<String, String> otherParams) {
        otherParams.set("offset", offset);
        messageTypes.forEach(type -> otherParams.add("messageTypes", type));
        otherParams.set("confirmationStatus", confirmationStatus.getStatus());
        otherParams.forEach((key, value) -> {
            if (value == null || value.isEmpty()) otherParams.remove(key);
        });
        return buildUrl(uriBuilder, otherParams);
    }

    private URI buildUrl(UriBuilder uriBuilder, MultiValueMap<String, String> params) {
        URI uri = URI.create(sseUri);
        return uriBuilder
            .queryParams(params)
            .scheme(uri.getScheme())
            .host(uri.getHost())
            .port(uri.getPort())
            .path(uri.getPath())
            .build();
    }

    private void processEvents(
        List<ServerSentEvent<String>> events,
        Function<List<ServerSentEvent<String>>, String> handleSSEs,
        IOffsetPersister offsetPersister
    ) {
        String id = handleSSEs.apply(events);
        if (id != null && offsetPersister != null) {
            offsetPersister.persistOffset(id);
        }
    }

    private void consume(
        ConfirmationStatus confirmationStatus,
        String offset,
        IOffsetPersister offsetPersister,
        AtomicInteger retriesLeft,
        AtomicInteger currentBackOffPeriod,
        List<String> messageTypes,
        Function<List<ServerSentEvent<String>>, String> handleSSEs,
        MultiValueMap<String, String> otherParams,
        Runnable doAfterAllRetriesFailed
    ) {
        long delay = currentBackOffPeriod == null ? 0 : currentBackOffPeriod.getAndAccumulate(2, backOffMultiplier) * 1000L;
        AtomicInteger newBackOffPeriod = currentBackOffPeriod == null ? new AtomicInteger(this.backOffPeriod) : currentBackOffPeriod;
        Flux<ServerSentEvent<String>> eventList = eventList(confirmationStatus, offset, messageTypes, otherParams);
        AtomicLong stamp = new AtomicLong();
        eventStreamSubscription = Mono.delay(Duration.ofMillis(delay))
            .doOnSuccess(s -> {
                stamp.set(streamLock.acquire());
                LOG.info("start consuming messages");
            })
            // doFinally inside flatMapMany, since lock should not be released if it could not be acquired
            .flatMapMany(l -> eventList.doFinally(s -> {
                if (s != SignalType.CANCEL) streamLock.tryRelease(stamp.get());
            }))
            .bufferTimeout(bufferSize, bufferTime)
            .doOnNext(events -> {
                long processingStamp = processingLock.acquireNow();
                try {
                    if (transactionTemplate == null) processEvents(events, handleSSEs, offsetPersister);
                    else transactionTemplate.executeWithoutResult(s -> processEvents(events, handleSSEs, offsetPersister));
                } finally {
                    processingLock.tryRelease(processingStamp);
                }
            })
            .doOnNext(events -> {
                retriesLeft.set(numberOfRetries);
                newBackOffPeriod.set(backOffPeriod);
            })
            .onErrorComplete(ex -> {
                this.reconnect(ex, confirmationStatus, offsetPersister, retriesLeft, newBackOffPeriod, messageTypes, handleSSEs, doAfterAllRetriesFailed, otherParams);
                return true;
            })
            .doFinally(s -> {
                if (s == SignalType.CANCEL) streamLock.tryRelease(stamp.get());
            })
            .subscribe();
    }

    private final IntBinaryOperator backOffMultiplier = (y, x) -> ((y < 60) ? (x * y) : y);

    protected Flux<ServerSentEvent<String>> eventList(
        ConfirmationStatus confirmationStatus,
        String offset,
        List<String> messageTypes,
        MultiValueMap<String, String> otherParams
    ) {
        HttpMethod method = HttpMethod.GET;
        String jwt = CtpApiClientUtil.createJWT(privateKey, consumerName, CtpApiClientUtil.DEFAULT_VALID_PERIOD_IN_SECS);
        URI uri = buildUrl(new DefaultUriBuilderFactory().builder(), confirmationStatus, offset, messageTypes, otherParams);
        String requestLine = String.format("%s %s HTTP/1.1", method, uri);

        Map<String, String> hmacHeaders = CtpApiClientUtil.getHmacHeaders(CtpApiClientUtil.HmacSignatureType.X_DATE_AND_DIGEST, new byte[0]);
        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", CtpApiClientUtil.calculateHttpRequestHmac(signer, consumerName, hmacHeaders, requestLine));
        headers.add("Cookie", String.format("jwt=%s", jwt));
        headers.add("User-Agent", SdkInfo.userAgent);
        this.addHeaders.accept(headers);
        hmacHeaders.forEach(headers::add);

        ParameterizedTypeReference<ServerSentEvent<String>> typeRef = new ParameterizedTypeReference<>() {
        };

        return webClientBuilder
            .clientConnector(new ReactorClientHttpConnector(HttpClient.create().proxyWithSystemProperties()))
            .build()
            .get()
            .uri(uri)
            .headers(h -> h.putAll(headers))
            .accept(MediaType.TEXT_EVENT_STREAM).retrieve()
            .bodyToFlux(typeRef)
            .timeout(Duration.ofMillis(30000))
            .onTerminateDetach()
            .doOnError((t) -> {
                if (t instanceof WebClientResponseException e) {
                    LOG.error("GET request for event stream yielded a {} with body {}", e.getStatusCode(), e.getResponseBodyAsString());
                }
            })
            .doOnTerminate(() -> { throw new RuntimeException("Event stream stopped"); });
    }

    private void reconnect(
        Throwable ex,
        ConfirmationStatus confirmationStatus,
        IOffsetPersister offsetPersister,
        AtomicInteger retriesLeft,
        AtomicInteger currentBackOffPeriod,
        List<String> messageTypes,
        Function<List<ServerSentEvent<String>>, String> handleSSEs,
        Runnable doAfterAllRetriesFailed,
        MultiValueMap<String, String> otherParams
    ) {
        try {
            int retries = retriesLeft.decrementAndGet();
            LOG.error("Error occurred when consuming messages:", ex);
            if (retries >= 0) {
                LOG.error("Trying to reconnect in " + currentBackOffPeriod + " seconds. Retries left: " + retries);
                String offset = offsetPersister == null ? DEFAULT_OFFSET : offsetPersister.getPersistedOffset();
                this.consume(confirmationStatus, offset, offsetPersister, retriesLeft, currentBackOffPeriod, messageTypes, handleSSEs, otherParams, doAfterAllRetriesFailed);
            } else {
                LOG.error("No retries left, stream consumption has stopped.");
                doAfterAllRetriesFailed.run();
            }
        } catch (Throwable t) {
            LOG.error("Unexpected error in reconnect, stream consumption has stopped", t);
            doAfterAllRetriesFailed.run();
        }
    }
}
