package nl.coin.sdk;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.util.unit.DataSize;

@Configuration
@PropertySource("classpath:common-sdk.properties")
public class CommonStreamConfig {
    private static final String SPRING_CODEC_MAX_IN_MEMORY_SIZE = "spring.codec.max-in-memory-size";
    private static final Logger logger = LoggerFactory.getLogger(Config.class);

    @Value("${" + SPRING_CODEC_MAX_IN_MEMORY_SIZE + "}")
    private DataSize inMemorySizeConfig;

    @Value("${memory-size.minimal}")
    private DataSize minimalInMemorySize;

    @PostConstruct
    private void verifyInMemorySize() {
        if (inMemorySizeConfig.toBytes() < minimalInMemorySize.toBytes()) {
            String errorText = "Failed to start due to: \n---------------------------------\n" +
                    "Minimal in-memory size for stream consumer should be at least: " + minimalInMemorySize + " but is: " + inMemorySizeConfig + ". " +
                    "\nSet " + SPRING_CODEC_MAX_IN_MEMORY_SIZE + "=" + minimalInMemorySize;
            logger.error(errorText);
            throw new IllegalArgumentException(errorText);
        }
    }
}
