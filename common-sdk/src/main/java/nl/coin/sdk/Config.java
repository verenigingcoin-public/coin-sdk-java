package nl.coin.sdk;

import nl.coin.sdk.common.crypto.HmacSha256Signer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;

import java.io.File;
import java.io.InputStream;
import java.security.PrivateKey;

import static nl.coin.sdk.common.crypto.CtpApiClientUtil.readPrivateKey;
import static nl.coin.sdk.common.crypto.CtpApiClientUtil.readPrivateKeyFile;

@Configuration
public class Config {
    @Bean
    public static PrivateKey privateKey(@Value("${private.key.file.name}") String privateKeyFile) {
        InputStream privateKeyIS = Config.class.getResourceAsStream(privateKeyFile);
        if (privateKeyIS != null) {
            return readPrivateKey(privateKeyIS);
        } else {
            File privateKeyF = new File(privateKeyFile);
            Assert.isTrue(privateKeyF.exists(), privateKeyFile + " does not point to valid file. Provide a valid path.");
            return readPrivateKeyFile(new File(privateKeyFile));
        }
    }

    @Bean
    public static HmacSha256Signer hmacSha256Signer(PrivateKey key,
                                                    @Value("${secret.file.name}") String secretFile) {
        InputStream secretIs = Config.class.getResourceAsStream(secretFile);
        if (secretIs != null) {
            return HmacSha256Signer.fromEncryptedBase64EncodedSecret(secretIs, key);
        } else {
            Assert.isTrue(new File(secretFile).exists(), secretFile + " does not point to valid file. Provide a valid path.");
            return HmacSha256Signer.fromEncryptedBase64EncodedSecretFile(secretFile, key);
        }
    }
}
