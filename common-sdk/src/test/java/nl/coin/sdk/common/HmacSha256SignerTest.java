package nl.coin.sdk.common;

import nl.coin.sdk.common.crypto.HmacSha256Signer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;
import java.util.stream.IntStream;

import static junit.framework.TestCase.assertEquals;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
public class HmacSha256SignerTest {

    @Autowired
    HmacSha256Signer hmacSha256Signer;

    String line = "2nf840hf-h9hth9euhgn89495fyvc7Y)*&yncnhsgnchc\n";
    Random random = new Random();

    @Test(timeout = 10_000)
    public void threadSafeTest() {
        IntStream.range(1,100).parallel().forEach((i) -> {
            String random = randomText();
            assertEquals(hmacSha256Signer.sign(random), (hmacSha256Signer.sign(random)));
        });
    }

    private String randomText() {
        StringBuilder data = new StringBuilder();
        int rangeEnd = random.nextInt(100);
        for (int i = 0; i < rangeEnd; i++) {
            data.append(line);
        }
        return data.toString();
    }
}
