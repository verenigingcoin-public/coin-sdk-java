package nl.coin.sdk.common;

import nl.coin.sdk.common.crypto.HmacSha256Signer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.PrivateKey;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
public class CommonMainTest {

    @Autowired
    PrivateKey privateKey;

    @Autowired
    HmacSha256Signer hmacSha256Signer;

    @Test
    public void bootTest() {
        assertNotNull("Private Key should be available as bean (autowired)" ,privateKey);
        assertNotNull("HMAC SHA 256 Signer should be available as bean (autowired)", hmacSha256Signer);
    }

    @Test
    public void signingTest() {
        assertEquals("FSv/8JfHsFTSQjRwfMsALwR+9NWm2+sz3axpxA1Erms=", this.hmacSha256Signer.sign("Test string to check correct signing of data"));
    }
}
