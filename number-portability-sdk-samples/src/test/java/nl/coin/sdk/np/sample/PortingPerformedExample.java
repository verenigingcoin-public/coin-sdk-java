package nl.coin.sdk.np.sample;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.portingperformed.PortingPerformedBuilder;
import nl.coin.sdk.np.messages.v3.portingperformed.PortingPerformedMessage;
import nl.coin.sdk.np.sample.util.TestUtil;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NumberPortabilitySamplesApplication.class)
public class PortingPerformedExample {

    @Value("${sender.operator.name}")
    private String senderOperatorName;

    @Value("${receiver.operator.name}")
    private String receiverOperatorName;

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendPortingPerformed() {
        Message<PortingPerformedMessage> message = createPortingPerformed();
        Assert.assertEquals(MessageType.PORTING_PERFORMED_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }

    private Message<PortingPerformedMessage> createPortingPerformed() {
        String dossierId = null; // use dossier-id from initial portingrequest
        return new PortingPerformedBuilder()
            .setHeader(senderOperatorName, senderOperatorName, TestUtil.ALL_OPERATORS_RECEIVER, null)
            .setDossierId(dossierId)
            .setPortingPerformed(senderOperatorName, receiverOperatorName)
            .addPortingPerformedSeq()
            .setNumberSeries("0303800007", "0303800007").finish()
            .build();
    }
}
