package nl.coin.sdk.np.sample;

import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.sample.util.ExampleInMemoryOffsetPersister;
import nl.coin.sdk.np.sample.util.ExampleLoggingNumberPortabilityMessageListener;
import nl.coin.sdk.np.service.impl.NumberPortabilityMessageConsumer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CountDownLatch;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NumberPortabilitySamplesApplication.class)
public class OpenStreamExamples {

    @Autowired
    private NumberPortabilityMessageConsumer numberPortabilityMessageConsumer;

    @Autowired
    private ExampleLoggingNumberPortabilityMessageListener listener;

    private final Runnable doAfterAllRetriesFailed = () -> {
        // Make sure someone is notified about this fatal error.
        // Also consider stopping the application completely in some way.
    };

    @Test
    public void openStream() {
        try {
            CountDownLatch latch = new CountDownLatch(100);
            listener.setLatch(latch);
            numberPortabilityMessageConsumer.startConsumingUnconfirmed(listener, doAfterAllRetriesFailed);
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void openSpecificStream() {
        try {
            CountDownLatch latch = new CountDownLatch(100);
            numberPortabilityMessageConsumer.startConsumingUnconfirmed(listener, doAfterAllRetriesFailed, MessageType.PORTING_REQUEST_V3, MessageType.CANCEL_V3);
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void openSpecificStreamForAllMessages() {
        try {
            CountDownLatch latch = new CountDownLatch(100);
            listener.setLatch(latch);
            numberPortabilityMessageConsumer.startConsumingAll(
                listener,
                new ExampleInMemoryOffsetPersister(),
                doAfterAllRetriesFailed,
                MessageType.PORTING_REQUEST_V3,
                MessageType.CANCEL_V3,
                MessageType.DEACTIVATION_V3,
                MessageType.PORTING_PERFORMED_V3,
                MessageType.PORTING_REQUEST_ANSWER_DELAYED_V3,
                MessageType.PORTING_REQUEST_ANSWER_V3
            );
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
