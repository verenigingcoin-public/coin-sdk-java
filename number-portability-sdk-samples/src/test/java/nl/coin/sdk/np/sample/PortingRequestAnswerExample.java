package nl.coin.sdk.np.sample;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.portingrequestanswer.PortingRequestAnswerBuilder;
import nl.coin.sdk.np.messages.v3.portingrequestanswer.PortingRequestAnswerMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NumberPortabilitySamplesApplication.class)
public class PortingRequestAnswerExample {

    @Value("${sender.operator.name}")
    private String senderOperatorName;

    @Value("${receiver.operator.name}")
    private String receiverOperatorName;

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendPortingRequestAnswer() {
        Message<PortingRequestAnswerMessage> message = createPortingRequestAnswer();
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }

    private Message<PortingRequestAnswerMessage> createPortingRequestAnswer() {
        String dossierId = null; // use dossier-id from initial portingrequest
        return new PortingRequestAnswerBuilder()
            .setHeader(
                senderOperatorName,
                senderOperatorName,
                receiverOperatorName,
                receiverOperatorName)
            .setDossierId(dossierId)
            .setBlocking("N")
            .addPortingRequestAnswerSeq()
            .setDonornetworkoperator(senderOperatorName)
            .setDonorserviceprovider(senderOperatorName)
            .setFirstPossibleDate(LocalDateTime.now())
            .setNumberSeries("0303800007", "0303800007")
            .finish()
            .build();
    }
}
