package nl.coin.sdk.np.sample;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.cancel.CancelBuilder;
import nl.coin.sdk.np.messages.v3.cancel.CancelMessage;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.sample.util.TestUtil;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NumberPortabilitySamplesApplication.class)
public class CancelExample {

    @Value("${sender.operator.name}")
    private String senderOperatorName;

    @Value("${receiver.operator.name}")
    private String receiverOperatorName;

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendCancel() {
        Message<CancelMessage> message = createCancel();
        Assert.assertEquals(MessageType.CANCEL_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }

    private Message<CancelMessage> createCancel() {
        String dossierId = TestUtil.generateDossierId(senderOperatorName);
        return new CancelBuilder()
            .setHeader(senderOperatorName, receiverOperatorName)
            .setDossierId(dossierId)
            .setNote("some additional information")
            .build();
    }
}
