package nl.coin.sdk.np.sample.util;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.NumberPortabilityMessage;
import nl.coin.sdk.np.messages.v3.activationsn.ActivationServiceNumberMessage;
import nl.coin.sdk.np.messages.v3.cancel.CancelMessage;
import nl.coin.sdk.np.messages.v3.deactivation.DeactivationMessage;
import nl.coin.sdk.np.messages.v3.deactivationsn.DeactivationServiceNumberMessage;
import nl.coin.sdk.np.messages.v3.enums.*;
import nl.coin.sdk.np.messages.v3.errorfound.ErrorFoundMessage;
import nl.coin.sdk.np.messages.v3.portingperformed.PortingPerformedMessage;
import nl.coin.sdk.np.messages.v3.portingrequest.PortingRequestMessage;
import nl.coin.sdk.np.messages.v3.portingrequestanswer.PortingRequestAnswerMessage;
import nl.coin.sdk.np.messages.v3.portingrequestanswerdelayed.PortingRequestAnswerDelayedMessage;
import nl.coin.sdk.np.messages.v3.range.RangeActivationMessage;
import nl.coin.sdk.np.messages.v3.range.RangeDeactivationMessage;
import nl.coin.sdk.np.messages.v3.tariffchangesn.TariffChangeServiceNumberMessage;
import nl.coin.sdk.np.service.INumberPortabilityMessageListener;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class ExampleLoggingNumberPortabilityMessageListener implements INumberPortabilityMessageListener {
    private final NumberPortabilityService numberPortabilityService;

    @Autowired
    public ExampleLoggingNumberPortabilityMessageListener(
        NumberPortabilityService numberPortabilityService
    ) {
        this.numberPortabilityService = numberPortabilityService;
    }

    private NumberPortabilityMessage lastReceivedMessage;
    private CountDownLatch latch;

    public NumberPortabilityMessage getLastReceivedMessage() {
        return lastReceivedMessage;
    }

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void onKeepAlive() {
        // If you want to do specific actions on the heartbeat, write your logic here
    }

    @Override
    public void onUnknownMessage(String messageId, Message message) {
        // If you want to do specific actions with an unknown message, write your logic here
    }

    @Override
    public void onException(Exception exception) {
        // Exceptions should be handled or rethrown here, otherwise a message may be lost
    }

    @Override
    public void onPortingRequest(String messageId, PortingRequestMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onPortingRequestAnswer(String messageId, PortingRequestAnswerMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onPortingRequestAnswerDelayed(String messageId, PortingRequestAnswerDelayedMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onPortingPerformed(String messageId, PortingPerformedMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onCancel(String messageId, CancelMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onDeactivation(String messageId, DeactivationMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onErrorFound(String messageId, ErrorFoundMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onActivationServiceNumber(String messageId, ActivationServiceNumberMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onDeactivationServiceNumber(String messageId, DeactivationServiceNumberMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onTariffChangeServiceNumber(String messageId, TariffChangeServiceNumberMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onRangeActivation(String messageId, RangeActivationMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onRangeDeactivation(String messageId, RangeDeactivationMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onEnumActivationNumber(String messageId, EnumActivationNumberMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onEnumActivationOperator(String messageId, EnumActivationOperatorMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onEnumActivationRange(String messageId, EnumActivationRangeMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onEnumDeactivationNumber(String messageId, EnumDeactivationNumberMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onEnumDeactivationOperator(String messageId, EnumDeactivationOperatorMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onEnumDeactivationRange(String messageId, EnumDeactivationRangeMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onEnumProfileActivation(String messageId, EnumProfileActivationMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onEnumProfileDeactivation(String messageId, EnumProfileDeactivationMessage message) {
        processMessage(messageId, message);
    }

    private void processMessage(String messageId, NumberPortabilityMessage message) {
        lastReceivedMessage = message;
        System.out.println("#########################################################");
        System.out.println("messageId: " + messageId);
        System.out.println("dossierId: " + message.getDossierid());
        System.out.println("header: " + message.getHeader());
        System.out.println("body: " + message.getBody());
        numberPortabilityService.sendConfirmation(messageId);
        if (latch != null) latch.countDown();
    }
}
