package nl.coin.sdk.np.sample;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.portingrequestanswerdelayed.PortingRequestAnswerDelayedBuilder;
import nl.coin.sdk.np.messages.v3.portingrequestanswerdelayed.PortingRequestAnswerDelayedMessage;
import nl.coin.sdk.np.sample.util.TestUtil;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NumberPortabilitySamplesApplication.class)
public class PortingRequestAnswerDelayedExample {

    @Value("${sender.operator.name}")
    private String senderOperatorName;

    @Value("${receiver.operator.name}")
    private String receiverOperatorName;

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendPortingRequestAnswerDelayed() {
        Message<PortingRequestAnswerDelayedMessage> message = createPortingRequestAnswerDelayedMessage();
        Assert.assertEquals(MessageType.PORTING_REQUEST_ANSWER_DELAYED_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }

    private Message<PortingRequestAnswerDelayedMessage> createPortingRequestAnswerDelayedMessage() {
        String dossierId = TestUtil.generateDossierId(senderOperatorName);

        return new PortingRequestAnswerDelayedBuilder()
            .setHeader(
                senderOperatorName,
                senderOperatorName,
                receiverOperatorName,
                receiverOperatorName)
            .setDossierId(dossierId)
            .setDonornetworkoperator(senderOperatorName)
            .setReasoncode("99")
            .build();
    }
}
