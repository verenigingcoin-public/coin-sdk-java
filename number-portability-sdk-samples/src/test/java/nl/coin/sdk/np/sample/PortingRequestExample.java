package nl.coin.sdk.np.sample;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.portingrequest.PortingRequestBuilder;
import nl.coin.sdk.np.messages.v3.portingrequest.PortingRequestMessage;
import nl.coin.sdk.np.sample.util.ExampleLoggingNumberPortabilityMessageListener;
import nl.coin.sdk.np.sample.util.TestUtil;
import nl.coin.sdk.np.service.impl.NumberPortabilityMessageConsumer;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NumberPortabilitySamplesApplication.class)
public class PortingRequestExample {

    @Value("${sender.operator.name}")
    private String senderOperatorName;

    @Autowired
    private NumberPortabilityMessageConsumer numberPortabilityMessageConsumer;

    @Autowired
    private ExampleLoggingNumberPortabilityMessageListener listener;

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    // This testcase only sends the message, there should be a consumer running to listen to the stream
    // and receiving the messages
    @Test
    public void sendPortingRequest() {
        Message<PortingRequestMessage> message = createPortingRequest();
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertNotNull(messageResponse.getTransactionId());
    }

    private Message<PortingRequestMessage> createPortingRequest() {
        String dossierId = TestUtil.generateDossierId(senderOperatorName);

        return new PortingRequestBuilder()
            .setHeader(senderOperatorName, senderOperatorName, TestUtil.CRDB_RECEIVER, null)
            .setRecipientnetworkoperator(senderOperatorName)
            .setRecipientserviceprovider(senderOperatorName)
            .setDossierId(dossierId)
            .setCustomerInfo(null, "Vereniging COIN", "10", null, "2803PK", "1234")
            .setNote("some additional information")
            .setContract("EARLY_TERMINATION")
            .addPortingRequestSeq()
            .setNumberSeries("0303800007", "0303800007").finish()
            .build();
    }
}
