package nl.coin.sdk.np.sample.util;

public class TestUtil {

    public static String generateDossierId(String operatorCode) {
        String randomNumber5Digits = String.valueOf(Math.round(Math.random() * 10000) + 9999);
        return operatorCode + "-" + randomNumber5Digits;
    }

    public static String CRDB_RECEIVER = "CRDB";

    public static String ALL_OPERATORS_RECEIVER = "ALLO";

}
