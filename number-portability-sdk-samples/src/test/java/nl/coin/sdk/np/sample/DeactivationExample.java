package nl.coin.sdk.np.sample;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.deactivation.DeactivationBuilder;
import nl.coin.sdk.np.messages.v3.deactivation.DeactivationMessage;
import nl.coin.sdk.np.sample.util.TestUtil;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NumberPortabilitySamplesApplication.class)
public class DeactivationExample {

    @Value("${sender.operator.name}")
    private String senderOperatorName;

    @Value("${receiver.operator.name}")
    private String receiverOperatorName;

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    private Message<DeactivationMessage> message;

    @Before
    public void createMessage() {
        String dossierId = TestUtil.generateDossierId(senderOperatorName);
        message = new DeactivationBuilder()
            .setHeader(senderOperatorName, TestUtil.ALL_OPERATORS_RECEIVER)
            .setDossierId(dossierId)
            .setOriginalNetworkOperator(receiverOperatorName)
            .setCurrentNetworkOperator(senderOperatorName)
            .addDeactivationSeq()
            .setNumberSeries("0303800007", "0303800007").finish()
            .build();
    }

    @Test
    public void sendDeactivation() {
        Assert.assertEquals(MessageType.DEACTIVATION_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
