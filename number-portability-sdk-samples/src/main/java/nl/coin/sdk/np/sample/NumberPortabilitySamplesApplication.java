package nl.coin.sdk.np.sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("nl.coin")
@SpringBootApplication
public class NumberPortabilitySamplesApplication {

    private static final Logger LOG = LoggerFactory.getLogger(NumberPortabilitySamplesApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(NumberPortabilitySamplesApplication.class, args);
        LOG.debug("Starting Number Portability SDK Samples Project");
    }
}
