# COIN Number Portability Java SDK Samples

For extensive documentation of the Java Number Portability SDK, please have a look at the following [README](https://gitlab.com/verenigingcoin-public/coin-sdk-java/blob/master/number-portability-sdk/README.md).

# Configure Examples
Besides the credentials configuration (see link above), the examples require a sender and receiver operator to be configured.

The `/src/test/resources/application-test.properties` properties file contains:

```properties
# Sender / Receiver Config
# ---------------
# Sender configuration
sender.operator.name=<configure-sender-operator-name!!!>

# Receiver configuration
receiver.operator.name=<configure-receiver-operator-name!!!>
```

