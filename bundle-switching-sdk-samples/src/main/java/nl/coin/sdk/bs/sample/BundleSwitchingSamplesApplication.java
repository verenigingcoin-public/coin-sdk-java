package nl.coin.sdk.bs.sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("nl.coin")
@SpringBootApplication
public class BundleSwitchingSamplesApplication {

    private static final Logger LOG = LoggerFactory.getLogger(BundleSwitchingSamplesApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(BundleSwitchingSamplesApplication.class, args);
        LOG.debug("Starting Bundle Switching SDK Samples Project");
    }
}
