package nl.coin.sdk.bs.sample;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.MessageType;
import nl.coin.sdk.bs.messages.v5.cancel.CancelBuilder;
import nl.coin.sdk.bs.messages.v5.cancel.CancelMessage;
import nl.coin.sdk.bs.messages.v5.common.MessageResponse;
import nl.coin.sdk.bs.sample.util.TestUtil;
import nl.coin.sdk.bs.service.impl.BundleSwitchingService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BundleSwitchingSamplesApplication.class)
public class CancelExample {

    @Value("${sender.serviceprovider.name}")
    private String senderServiceProviderName;

    @Value("${receiver.serviceprovider.name}")
    private String receiverServiceProviderName;

    @Autowired
    private BundleSwitchingService bundleSwitchingService;

    @Test
    public void sendCancel() {
        Message<CancelMessage> message = createCancel();
        Assert.assertEquals(MessageType.CANCEL_V5, message.getMessageType());
        MessageResponse messageResponse = bundleSwitchingService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }

    private Message<CancelMessage> createCancel() {
        String dossierId = TestUtil.generateDossierId(senderServiceProviderName);
        return new CancelBuilder()
                .setHeader(senderServiceProviderName, receiverServiceProviderName)
                .setDossierId(dossierId)
                .setNote("Reason for cancelling")
                .build();
    }
}
