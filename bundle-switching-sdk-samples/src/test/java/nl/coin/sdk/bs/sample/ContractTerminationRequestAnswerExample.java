package nl.coin.sdk.bs.sample;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.common.MessageResponse;
import nl.coin.sdk.bs.messages.v5.contractterminationrequestanswer.ContractTerminationRequestAnswerBuilder;
import nl.coin.sdk.bs.messages.v5.contractterminationrequestanswer.ContractTerminationRequestAnswerMessage;
import nl.coin.sdk.bs.sample.util.TestUtil;
import nl.coin.sdk.bs.service.impl.BundleSwitchingService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BundleSwitchingSamplesApplication.class)
public class ContractTerminationRequestAnswerExample {

    @Value("${sender.serviceprovider.name}")
    private String senderServiceProviderName;

    @Value("${receiver.serviceprovider.name}")
    private String receiverServiceProviderName;

    @Autowired
    private BundleSwitchingService bundleSwitchingService;

    @Test
    public void sendContractTerminationRequestAnswer() {
        Message<ContractTerminationRequestAnswerMessage> message = createContractTerminationRequestAnswer();
        MessageResponse messageResponse = bundleSwitchingService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }

    private Message<ContractTerminationRequestAnswerMessage> createContractTerminationRequestAnswer() {
        String dossierId = null; // use dossier-id from initial contractterminationrequest
        return new ContractTerminationRequestAnswerBuilder()
            .setHeader(senderServiceProviderName, receiverServiceProviderName)
            .setDossierId(dossierId)
            .setBlocking(false)
            .setBlockingcode("0")
            .setFirstPossibleDate(LocalDateTime.now())
            .build();
    }
}
