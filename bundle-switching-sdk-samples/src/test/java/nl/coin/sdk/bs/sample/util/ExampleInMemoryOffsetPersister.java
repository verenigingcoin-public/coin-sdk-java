package nl.coin.sdk.bs.sample.util;

import nl.coin.sdk.common.client.IOffsetPersister;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicReference;

@Component
public class ExampleInMemoryOffsetPersister implements IOffsetPersister {

    final private AtomicReference<String> offset = new AtomicReference<>();

    @Override
    public void persistOffset(String offset) {
        this.offset.set(offset);
    }

    @Override
    public String getPersistedOffset() {
        return this.offset.get();
    }
}
