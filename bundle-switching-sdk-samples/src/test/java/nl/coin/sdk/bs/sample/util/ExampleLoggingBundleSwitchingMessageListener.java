package nl.coin.sdk.bs.sample.util;

import nl.coin.sdk.bs.messages.v5.BundleSwitchingMessage;
import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.cancel.CancelMessage;
import nl.coin.sdk.bs.messages.v5.contractterminationperformed.ContractTerminationPerformedMessage;
import nl.coin.sdk.bs.messages.v5.contractterminationrequest.ContractTerminationRequestMessage;
import nl.coin.sdk.bs.messages.v5.contractterminationrequestanswer.ContractTerminationRequestAnswerMessage;
import nl.coin.sdk.bs.messages.v5.errorfound.ErrorFoundMessage;
import nl.coin.sdk.bs.service.IBundleSwitchingMessageListener;
import nl.coin.sdk.bs.service.impl.BundleSwitchingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class ExampleLoggingBundleSwitchingMessageListener implements IBundleSwitchingMessageListener {
    private final BundleSwitchingService bundleSwitchingService;

    @Autowired
    public ExampleLoggingBundleSwitchingMessageListener(
        BundleSwitchingService bundleSwitchingService
    ) {
        this.bundleSwitchingService = bundleSwitchingService;
    }

    private BundleSwitchingMessage lastReceivedMessage;
    private CountDownLatch latch;

    public BundleSwitchingMessage getLastReceivedMessage() {
        return lastReceivedMessage;
    }

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void onKeepAlive() {
        // If you want to do specific actions on the heartbeat, write your logic here
    }

    @Override
    public void onUnknownMessage(String messageId, Message message) {
        // If you want to do specific actions with an unknown message, write your logic here
    }

    @Override
    public void onException(Exception exception) {
        // Exceptions should be handled or rethrown here, otherwise a message may be lost
    }

    @Override
    public void onContractTerminationRequest(String messageId, ContractTerminationRequestMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onContractTerminationRequestAnswer(String messageId, ContractTerminationRequestAnswerMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onContractTerminationPerformed(String messageId, ContractTerminationPerformedMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onCancel(String messageId, CancelMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onErrorFound(String messageId, ErrorFoundMessage message) {
        processMessage(messageId, message);
    }

    private void processMessage(String messageId, BundleSwitchingMessage message) {
        lastReceivedMessage = message;
        System.out.println("#########################################################");
        System.out.println("messageId: " + messageId);
        System.out.println("dossierId: " + message.getDossierid());
        System.out.println("header: " + message.getHeader());
        System.out.println("body: " + message.getBody());
        bundleSwitchingService.sendConfirmation(messageId);
        if (latch != null) latch.countDown();
    }
}
