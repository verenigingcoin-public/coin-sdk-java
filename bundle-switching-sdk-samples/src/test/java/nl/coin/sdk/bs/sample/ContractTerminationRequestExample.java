package nl.coin.sdk.bs.sample;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.common.MessageResponse;
import nl.coin.sdk.bs.messages.v5.contractterminationrequest.ContractTerminationRequestBuilder;
import nl.coin.sdk.bs.messages.v5.contractterminationrequest.ContractTerminationRequestMessage;
import nl.coin.sdk.bs.sample.util.TestUtil;
import nl.coin.sdk.bs.service.impl.BundleSwitchingService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BundleSwitchingSamplesApplication.class)
public class ContractTerminationRequestExample {

    @Value("${sender.serviceprovider.name}")
    private String senderServiceProviderName;

    @Value("${receiver.serviceprovider.name}")
    private String receiverServiceProviderName;

    @Autowired
    private BundleSwitchingService bundleSwitchingService;

    // This testcase only sends the message, there should be a consumer running to listen to the stream
    // and receiving the messages
    @Test
    public void sendContractTerminationRequest() {
        Message<ContractTerminationRequestMessage> message = createContractTerminationRequest();
        MessageResponse messageResponse = bundleSwitchingService.sendMessage(message);
        Assert.assertNotNull(messageResponse.getTransactionId());
    }

    private Message<ContractTerminationRequestMessage> createContractTerminationRequest() {
        String dossierId = TestUtil.generateDossierId(senderServiceProviderName);

        return new ContractTerminationRequestBuilder()
            .setHeader(senderServiceProviderName, receiverServiceProviderName)
            .setRecipientserviceprovider(senderServiceProviderName)
            .setDonorserviceprovider(receiverServiceProviderName)
            .setDossierId(dossierId)
            .setEarlytermination(false)
            .setName("name")
            .setAddress("1234AB", "11", "A")
            .addNumberSeries("0612345678", "0612345678")
            .addValidationBlock("iban", "123")
            .build();
    }
}
