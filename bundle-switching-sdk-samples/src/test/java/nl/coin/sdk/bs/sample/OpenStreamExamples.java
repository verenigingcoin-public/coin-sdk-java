package nl.coin.sdk.bs.sample;

import nl.coin.sdk.bs.messages.v5.MessageType;
import nl.coin.sdk.bs.sample.util.ExampleInMemoryOffsetPersister;
import nl.coin.sdk.bs.sample.util.ExampleLoggingBundleSwitchingMessageListener;
import nl.coin.sdk.bs.service.impl.BundleSwitchingMessageConsumer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CountDownLatch;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BundleSwitchingSamplesApplication.class)
public class OpenStreamExamples {

    @Autowired
    private BundleSwitchingMessageConsumer bundleSwitchingMessageConsumer;

    @Autowired
    private ExampleLoggingBundleSwitchingMessageListener listener;

    private final Runnable doAfterAllRetriesFailed = () -> {
        // Make sure someone is notified about this fatal error.
        // Also consider stopping the application completely in some way.
    };

    @Test
    public void openStream() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(100);
        listener.setLatch(latch);
        bundleSwitchingMessageConsumer.startConsumingUnconfirmed(listener, doAfterAllRetriesFailed);
        latch.await();
    }

    @Test
    public void openSpecificStream() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(100);
        listener.setLatch(latch);
        bundleSwitchingMessageConsumer.startConsumingUnconfirmed(listener, doAfterAllRetriesFailed, MessageType.CONTRACT_TERMINATION_REQUEST_V5, MessageType.CANCEL_V5);
        latch.await();
    }

    @Test
    public void openSpecificStreamForAllMessages() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(100);
        listener.setLatch(latch);
        bundleSwitchingMessageConsumer.startConsumingAll(listener, new ExampleInMemoryOffsetPersister(), doAfterAllRetriesFailed, MessageType.CONTRACT_TERMINATION_REQUEST_V5);
        latch.await();
    }
}
