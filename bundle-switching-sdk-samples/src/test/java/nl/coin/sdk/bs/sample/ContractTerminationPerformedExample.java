package nl.coin.sdk.bs.sample;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.MessageType;
import nl.coin.sdk.bs.messages.v5.common.MessageResponse;
import nl.coin.sdk.bs.messages.v5.contractterminationperformed.ContractTerminationPerformedBuilder;
import nl.coin.sdk.bs.messages.v5.contractterminationperformed.ContractTerminationPerformedMessage;
import nl.coin.sdk.bs.sample.util.TestUtil;
import nl.coin.sdk.bs.service.impl.BundleSwitchingService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BundleSwitchingSamplesApplication.class)
public class ContractTerminationPerformedExample {

    @Value("${sender.serviceprovider.name}")
    private String senderServiceProviderName;

    @Value("${receiver.serviceprovider.name}")
    private String receiverServiceProviderName;

    @Autowired
    private BundleSwitchingService bundleSwitchingService;

    @Test
    public void sendContractTerminationPerformed() {
        Message<ContractTerminationPerformedMessage> message = createContractTerminationPerformed();
        Assert.assertEquals(MessageType.CONTRACT_TERMINATION_PERFORMED_V5, message.getMessageType());
        MessageResponse messageResponse = bundleSwitchingService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }

    private Message<ContractTerminationPerformedMessage> createContractTerminationPerformed() {
        String dossierId = null; // use dossier-id from initial contractterminationrequest
        return new ContractTerminationPerformedBuilder()
            .setHeader(senderServiceProviderName, receiverServiceProviderName)
            .setDossierId(dossierId)
            .build();
    }
}
