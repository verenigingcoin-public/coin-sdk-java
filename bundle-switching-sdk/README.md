# Java Bundle Switching SDK

## Introduction

The Java SDK supports secured access to the Bundle Switching API using the Spring Framework (Spring Boot 3.x).

For a quick start, follow the steps below:
* [Setup](#setup)
* [Configure Credentials](#configure-credentials)
* [Send Messages](#send-messages)
* [Consume Messages](#consume-messages)
* [Error handling](#error-handling)
* [Configure Logging](#configure-logging)


## Setup

### Samples Project for the Bundle Switching API
COIN provides a sample project in the `bundle-switching-sdk-samples` directory.

### Manual Installation
For manual installation of the Bundle Switching SDK, add the `bundle-switching-sdk` dependency to your build file.

#### Maven

If you are using Maven:
- add the COIN repository 
```xml
<repositories>
    <repository>
        <id>gitlab-repo</id>
        <url>https://gitlab.com/api/v4/projects/12541046/packages/maven</url>
        <layout>default</layout>
    </repository>
</repositories>
```
- add the dependency to your pom.xml file:
```xml
<dependency>
    <groupId>nl.coin</groupId>
    <artifactId>bundle-switching-sdk</artifactId>
    <version>{latest-version}</version>
</dependency>
```

#### Gradle

If you are using Gradle:
- add the COIN repository:

```
repositories {
    maven {
        url 'https://gitlab.com/api/v4/projects/12541046/packages/maven'
    }
}

```
- add the dependency to the dependencies block:

```
compile 'nl.coin:bundle-switching-sdk:{latest-version}'
```

## Configure Credentials
For secure access various credentials are required.
- Check [this README](https://gitlab.com/verenigingcoin-public/consumer-configuration/-/blob/master/README.md) to find out how to configure these.
- To summarize, you will need:
    - Consumer name 
    - `private-key.pem` file
    - `sharedkey.encrypted` encrypted (by public key) HMAC secret file
  
Add the following properties to the `/src/main/resources/application.properties` file :
```properties
#Name of the consumer as configured in: https://test-portal.coin.nl/iam#/
consumer.name=<your-consumer-name>
#Path to private key file
private.key.file.name=path-to/private-key.pem
#Path to encrypted HMAC secret for given consumer as copied from:  https://test-portal.coin.nl/iam#/
secret.file.name=path-to/sharedkey.encrypted
```

## Send Messages
The SDK provides various message builders for creating messages to send by means of the `BundleSwitchingService` service.

### Create and send a message
The example below shows how to call the Bundle Switching API 'contractterminationrequest' endpoint using the provided `<MessageType>Builder` class to create a payload and send it with 
the `BundleSwitchingService` service. 

```java
@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BundleSwitchingSamplesApplication.class)
public class ContractTerminationRequestExample {

    @Autowired
    private BundleSwitchingService bundleSwitchingService;

    @Test
    public void sendContractTerminationRequestExample() {
        //Use a message builder to create bundle switching messages
         Message<ContractTerminationRequestMessage> message = new ContractTerminationRequestBuilder()
            .setHeader(senderServiceProviderName, receiverServiceProviderName)
            .setRecipientserviceprovider(senderServiceProviderName)
            .setDonorserviceprovider(receiverServiceProviderName)
            .setDossierId(dossierId)
            .setEarlytermination(false)
            .setName("name")
            .setAddress("1234AB", "11", "A")
            .addNumberSeries("0612345678", "0612345678")
            .build();
         //Use the BundleSwitchingService to send the message
        MessageResponse messageResponse = bundleSwitchingService.sendMessage(message);
        Assert.assertNotNull(messageResponse.getTransactionId());
    }
}
```

## Consume Messages

### Create Message Listener
For message consumption, the Bundle Switching API makes use of HTTP's [ServerSentEvents](https://en.wikipedia.org/wiki/Server-sent_events).
The SDK offers a Listener interface `IBundleSwitchingMessageListener` which is triggered upon reception of a message payload.
Whenever the API doesn't send any other message for 20 seconds, it sends an empty 'heartbeat' message, which triggers the onKeepAlive() method.

```java
public interface IBundleSwitchingMessageListener {
    void onKeepAlive();

    void onUnknownMessage(String messageId, Message message);

    void onException(Exception exception);

    void onContractTerminationRequest(String messageId, ContractTerminationRequestMessage message);

    void onContractTerminationRequestAnswer(String messageId, ContractTerminationRequestAnswerMessage message);

    void onContractTerminationPerformed(String messageId, ContractTerminationPerformedMessage message);

    void onCancel(String messageId, CancelMessage message);

    void onErrorFound(String messageId, ErrorFoundMessage message);
}
```

### Start consuming messages 

The `BundleSwitchingMessageConsumer` has a couple of `startConsuming...()` methods for consuming messages, of which `startConsumingUnconfirmed()` is most useful.
All these methods need an instance of the `IBundleSwitchingMessageListener`.

Here is an example:

```java
@SpringBootTest
public class OpenStreamExamples {

    @Autowired
    private BundleSwitchingMessageConsumer bundleSwitchingMessageConsumer;

    @Autowired
    private ExampleLoggingBundleSwitchingMessageListener listener;

    private final Runnable doAfterAllRetriesFailed = () -> {
        // Make sure someone is notified about this fatal error.
        // Also consider stopping the application completely in some way.
    };

    @Test
    public void openStream() {
        bundleSwitchingMessageConsumer.startConsumingUnconfirmed(listener, doAfterAllRetriesFailed);
    }
}
```

### Consume specific messages using filters

The `BundleSwitchingMessageConsumer` provides various filters for message consumption. The filters are:
- `MessageType`: All possible message types, including errors. Use the `MessageType`-enumeration to indicate which messages have to be consumed.
- confirmation status: By using `startConsumingAll()`, all messages will be received, both confirmed and unconfirmed.   
    **Note:** this enables the consumption of the *whole message history*.
    Therefore, this requires you to supply an implementation of the `IOffsetPersister` interface.
    The purpose of this interface is to track the `message-id` of the last received and processed message.
    In the case of a reconnect, message consumption will then resume where it left off.
- `offset`: starts consuming messages based on the given `message-id` offset. ***Note:*** it is the responsibility of the client to keep track of the `offset`.
- `serviceProviders`: use this parameter if there are there are multiple service providers linked to your consumer and you only want to consume messages for a subset of these providers.

The `BundleSwitchingMessageConsumer` can be configured as follows:

```properties
#BundleSwitchingMessageConsumer config
#---------------
#Number of retries when connection fails
listener.numberOfRetries=3
#First retry after seconds
listener.backOffPeriod=1
```

The message consumer will try to connect up to `listener.numberOfRetries` times to the Bundle Switching API.
If the consumer tries to establish a connection within this number of retries, it will call the provided `Runnable doAfterAllRetriesFailed`.
```java
public void openStream() {
	bundleSwitchingMessageConsumer.startConsumingUnconfirmed(listener, doAfterAllRetriesFailed);
}
```

#### Confirm Messages
Once a consumed message is processed it needs to be confirmed. To confirm a message use the `BundleSwitchingService.sendConfirmation(id)` method:
```java
@SpringBootTest
public class ConfirmationExample {

    @Autowired
    private BundleSwitchingMessageConsumer bundleSwitchingMessageConsumer;

    @Autowired
    private BundleSwitchingService bundleSwitchingService;

    @Test
    public void openStreamAndConfirm() {
        bundleSwitchingMessageConsumer.startConsumingUnconfirmed(new IBundleSwitchingMessageListener() {
            @Override
            public void onContractTerminationRequest(String messageId, ContractTerminationRequestMessage message) {
                //process message...

                //confirm message after successful processing
                bundleSwitchingService.sendConfirmation(messageId);
            }
        }, () -> { /* handle fatal error */ });
    }
}
```

## Error Handling

The Bundle Switching API can return errors in one of two ways:
1. The server received an incorrect payload and replies with an error response (synchronous)

    The REST layer of the system only performs basic payload checks, such as swagger schema validity and message authorization. Any errors in these checks are immediately returned 
    as an error reply.
    API error replies are wrapped in one of the following exceptions:
     - `RestClientErrorException` containing an `ErrorMessage` class with `transactionId` and detailed `errors` properties.
     - `HttpServerErrorException` containing status code and string body.

2. As a ServerSentEvent containing an `ErrorFoundMessage` (asynchronous)

    The system performs detailed functional validation asynchronously. Errors in this stage are sent via a `ServerSentEvent`, 
    which will eventually be offered in the `onErrorFound(String messageId, ErrorFoundMessage message)` of the `IBundleSwitchingMessageListener`:
    ```java
    public interface IBundleSwitchingMessageListener {
        // Handle Error Found messages
        void onErrorFound(String messageId, ErrorFoundMessage message);
    }
    ```

    ***Note:*** `ErrorFound` messages need to be confirmed like any other message received via a `ServerSentEvent`. 


## Configure Logging

The implementation provided uses the HttpClient from Apache Commons to send and receive messages. Logging of the messages (header or content) can be enabled by setting the 
following properties to the desired log-level:
```properties
#Http Logging settings
org.apache.commons.logging.simplelog.log.httpclient.wire.header=WARN
org.apache.commons.logging.simplelog.log.httpclient.wire.content=WARN
```
More information about the possibilities can be found at the site of the [Apache Commons HttpClient](https://hc.apache.org/httpcomponents-client-4.5.x/logging.html)
