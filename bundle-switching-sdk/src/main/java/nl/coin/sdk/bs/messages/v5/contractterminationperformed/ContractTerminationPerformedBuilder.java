package nl.coin.sdk.bs.messages.v5.contractterminationperformed;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.MessageType;
import nl.coin.sdk.bs.messages.v5.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

public class ContractTerminationPerformedBuilder extends HeaderCreator<ContractTerminationPerformedBuilder> implements Builder<Message<ContractTerminationPerformedMessage>> {
    private ContractTerminationPerformed contractTerminationPerformed = new ContractTerminationPerformed();

    @Override
    protected ContractTerminationPerformedBuilder getThis() {
        return this;
    }

    public ContractTerminationPerformedBuilder setDossierId(String dossierId) {
        contractTerminationPerformed.setDossierid(dossierId);
        return this;
    }

    public ContractTerminationPerformedBuilder setActualDatetime(String actualdatetime) {
        contractTerminationPerformed.setActualdatetime(actualdatetime);
        return this;
    }

    public ContractTerminationPerformedBuilder setNote(String note) {
        contractTerminationPerformed.setNote(note);
        return this;
    }

    public Message<ContractTerminationPerformedMessage> build() {
        ContractTerminationPerformedBody contractTerminationPerformedBody = new ContractTerminationPerformedBody().contractterminationperformed(contractTerminationPerformed);
        ContractTerminationPerformedMessage message = new ContractTerminationPerformedMessage()
                .header(header)
                .body(contractTerminationPerformedBody);
        return new Message<>(message, MessageType.CONTRACT_TERMINATION_PERFORMED_V5);
    }
}
