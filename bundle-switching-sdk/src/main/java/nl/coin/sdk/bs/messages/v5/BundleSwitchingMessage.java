package nl.coin.sdk.bs.messages.v5;

import nl.coin.sdk.bs.messages.v5.common.Header;

public interface BundleSwitchingMessage<T> {
    Header getHeader();
    void setHeader(Header header);
    String getDossierid();
    T getBody();
}
