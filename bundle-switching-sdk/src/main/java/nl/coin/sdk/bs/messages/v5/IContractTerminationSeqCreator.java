package nl.coin.sdk.bs.messages.v5;

public interface IContractTerminationSeqCreator<BUILDER, SEQUENCE> {
    BUILDER setBuild(SEQUENCE p);
}
