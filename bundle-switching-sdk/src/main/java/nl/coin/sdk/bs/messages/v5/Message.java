package nl.coin.sdk.bs.messages.v5;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Objects;

public class Message<T extends BundleSwitchingMessage> {
    private T message;

    @JsonIgnore
    private MessageType messageType;

    public Message(){}

    public Message(T message, MessageType messageType) {
        this.message = message;
        this.messageType = messageType;
    }

    public T getMessage() {
        return message;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Message<T> message = (Message<T>) o;
        return this.message.equals(message.getMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, messageType);
    }
}
