package nl.coin.sdk.bs.messages.v5.contractterminationrequest;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.MessageType;
import nl.coin.sdk.bs.messages.v5.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

public class ContractTerminationRequestBuilder extends HeaderCreator<ContractTerminationRequestBuilder> implements Builder<Message<ContractTerminationRequestMessage>> {
    private ContractTerminationRequest contractTerminationRequest = new ContractTerminationRequest();

    @Override
    protected ContractTerminationRequestBuilder getThis() {
        return this;
    }

    public ContractTerminationRequestBuilder setRecipientnetworkoperator(String recipientnetworkoperator) {
        contractTerminationRequest.setRecipientnetworkoperator(recipientnetworkoperator);
        return this;
    }

    public ContractTerminationRequestBuilder setRecipientserviceprovider(String recipientserviceprovider) {
        contractTerminationRequest.setRecipientserviceprovider(recipientserviceprovider);
        return this;
    }

    public ContractTerminationRequestBuilder setDonornetworkoperator(String donornetworkoperator) {
        contractTerminationRequest.setDonornetworkoperator(donornetworkoperator);
        return this;
    }

    public ContractTerminationRequestBuilder setDonorserviceprovider(String donorserviceprovider) {
        contractTerminationRequest.setDonorserviceprovider(donorserviceprovider);
        return this;
    }

    public ContractTerminationRequestBuilder setDossierId(String dossierid) {
        contractTerminationRequest.setDossierid(dossierid);
        return this;
    }

    public ContractTerminationRequestBuilder setNote(String note) {
        contractTerminationRequest.setNote(note);
        return this;
    }

    public ContractTerminationRequestBuilder setName(String name) {
        contractTerminationRequest.setName(name);
        return this;
    }

    public ContractTerminationRequestBuilder setEarlytermination(Boolean earlytermination) {
        contractTerminationRequest.setEarlytermination(earlytermination ? "Y" : "N");
        return this;
    }

    public ContractTerminationRequestBuilder setAddress(String postcode, String housenr) {
        return setAddress(postcode, housenr, null);
    }

    public ContractTerminationRequestBuilder setAddress(String postcode, String housenr, String housenrExt) {
        AddressBlock address = new AddressBlock();
        address.setPostcode(postcode);
        address.setHousenr(housenr);
        address.setHousenr_ext(housenrExt);
        contractTerminationRequest.setAddressblock(address);
        return this;
    }

    public ContractTerminationRequestBuilder addNumberSeries(String start, String end) {
        NumberSeries numberSeries = new NumberSeries();
        numberSeries.setStart(start);
        numberSeries.setEnd(end);
        contractTerminationRequest.addNumberseriesItem(numberSeries);
        return this;
    }

    public ContractTerminationRequestBuilder addValidationBlock(String name, String value) {
        ValidationBlock validation = new ValidationBlock();
        validation.setName(name);
        validation.setValue(value);
        contractTerminationRequest.addValidationblockItem(validation);
        return this;
    }

    public Message<ContractTerminationRequestMessage> build() {
        ContractTerminationRequestBody contractTerminationRequestBody = new ContractTerminationRequestBody().contractterminationrequest(contractTerminationRequest);
        ContractTerminationRequestMessage message = new ContractTerminationRequestMessage()
                .header(header)
                .body(contractTerminationRequestBody);
        return new Message<>(message, MessageType.CONTRACT_TERMINATION_REQUEST_V5);
    }
}
