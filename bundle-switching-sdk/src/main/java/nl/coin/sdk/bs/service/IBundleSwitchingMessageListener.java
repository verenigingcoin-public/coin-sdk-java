package nl.coin.sdk.bs.service;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.cancel.CancelMessage;
import nl.coin.sdk.bs.messages.v5.contractterminationperformed.ContractTerminationPerformedMessage;
import nl.coin.sdk.bs.messages.v5.contractterminationrequest.ContractTerminationRequestMessage;
import nl.coin.sdk.bs.messages.v5.contractterminationrequestanswer.ContractTerminationRequestAnswerMessage;
import nl.coin.sdk.bs.messages.v5.errorfound.ErrorFoundMessage;

public interface IBundleSwitchingMessageListener {
    void onKeepAlive();

    void onUnknownMessage(String messageId, Message message);

    void onException(Exception exception);

    void onContractTerminationRequest(String messageId, ContractTerminationRequestMessage message);

    void onContractTerminationRequestAnswer(String messageId, ContractTerminationRequestAnswerMessage message);

    void onContractTerminationPerformed(String messageId, ContractTerminationPerformedMessage message);

    void onCancel(String messageId, CancelMessage message);

    void onErrorFound(String messageId, ErrorFoundMessage message);
}
