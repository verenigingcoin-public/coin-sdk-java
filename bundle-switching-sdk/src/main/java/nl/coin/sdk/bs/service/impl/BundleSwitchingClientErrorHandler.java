package nl.coin.sdk.bs.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.coin.sdk.bs.messages.v5.common.ErrorResponse;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.util.HashMap;

@Component
public class BundleSwitchingClientErrorHandler implements ResponseErrorHandler {

    private static final Logger LOG = LoggerFactory.getLogger(BundleSwitchingService.class);
    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
        return httpResponse.getStatusCode().is4xxClientError() || httpResponse.getStatusCode().is5xxServerError();
    }

    @Override
    public void handleError(ClientHttpResponse httpResponse) throws IOException {
        try {
            String body = IOUtils.toString(httpResponse.getBody());
            HashMap result = new ObjectMapper().readValue(body, HashMap.class);
            if (result.containsKey("transactionId")) {
                clientError(httpResponse, body);
            } else {
                otherError(httpResponse);
            }
        } catch (IOException e) {
            otherError(httpResponse);
        }
    }

    private void clientError(ClientHttpResponse httpResponse, String body) throws IOException {
        ErrorResponse errorResponse = mapper.readValue(body, ErrorResponse.class);
        LOG.error("Client error occurred. Status code=[{}] Error response=[{}]", httpResponse.getStatusCode(), errorResponse.toString());
        throw new RestClientErrorException(httpResponse.getStatusCode(), errorResponse);
    }

    private void otherError(ClientHttpResponse httpResponse) throws IOException {
        LOG.error("Unexpected error occurred. Status code=[{}]", httpResponse.getStatusCode());
        throw new HttpServerErrorException(httpResponse.getStatusCode(), httpResponse.getStatusText());
    }

}
