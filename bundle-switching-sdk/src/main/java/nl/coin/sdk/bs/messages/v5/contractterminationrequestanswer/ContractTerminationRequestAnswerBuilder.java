package nl.coin.sdk.bs.messages.v5.contractterminationrequestanswer;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.MessageType;
import nl.coin.sdk.bs.messages.v5.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ContractTerminationRequestAnswerBuilder extends HeaderCreator<ContractTerminationRequestAnswerBuilder> implements Builder<Message<ContractTerminationRequestAnswerMessage>> {
    private final ContractTerminationRequestAnswer contractTerminationRequestAnswer = new ContractTerminationRequestAnswer();

    @Override
    protected ContractTerminationRequestAnswerBuilder getThis() {
        return this;
    }

    public ContractTerminationRequestAnswerBuilder setDossierId(String dossierId) {
        contractTerminationRequestAnswer.setDossierid(dossierId);
        return this;
    }

    public ContractTerminationRequestAnswerBuilder setBlocking(Boolean blocking) {
        contractTerminationRequestAnswer.setBlocking(blocking ? "Y" : "N");
        return this;
    }

    public ContractTerminationRequestAnswerBuilder setNote(String note) {
        contractTerminationRequestAnswer.setNote(note);
        return this;
    }

    public ContractTerminationRequestAnswerBuilder setBlockingcode(String blockingcode) {
        contractTerminationRequestAnswer.setBlockingcode(blockingcode);
        return this;
    }

    public ContractTerminationRequestAnswerBuilder setFirstPossibleDate(String firstpossibledate) {
        contractTerminationRequestAnswer.setFirstpossibledate(firstpossibledate);
        return this;
    }

    public ContractTerminationRequestAnswerBuilder setFirstPossibleDate(LocalDateTime firstpossibledate) {
        contractTerminationRequestAnswer.setFirstpossibledate(firstpossibledate.withNano(0).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        return this;
    }

    public ContractTerminationRequestAnswerBuilder setInfraBlock(String infraprovider, String infratype, String infraid) {
        InfraBlock infra = new InfraBlock();
        infra.setInfraprovider(infraprovider);
        infra.setInfratype(infratype);
        infra.setInfraid(infraid);
        contractTerminationRequestAnswer.setInfrablock(infra);
        return this;
    }

    public Message<ContractTerminationRequestAnswerMessage> build() {
        ContractTerminationRequestAnswerBody body = new ContractTerminationRequestAnswerBody()
                .contractterminationrequestanswer(contractTerminationRequestAnswer);

        ContractTerminationRequestAnswerMessage message = new ContractTerminationRequestAnswerMessage()
                .header(header)
                .body(body);

        return new Message<>(message, MessageType.CONTRACT_TERMINATION_REQUEST_ANSWER_V5);
    }
}
