package nl.coin.sdk.bs.messages.v5;

public enum MessageType {

    CANCEL_V5("cancel"),
    CONTRACT_TERMINATION_REQUEST_V5("contractterminationrequest"),
    CONTRACT_TERMINATION_REQUEST_ANSWER_V5("contractterminationrequestanswer"),
    CONTRACT_TERMINATION_PERFORMED_V5("contractterminationperformed"),
    ERROR_FOUND_V5("errorfound");

    final private String type;
    final private String eventName;

    private static final String VERSION_SUFFIX_V5 = "-v5";

    MessageType(String type) {
        this.type = type;
        this.eventName = type + VERSION_SUFFIX_V5;
    }

    public String getType() {
        return type;
    }

    public String getEventName() {
        return eventName;
    }

}
