/*
 * COIN Bundle Switching Rest API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package nl.coin.sdk.bs.messages.v5.contractterminationrequest;

import java.util.Objects;

/**
 * AddressBlock
 */
public class AddressBlock {
    private String postcode = null;

    private String housenr = null;

    private String housenr_ext = null;

    public AddressBlock postcode(String postcode) {
        this.postcode = postcode;
        return this;
    }

    /**
     * Contract Postcode, that is requested to be terminated
     *
     * @return postcode
     **/
    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public AddressBlock housenr(String housenr) {
        this.housenr = housenr;
        return this;
    }

    /**
     * Contract House number that is requested to be terminated
     *
     * @return housenr
     **/
    public String getHousenr() {
        return housenr;
    }

    public void setHousenr(String housenr) {
        this.housenr = housenr;
    }

    public AddressBlock housenrExt(String housenrExt) {
        this.housenr_ext = housenrExt;
        return this;
    }

    /**
     * Contract House number that is requested to be terminated
     *
     * @return housenrExt
     **/

    public String getHousenr_ext() {
        return housenr_ext;
    }

    public void setHousenr_ext(String housenr_ext) {
        this.housenr_ext = housenr_ext;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddressBlock addressBlock = (AddressBlock) o;
        return Objects.equals(this.postcode, addressBlock.postcode) &&
            Objects.equals(this.housenr, addressBlock.housenr) &&
            Objects.equals(this.housenr_ext, addressBlock.housenr_ext);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postcode, housenr, housenr_ext);
    }


    @Override
    public String toString() {

        return "class AddressBlock {\n" +
            "    postcode: " + toIndentedString(postcode) + "\n" +
            "    housenr: " + toIndentedString(housenr) + "\n" +
            "    housenrExt: " + toIndentedString(housenr_ext) + "\n" +
            "}";
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}

