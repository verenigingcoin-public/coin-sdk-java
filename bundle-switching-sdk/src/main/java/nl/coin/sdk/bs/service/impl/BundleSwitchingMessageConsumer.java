package nl.coin.sdk.bs.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.coin.sdk.bs.messages.v5.BundleSwitchingMessage;
import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.MessageType;
import nl.coin.sdk.bs.messages.v5.cancel.CancelMessage;
import nl.coin.sdk.bs.messages.v5.contractterminationperformed.ContractTerminationPerformedMessage;
import nl.coin.sdk.bs.messages.v5.contractterminationrequest.ContractTerminationRequestMessage;
import nl.coin.sdk.bs.messages.v5.contractterminationrequestanswer.ContractTerminationRequestAnswerMessage;
import nl.coin.sdk.bs.messages.v5.errorfound.ErrorFoundMessage;
import nl.coin.sdk.bs.service.IBundleSwitchingMessageListener;
import nl.coin.sdk.common.client.IOffsetPersister;
import nl.coin.sdk.common.client.SseConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

@Service
public class BundleSwitchingMessageConsumer {

    private final SseConsumer sseConsumer;
    private final ObjectMapper mapper;

    @Autowired
    public BundleSwitchingMessageConsumer(SseConsumer sseConsumer, ObjectMapper mapper) {
        this.sseConsumer = sseConsumer;
        this.mapper = mapper;
    }

    /**
     * Recommended method for consuming messages. On connect or reconnect
     * it will consume all unconfirmed messages.
     */
    public void startConsumingUnconfirmed(
        IBundleSwitchingMessageListener listener,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmed(
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Method for consuming messages. On connect or reconnect
     * it will consume all unconfirmed messages.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmed(
        IBundleSwitchingMessageListener listener,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmed(
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Method for consuming messages. On connect or reconnect
     * it will consume all unconfirmed messages for the provided service providers.
     * <br><br>
     * Use this method rather than {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, Runnable, MessageType...)} if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     */
    public void startConsumingUnconfirmed(
        IBundleSwitchingMessageListener listener,
        List<String> serviceProviders,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmed(
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Method for consuming messages. On connect or reconnect
     * it will consume all unconfirmed messages for the provided service providers.
     * <br><br>
     * Use this method rather than {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, MessageType...)} if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, List, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmed(
        IBundleSwitchingMessageListener listener,
        List<String> serviceProviders,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmed(
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, Runnable, MessageType...)} does not meet needs.
     */
    public void startConsumingAll(
        IBundleSwitchingMessageListener listener,
        IOffsetPersister offsetPersister,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingAll(
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, MessageType...)} does not meet needs.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingAll(IBundleSwitchingMessageListener, IOffsetPersister, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingAll(
        IBundleSwitchingMessageListener listener,
        IOffsetPersister offsetPersister,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingAll(
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, List, Runnable, MessageType...)} does not meet needs.
     * <br><br>
     * Use this method rather than {@link #startConsumingAll(IBundleSwitchingMessageListener, IOffsetPersister, Runnable, MessageType...)} if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     */
    public void startConsumingAll(
        IBundleSwitchingMessageListener listener,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingAll(
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, List, MessageType...)} does not meet needs.
     * <br><br>
     * Use this method rather than {@link #startConsumingAll(IBundleSwitchingMessageListener, IOffsetPersister, MessageType...)} if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingAll(IBundleSwitchingMessageListener, IOffsetPersister, List, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingAll(
        IBundleSwitchingMessageListener listener,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingAll(
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed, from a certain offset.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, Runnable, MessageType...)} does not meet needs.
     */
    public void startConsumingAll(
        IBundleSwitchingMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingAll(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed, from a certain offset.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, MessageType...)} does not meet needs.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingAll(IBundleSwitchingMessageListener, String, IOffsetPersister, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingAll(
        IBundleSwitchingMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingAll(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed, from a certain offset.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, List, Runnable, MessageType...)} does not meet needs.
     * <br><br>
     * Use this method rather than {@link #startConsumingAll(IBundleSwitchingMessageListener, String, IOffsetPersister, Runnable, MessageType...)}
     * if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     */
    public void startConsumingAll(
        IBundleSwitchingMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingAll(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed, from a certain offset.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, List, MessageType...)} does not meet needs.
     * <br><br>
     * Use this method rather than {@link #startConsumingAll(IBundleSwitchingMessageListener, String, IOffsetPersister, MessageType...)}
     * if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingAll(IBundleSwitchingMessageListener, String, IOffsetPersister, List, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingAll(
        IBundleSwitchingMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingAll(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, Runnable, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     */
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IBundleSwitchingMessageListener listener,
        IOffsetPersister offsetPersister,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmedWithOffsetPersistence(IBundleSwitchingMessageListener, IOffsetPersister, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IBundleSwitchingMessageListener listener,
        IOffsetPersister offsetPersister,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, List, Runnable, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     * <br><br>
     * Use this method rather than {@link #startConsumingUnconfirmedWithOffsetPersistence(IBundleSwitchingMessageListener, IOffsetPersister, Runnable, MessageType...)} if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     */
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IBundleSwitchingMessageListener listener,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, List, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     * <br><br>
     * Use this method rather than {@link #startConsumingUnconfirmedWithOffsetPersistence(IBundleSwitchingMessageListener, IOffsetPersister, MessageType...)} if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmedWithOffsetPersistence(IBundleSwitchingMessageListener, IOffsetPersister, List, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IBundleSwitchingMessageListener listener,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, List, Runnable, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     */
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IBundleSwitchingMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, List, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmedWithOffsetPersistence(IBundleSwitchingMessageListener, String, IOffsetPersister, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IBundleSwitchingMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, List, Runnable, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     * <br><br>
     * Use this method rather than {@link #startConsumingUnconfirmedWithOffsetPersistence(IBundleSwitchingMessageListener, String, IOffsetPersister, Runnable, MessageType...)}
     * if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     */
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IBundleSwitchingMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IBundleSwitchingMessageListener, List, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     * <br><br>
     * Use this method rather than {@link #startConsumingUnconfirmedWithOffsetPersistence(IBundleSwitchingMessageListener, String, IOffsetPersister, MessageType...)}
     * if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmedWithOffsetPersistence(IBundleSwitchingMessageListener, String, IOffsetPersister, List, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IBundleSwitchingMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    public void stopConsuming() {
        sseConsumer.stopConsuming();
    }

    private List<String> toStrings(MessageType... messageTypes) {
        return Arrays.stream(messageTypes).map(MessageType::getType).collect(Collectors.toList());
    }

    private LinkedMultiValueMap<String, String> toMap(String key, List<String> values) {
        LinkedMultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.put(key, values);
        return map;
    }

    protected String handleSSE(IBundleSwitchingMessageListener listener, List<ServerSentEvent<String>> sses) {
        if (sses.size() != 1) throw new RuntimeException("Stream buffer size should be 1");
        ServerSentEvent<String> sse = sses.get(0);
        String event = sse.event();
        String messageId = sse.id();
        String data = sse.data();
        try {
            if (MessageType.CONTRACT_TERMINATION_REQUEST_V5.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<ContractTerminationRequestMessage>>() {
                }, listener::onContractTerminationRequest);
            } else if (MessageType.CONTRACT_TERMINATION_REQUEST_ANSWER_V5.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<ContractTerminationRequestAnswerMessage>>() {
                }, listener::onContractTerminationRequestAnswer);
            } else if (MessageType.CONTRACT_TERMINATION_PERFORMED_V5.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<ContractTerminationPerformedMessage>>() {
                }, listener::onContractTerminationPerformed);
            } else if (MessageType.CANCEL_V5.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<CancelMessage>>() {
                }, listener::onCancel);
            } else if (MessageType.ERROR_FOUND_V5.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<ErrorFoundMessage>>() {
                }, listener::onErrorFound);
            } else if (sse.event() != null) {
                Message message = mapper.readValue(data, new TypeReference<Message>() {
                });
                listener.onUnknownMessage(messageId, message);
                return null;
            } else {
                listener.onKeepAlive();
                return null;
            }
            return messageId;
        } catch (IOException exception) {
            listener.onException(exception);
            return null;
        }
    }

    protected <T extends BundleSwitchingMessage> void handleEvent(
        String messageId, String data, TypeReference<Message<T>> typeReference, BiConsumer<String, T> consumer) throws IOException {
        Message<T> message = mapper.readValue(data, typeReference);
        consumer.accept(messageId, message.getMessage());
    }
}

