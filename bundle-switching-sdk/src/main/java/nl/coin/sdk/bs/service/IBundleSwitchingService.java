package nl.coin.sdk.bs.service;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.common.MessageResponse;

public interface IBundleSwitchingService {

    void sendConfirmation(String id);

    MessageResponse sendMessage(Message message);
}
