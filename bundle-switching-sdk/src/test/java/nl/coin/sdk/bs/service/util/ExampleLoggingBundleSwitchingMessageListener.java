package nl.coin.sdk.bs.service.util;

import nl.coin.sdk.bs.messages.v5.BundleSwitchingMessage;
import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.MessageType;
import nl.coin.sdk.bs.messages.v5.cancel.CancelMessage;
import nl.coin.sdk.bs.messages.v5.contractterminationperformed.ContractTerminationPerformedMessage;
import nl.coin.sdk.bs.messages.v5.contractterminationrequest.ContractTerminationRequestMessage;
import nl.coin.sdk.bs.messages.v5.contractterminationrequestanswer.ContractTerminationRequestAnswerMessage;
import nl.coin.sdk.bs.messages.v5.errorfound.ErrorFoundMessage;
import nl.coin.sdk.bs.service.IBundleSwitchingMessageListener;
import nl.coin.sdk.bs.service.impl.BundleSwitchingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class ExampleLoggingBundleSwitchingMessageListener implements IBundleSwitchingMessageListener {
    private final BundleSwitchingService bundleSwitchingService;
    private StreamTestResultTracker resultTracker;

    @Autowired
    public ExampleLoggingBundleSwitchingMessageListener(
            BundleSwitchingService bundleSwitchingService) {
        this.bundleSwitchingService = bundleSwitchingService;
    }

    private CountDownLatch latch;

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }

    public void setResultTracker(StreamTestResultTracker resultTracker) {
        this.resultTracker = resultTracker;
    }

    @Override
    public void onKeepAlive() {
    }

    @Override
    public void onUnknownMessage(String messageId, Message message) {

    }

    @Override
    public void onException(Exception exception) {

    }

    @Override
    public void onContractTerminationRequest(String messageId, ContractTerminationRequestMessage message) {
        setMessageAsReceivedInResult(MessageType.CONTRACT_TERMINATION_REQUEST_V5);
        processMessage(messageId, message);
    }

    void setMessageAsReceivedInResult(MessageType contractterminationRequestV1) {
        if (this.resultTracker != null) {
            this.resultTracker.setResult(contractterminationRequestV1.getType(), Boolean.TRUE);
        }
    }

    @Override
    public void onContractTerminationRequestAnswer(String messageId, ContractTerminationRequestAnswerMessage message) {
        setMessageAsReceivedInResult(MessageType.CONTRACT_TERMINATION_REQUEST_ANSWER_V5);
        processMessage(messageId, message);
    }

    @Override
    public void onContractTerminationPerformed(String messageId, ContractTerminationPerformedMessage message) {
        setMessageAsReceivedInResult(MessageType.CONTRACT_TERMINATION_PERFORMED_V5);
        processMessage(messageId, message);
    }

    @Override
    public void onCancel(String messageId, CancelMessage message) {
        setMessageAsReceivedInResult(MessageType.CANCEL_V5);
        processMessage(messageId, message);
    }

    @Override
    public void onErrorFound(String messageId, ErrorFoundMessage message) {
        setMessageAsReceivedInResult(MessageType.ERROR_FOUND_V5);
        processMessage(messageId, message);
    }

    private void processMessage(String messageId, BundleSwitchingMessage message) {
        System.out.println("message = " + message.toString());
        bundleSwitchingService.sendConfirmation(messageId);
        if (latch != null) latch.countDown();
    }
}
