package nl.coin.sdk.bs.service;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.contractterminationrequest.ContractTerminationRequestBuilder;
import nl.coin.sdk.bs.messages.v5.contractterminationrequest.ContractTerminationRequestMessage;
import nl.coin.sdk.bs.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;

public class ContractTerminationRequestBuilderTest {

    @Test
    public void createContractTerminationRequestFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        ContractTerminationRequestBuilder contractterminationRequestBuilder = new ContractTerminationRequestBuilder()
                .setHeader("LOADA", "LOADB")
                .setRecipientserviceprovider("LOADB")
                .setRecipientnetworkoperator("LOADB")
                .setDonornetworkoperator("LOADA")
                .setDonorserviceprovider("LOADA")
                .setDossierId(dossierId)
                .setAddress("1234AB", "1")
                .setNote("This is a note");
        for (int i = 0; i < 250; i++) {
            String lastDigits = String.format("%03d", i);
            String phoneNumber = "0612345" + lastDigits;
            contractterminationRequestBuilder.addNumberSeries(phoneNumber, phoneNumber);
        }
        Message<ContractTerminationRequestMessage> message = contractterminationRequestBuilder.build();

        Assert.assertEquals("Donor serviceprovider should be LOADA", "LOADA", message.getMessage().getBody().getContractterminationrequest().getDonorserviceprovider());
    }
}
