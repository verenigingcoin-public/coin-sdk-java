package nl.coin.sdk.bs.service;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.MessageType;
import nl.coin.sdk.bs.messages.v5.common.MessageResponse;
import nl.coin.sdk.bs.messages.v5.contractterminationperformed.ContractTerminationPerformedBuilder;
import nl.coin.sdk.bs.messages.v5.contractterminationperformed.ContractTerminationPerformedMessage;
import nl.coin.sdk.bs.service.impl.BundleSwitchingService;
import nl.coin.sdk.bs.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class ContractTerminationPerformedTest {

    @Autowired
    private BundleSwitchingService bundleSwitchingService;

    @Test
    public void sendContractTerminationPerformedMinimal() {
        sendAndAssertMessage(createContractTerminationPerformedMinimal());
    }

    @Test
    public void sendContractTerminationPerformedFull() {
        sendAndAssertMessage(createContractTerminationPerformedFull());
    }

    private Message<ContractTerminationPerformedMessage> createContractTerminationPerformedMinimal() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new ContractTerminationPerformedBuilder()
            .setHeader("LOADA", "LOADB")
            .setDossierId(dossierId)
                .setNote("NOTE")
            .build();
    }

    private Message<ContractTerminationPerformedMessage> createContractTerminationPerformedFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        ContractTerminationPerformedBuilder contractterminationPerformedBuilder = new ContractTerminationPerformedBuilder()
            .setHeader("LOADA", "LOADB")
            .setDossierId(dossierId)
            .setMessageId("dsgsdf-234")
                .setNote("NOTE")
                .setActualDatetime(LocalDateTime.now().withNano(0).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        return contractterminationPerformedBuilder.build();
    }

    private void sendAndAssertMessage(Message<ContractTerminationPerformedMessage> message) {
        Assert.assertEquals(MessageType.CONTRACT_TERMINATION_PERFORMED_V5, message.getMessageType());
        MessageResponse messageResponse = bundleSwitchingService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
        assertThat(message.getMessage().getBody().getContractterminationperformed().getNote()).isEqualTo("NOTE");
    }
}
