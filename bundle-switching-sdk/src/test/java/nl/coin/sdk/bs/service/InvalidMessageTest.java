package nl.coin.sdk.bs.service;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.contractterminationrequest.ContractTerminationRequestBuilder;
import nl.coin.sdk.bs.messages.v5.contractterminationrequest.ContractTerminationRequestMessage;
import nl.coin.sdk.bs.service.impl.BundleSwitchingService;
import nl.coin.sdk.bs.service.impl.RestClientErrorException;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class InvalidMessageTest {

    @Autowired
    private BundleSwitchingService bundleSwitchingService;

    @Test
    public void sendInvalidMessage() {
        Message<ContractTerminationRequestMessage> message = new ContractTerminationRequestBuilder()
            .addNumberSeries("0612345678", "0612345678")
            .addValidationBlock("name", "value")
            .setAddress("1234AB", "23")
            .setRecipientserviceprovider("LOADA")
            .setDonorserviceprovider("LOADB")
            .setName("a name")
            .setEarlytermination(true)
            .build();
        try {
            bundleSwitchingService.sendMessage(message);
        } catch (HttpClientErrorException e) {
            Assert.assertFalse(((RestClientErrorException) e).getTransactionId().isEmpty());
            Assert.assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
            Assert.assertThat(
                e.getMessage(),
                CoreMatchers.containsString("1000: Provide mandatory field message.header.receiver")
            );
        }
    }
}
