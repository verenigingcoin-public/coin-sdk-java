package nl.coin.sdk.bs.service;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.MessageType;
import nl.coin.sdk.bs.messages.v5.common.MessageResponse;
import nl.coin.sdk.bs.messages.v5.contractterminationrequest.ContractTerminationRequestBuilder;
import nl.coin.sdk.bs.messages.v5.contractterminationrequest.ContractTerminationRequestMessage;
import nl.coin.sdk.bs.service.impl.BundleSwitchingService;
import nl.coin.sdk.bs.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class ContractTerminationRequestTest {

    @Autowired
    private BundleSwitchingService bundleSwitchingService;

    @Test
    public void sendContractTerminationRequestMinimal() {
        sendMessage(createContractTerminationRequestMinimal());
    }

    @Test
    public void sendContractTerminationRequestFull() {
        sendMessage(createContractTerminationRequestFull());
    }

    private Message<ContractTerminationRequestMessage> createContractTerminationRequestMinimal() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new ContractTerminationRequestBuilder()
            .setHeader("LOADA", "LOADB")
            .setRecipientserviceprovider("LOADA")
            .setDonorserviceprovider("LOADB")
            .setEarlytermination(false)
            .setName("name")
            .setAddress("1234AB", "11", "A")
            .setDossierId(dossierId)
            .addNumberSeries("0612345678", "0612345678")
            .addValidationBlock("iban", "123")
            .build();
    }

    private Message<ContractTerminationRequestMessage> createContractTerminationRequestFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        ContractTerminationRequestBuilder contractterminationRequestBuilder = new ContractTerminationRequestBuilder()
            .setHeader("LOADA", "LOADB")
            .setRecipientserviceprovider("LOADB")
            .setRecipientnetworkoperator("LOADB")
            .setDonornetworkoperator("LOADA")
            .setDonorserviceprovider("LOADA")
            .setDossierId(dossierId)
            .setEarlytermination(false)
            .setName("name")
            .setAddress("1234AB", "11", "A")
            .setNote("This is a note")
            .addValidationBlock("name", "value");
        for (int i = 0; i < 250; i++) {
            String lastDigits = String.format("%03d", i);
            String phoneNumber = "0612345" + lastDigits;
            contractterminationRequestBuilder.addNumberSeries(phoneNumber, phoneNumber);
        }
        return contractterminationRequestBuilder.build();
    }

    private void sendMessage(Message<ContractTerminationRequestMessage> message) {
        Assert.assertEquals(MessageType.CONTRACT_TERMINATION_REQUEST_V5, message.getMessageType());
        MessageResponse messageResponse = bundleSwitchingService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
