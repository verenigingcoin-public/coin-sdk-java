package nl.coin.sdk.bs.service.util;

public class TestUtil {
    public static String generateRandomDossierId() {
        return generateRandomDossierId("ABC");
    }

    public static String generateRandomDossierId(String operatorCode) {
        return operatorCode + "-A-" + (Math.round(Math.random() * 10000) + 9999) + "-0";
    }
}
