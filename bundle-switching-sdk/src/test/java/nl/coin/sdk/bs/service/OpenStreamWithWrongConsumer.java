package nl.coin.sdk.bs.service;

import nl.coin.sdk.bs.messages.v5.MessageType;
import nl.coin.sdk.bs.service.impl.BundleSwitchingMessageConsumer;
import nl.coin.sdk.bs.service.util.ExampleLoggingBundleSwitchingMessageListener;
import nl.coin.sdk.bs.service.util.InMemoryOffsetPersister;
import nl.coin.sdk.common.client.SseConsumer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.concurrent.CountDownLatch;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class OpenStreamWithWrongConsumer {

    @Autowired
    private SseConsumer sseConsumer;

    @Autowired
    private BundleSwitchingMessageConsumer bundleSwitchingMessageConsumer;

    @Autowired
    private ExampleLoggingBundleSwitchingMessageListener listener;

    @Test(timeout = 20000)
    public void stopListeningWhenConnectFails() throws InterruptedException {
        Object originalValue = ReflectionTestUtils.getField(sseConsumer, "consumerName");
        ReflectionTestUtils.setField(sseConsumer, "consumerName", "ne-consumer");
        CountDownLatch latch = new CountDownLatch(1);
        try {
            bundleSwitchingMessageConsumer.startConsumingAll(listener, new InMemoryOffsetPersister(), latch::countDown,
                MessageType.CONTRACT_TERMINATION_REQUEST_V5,
                MessageType.CANCEL_V5,
                MessageType.CONTRACT_TERMINATION_PERFORMED_V5,
                MessageType.CONTRACT_TERMINATION_REQUEST_ANSWER_V5);
            latch.await();
        } finally {
            ReflectionTestUtils.setField(sseConsumer, "consumerName", originalValue);
        }
    }
}
