package nl.coin.sdk.bs.service;

import nl.coin.sdk.bs.messages.v5.Message;
import nl.coin.sdk.bs.messages.v5.MessageType;
import nl.coin.sdk.bs.messages.v5.common.MessageResponse;
import nl.coin.sdk.bs.messages.v5.contractterminationrequestanswer.ContractTerminationRequestAnswerBuilder;
import nl.coin.sdk.bs.messages.v5.contractterminationrequestanswer.ContractTerminationRequestAnswerMessage;
import nl.coin.sdk.bs.service.impl.BundleSwitchingService;
import nl.coin.sdk.bs.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class ContractTerminationRequestAnswerTest {

    @Autowired
    private BundleSwitchingService bundleSwitchingService;

    @Test
    public void sendContractTerminationRequestAnswerMinimal() {
        sendMessage(createContractTerminationRequestAnswerMinimal());
    }

    @Test
    public void sendContractTerminationRequestAnswerFull() {
        sendMessage(createContractTerminationRequestAnswerFull());
    }

    private Message<ContractTerminationRequestAnswerMessage> createContractTerminationRequestAnswerMinimal() {
        String dossierId = TestUtil.generateRandomDossierId();

        return new ContractTerminationRequestAnswerBuilder()
            .setHeader("LOADA", "LOADB")
            .setDossierId(dossierId)
            .setBlocking(false)
            .setBlockingcode("0")
            .build();
    }

    private Message<ContractTerminationRequestAnswerMessage> createContractTerminationRequestAnswerFull() {
        String dossierId = TestUtil.generateRandomDossierId();

        return new ContractTerminationRequestAnswerBuilder()
            .setHeader(
                "LOADA",
                "LOADA",
                "LOADB",
                "LOADB")
            .setDossierId(dossierId)
            .setBlocking(true)
            .setBlockingcode("43")
            .setFirstPossibleDate(LocalDateTime.now())
            .setInfraBlock("prov", "type", "4")
            .build();
    }

    private void sendMessage(Message<ContractTerminationRequestAnswerMessage> message) {
        Assert.assertEquals(MessageType.CONTRACT_TERMINATION_REQUEST_ANSWER_V5, message.getMessageType());
        MessageResponse messageResponse = bundleSwitchingService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
