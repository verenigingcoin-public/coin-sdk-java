package nl.coin.sdk.bs.service;

import nl.coin.sdk.bs.messages.v5.MessageType;
import nl.coin.sdk.bs.service.impl.BundleSwitchingMessageConsumer;
import nl.coin.sdk.bs.service.util.ExampleLoggingBundleSwitchingMessageListener;
import nl.coin.sdk.bs.service.util.InMemoryOffsetPersister;
import nl.coin.sdk.bs.service.util.StreamTestResultTracker;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.concurrent.CountDownLatch;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class OpenStreamTest {

    @Autowired
    private BundleSwitchingMessageConsumer bundleSwitchingMessageConsumer;

    @Autowired
    private ExampleLoggingBundleSwitchingMessageListener listener;

    @AfterEach
    public void stopStream() {
        bundleSwitchingMessageConsumer.stopConsuming();
    }

    @Test(timeout = 20000)
    public void openStream() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(5);
        listener.setLatch(latch);
        bundleSwitchingMessageConsumer.startConsumingUnconfirmed(listener, Assertions::fail);
        latch.await();
    }

    @Test(timeout = 30000)
    public void openStreamAndHandleAllMessageTypes() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(30);
        listener.setLatch(latch);

        StreamTestResultTracker tracker = new StreamTestResultTracker();
        listener.setResultTracker(tracker);
        bundleSwitchingMessageConsumer.startConsumingUnconfirmedWithOffsetPersistence(listener, new InMemoryOffsetPersister(), Assertions::fail);
        latch.await();

        Assert.assertFalse("For at least one message the handler isn't called: " + tracker.getResults(), tracker.getResults().containsValue(Boolean.FALSE));
    }

    @Test(timeout = 20000)
    public void openSpecificStreamForAllMessagesAndOneProvider() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(5);
        listener.setLatch(latch);
        bundleSwitchingMessageConsumer.startConsumingAll(listener, new InMemoryOffsetPersister(),
            Collections.singletonList("LOADA"),
            () -> {},
            MessageType.CONTRACT_TERMINATION_REQUEST_V5,
            MessageType.CANCEL_V5,
            MessageType.CONTRACT_TERMINATION_PERFORMED_V5,
            MessageType.CONTRACT_TERMINATION_REQUEST_ANSWER_V5);
        latch.await();
    }
}
