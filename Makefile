include Makefile.mk

ALL=common-sdk number-portability-sdk number-portability-sdk-samples bundle-switching-sdk bundle-switching-sdk-samples weas-migration-sdk weas-migration-sdk-samples mobile-connect-sdk mobile-connect-sdk-samples
ALL_DEPLOY=common-sdk number-portability-sdk bundle-switching-sdk weas-migration-sdk mobile-connect-sdk
IS_RELEASE := $(shell . $(RELEASE_SUPPORT) ; differsFromRelease || echo "release")
CI_JOB_ID ?=local

tag-patch-release: VERSION := $(shell . $(RELEASE_SUPPORT); nextPatchLevel)
tag-patch-release: tag

tag-minor-release: VERSION := $(shell . $(RELEASE_SUPPORT); nextMinorLevel)
tag-minor-release: tag

tag-major-release: VERSION := $(shell . $(RELEASE_SUPPORT); nextMajorLevel)
tag-major-release: tag

tag: TAG=$(shell . $(RELEASE_SUPPORT); getTag $(VERSION))
tag: SNAPSHOT_VERSION = $(shell . $(RELEASE_SUPPORT); nextPatchLevel $(VERSION))-SNAPSHOT
tag: CURRENT_VERSION = $(shell grep "<coin.sdk.version>" pom.xml | grep -o "[0-9]*\.[0-9]*\.[0-9]*\(-[a-z0-9A-Z]*\)\?")
tag: check-status
	@. $(RELEASE_SUPPORT) ; ! tagExists $(TAG) || (echo "ERROR: tag $(TAG) for version $(VERSION) already tagged in git" >&2 && exit 1) ;
	@. $(RELEASE_SUPPORT) ; setRelease $(VERSION)

	#Deploy Release
	mvn versions:set -DnewVersion=$(VERSION) -DgenerateBackupPoms=false
	mvn versions:set-property -Dproperty=coin.sdk.version -DnewVersion=$(VERSION) -DgenerateBackupPoms=false
	find . -name "SdkInfo.java" | xargs sed -i.tmp -E "s/(coin-sdk-java)-[0-9]+\.[0-9]+\.[0-9]+(-SNAPSHOT)?/\1-$(VERSION)/"
	find . -type f -name "*.tmp" | xargs rm
	git add .
	git commit -m "bumped to version $(VERSION)" ;
	git tag $(TAG) ;
	@ if [ -n "$(shell git remote -v)" ] ; then git push --tags ; else echo 'no remote to push tags to' ; fi

	#Deploy Snapshot
	find . -type f -name "pom.xml" | xargs sed -i.tmp 's_$(VERSION)_$(SNAPSHOT_VERSION)_'
	mvn versions:set -DnewVersion=$(SNAPSHOT_VERSION) -DgenerateBackupPoms=false
	mvn versions:set-property -Dproperty=coin.sdk.version -DnewVersion=$(SNAPSHOT_VERSION) -DgenerateBackupPoms=false
	find . -name "SdkInfo.java" | xargs sed -i.tmp -E "s/(coin-sdk-java)-$(VERSION)/\1-$(SNAPSHOT_VERSION)/"
	find . -type f -name "*.tmp" | xargs rm
	git add .
	git commit -m "bumped to snapshot version $(SNAPSHOT_VERSION)" ;
	@ if [ -n "$(shell git remote -v)" ] ; then git push ; git push --tags ; else echo 'no remote to push tags to' ; fi

version-check-java:
	for DIR in $(ALL); do $(MAKE) -C $$DIR version-check-java ; done

dependency-vulnerability-check-java:
	for DIR in $(ALL); do $(MAKE) -C $$DIR dependency-vulnerability-check-java; done

package: pre-build
	mvn $(MVN_OPTIONS) -B -Ddependency-check.skip=true package

test:
	mvn $(MVN_OPTIONS) -B clean test

pre-push:
	echo "You cannot run the push command in the main directory"
	exit 1

pre-build:
	mvn $(MVN_OPTIONS) -B clean

deploy:
ifdef IS_RELEASE
	@if [[ "$(VERSION)" =~ .*-dirty ]] ; then echo "refusing to deploy a dirty image." && git status -s . && exit 1 ; else exit 0; fi
	@. $(RELEASE_SUPPORT) ; ! tagExists $(TAG) || (echo "Skipping deploy." >&2 && exit 0) ;
	make deploy-mvn-packages
	set -e ; for DIR in $(ALL_DEPLOY); do $(MAKE) -C $$DIR deploy-mvn-packages ; done
else
	@if [[ "$(VERSION)" =~ .*-dirty ]] ; then echo "refusing to deploy a dirty image." && git status -s . && exit 1 ; else exit 0; fi
	@. $(RELEASE_SUPPORT) ; ! tagExists $(TAG) || (echo "Skipping deploy." >&2 && exit 0) ;
	make deploy-snapshot
	set -e ; for DIR in $(ALL_DEPLOY); do $(MAKE) -C $$DIR deploy-mvn-packages ; done
endif

deploy-snapshot:
	mvn $(MVN_OPTIONS) -Dmaven.test.skip=true -Ddependency-check.skip=true -Daether.checksums.algorithms="MD5,SHA-1,SHA-256" deploy

deploy-mvn-packages:
	mvn $(MVN_OPTIONS) -Dmaven.test.skip=true -Ddependency-check.skip=true javadoc:jar source:jar -Daether.checksums.algorithms="MD5,SHA-1,SHA-256" deploy

clean-install:
	mvn $(MVN_OPTIONS) -Dmaven.test.skip=true -Ddependency-check.skip=true clean install
