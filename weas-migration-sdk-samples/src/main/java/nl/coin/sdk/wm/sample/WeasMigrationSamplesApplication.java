package nl.coin.sdk.wm.sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("nl.coin")
@SpringBootApplication
public class WeasMigrationSamplesApplication {

    private static final Logger LOG = LoggerFactory.getLogger(WeasMigrationSamplesApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(WeasMigrationSamplesApplication.class, args);
        LOG.debug("Starting Weas Migration SDK Samples Project");
    }
}
