package nl.coin.sdk.wm.sample;

import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.common.MessageResponse;
import nl.coin.sdk.wm.messages.v1.requestanswer.WeasMigrationRequestAnswerBuilder;
import nl.coin.sdk.wm.messages.v1.requestanswer.WeasMigrationRequestAnswerMessage;
import nl.coin.sdk.wm.service.impl.WeasMigrationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WeasMigrationSamplesApplication.class)
public class WeasMigrationRequestAnswerExample {

    @Value("${sender.serviceprovider.name}")
    private String senderServiceProviderName;

    @Value("${receiver.serviceprovider.name}")
    private String receiverServiceProviderName;

    @Autowired
    private WeasMigrationService weasMigrationService;

    @Test
    public void sendWeasMigrationRequestAnswer() {
        Message<WeasMigrationRequestAnswerMessage> message = createWeasMigrationRequestAnswer();
        MessageResponse messageResponse = weasMigrationService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }

    private Message<WeasMigrationRequestAnswerMessage> createWeasMigrationRequestAnswer() {
        String dossierId = null; // use dossier-id from initial contractterminationrequest
        return new WeasMigrationRequestAnswerBuilder()
            .setHeader(senderServiceProviderName, receiverServiceProviderName)
            .setDossierId(dossierId)
            .setBlocking(false)
            .setBlockingcode("0")
            .setFirstPossibleDate(LocalDateTime.now())
            .setInfraBlock("infraprovider", "infratype", "infraid")
            .build();
    }
}
