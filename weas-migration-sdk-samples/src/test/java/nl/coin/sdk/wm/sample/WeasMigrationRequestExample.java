package nl.coin.sdk.wm.sample;

import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.common.MessageResponse;
import nl.coin.sdk.wm.messages.v1.request.WeasMigrationRequestBuilder;
import nl.coin.sdk.wm.messages.v1.request.WeasMigrationRequestMessage;
import nl.coin.sdk.wm.sample.util.TestUtil;
import nl.coin.sdk.wm.service.impl.WeasMigrationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WeasMigrationSamplesApplication.class)
public class WeasMigrationRequestExample {

    @Value("${sender.serviceprovider.name}")
    private String senderServiceProviderName;

    @Value("${receiver.serviceprovider.name}")
    private String receiverServiceProviderName;

    @Autowired
    private WeasMigrationService weasMigrationService;

    // This testcase only sends the message, there should be a consumer running to listen to the stream
    // and receiving the messages
    @Test
    public void sendWeasMigrationRequest() {
        Message<WeasMigrationRequestMessage> message = createWeasMigrationRequest();
        MessageResponse messageResponse = weasMigrationService.sendMessage(message);
        Assert.assertNotNull(messageResponse.getTransactionId());
    }

    private Message<WeasMigrationRequestMessage> createWeasMigrationRequest() {
        String dossierId = TestUtil.generateDossierId(senderServiceProviderName);

        return new WeasMigrationRequestBuilder()
            .setHeader(senderServiceProviderName, receiverServiceProviderName)
            .setRecipientserviceprovider(senderServiceProviderName)
            .setDonorserviceprovider(receiverServiceProviderName)
            .setDossierId(dossierId)
            .setEarlytermination(false)
            .setName("name")
            .setAddress("1234AB", "11", "A")
            .addNumberSeries("0612345678", "0612345678")
            .addValidationBlock("iban", "123")
            .build();
    }
}
