package nl.coin.sdk.wm.sample;

import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.MessageType;
import nl.coin.sdk.wm.messages.v1.cancel.WeasMigrationCancelBuilder;
import nl.coin.sdk.wm.messages.v1.cancel.WeasMigrationCancelMessage;
import nl.coin.sdk.wm.messages.v1.common.MessageResponse;
import nl.coin.sdk.wm.sample.util.TestUtil;
import nl.coin.sdk.wm.service.impl.WeasMigrationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WeasMigrationSamplesApplication.class)
public class WeasMigrationCancelExample {

    @Value("${sender.serviceprovider.name}")
    private String senderServiceProviderName;

    @Value("${receiver.serviceprovider.name}")
    private String receiverServiceProviderName;

    @Autowired
    private WeasMigrationService weasMigrationService;

    @Test
    public void sendCancel() {
        Message<WeasMigrationCancelMessage> message = createCancel();
        Assert.assertEquals(MessageType.CANCEL_V1, message.getMessageType());
        MessageResponse messageResponse = weasMigrationService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }

    private Message<WeasMigrationCancelMessage> createCancel() {
        String dossierId = TestUtil.generateDossierId(senderServiceProviderName);
        return new WeasMigrationCancelBuilder()
                .setHeader(senderServiceProviderName, receiverServiceProviderName)
                .setDossierId(dossierId)
                .setNote("Reason for cancelling")
                .build();
    }
}
