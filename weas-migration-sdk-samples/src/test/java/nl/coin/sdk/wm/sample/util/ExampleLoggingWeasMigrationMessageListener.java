package nl.coin.sdk.wm.sample.util;

import nl.coin.sdk.wm.messages.v1.WeasMigrationMessage;
import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.cancel.WeasMigrationCancelMessage;
import nl.coin.sdk.wm.messages.v1.request.WeasMigrationRequestMessage;
import nl.coin.sdk.wm.messages.v1.requestanswer.WeasMigrationRequestAnswerMessage;
import nl.coin.sdk.wm.messages.v1.errorfound.WeasMigrationErrorFoundMessage;
import nl.coin.sdk.wm.service.IWeasMigrationMessageListener;
import nl.coin.sdk.wm.service.impl.WeasMigrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class ExampleLoggingWeasMigrationMessageListener implements IWeasMigrationMessageListener {
    private final WeasMigrationService weasMigrationService;

    @Autowired
    public ExampleLoggingWeasMigrationMessageListener(
        WeasMigrationService weasMigrationService
    ) {
        this.weasMigrationService = weasMigrationService;
    }

    private WeasMigrationMessage lastReceivedMessage;
    private CountDownLatch latch;

    public WeasMigrationMessage getLastReceivedMessage() {
        return lastReceivedMessage;
    }

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void onKeepAlive() {
        // If you want to do specific actions on the heartbeat, write your logic here
    }

    @Override
    public void onUnknownMessage(String messageId, Message message) {
        // If you want to do specific actions with an unknown message, write your logic here
    }

    @Override
    public void onException(Exception exception) {
        // Exceptions should be handled or rethrown here, otherwise a message may be lost
    }

    @Override
    public void onRequest(String messageId, WeasMigrationRequestMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onRequestAnswer(String messageId, WeasMigrationRequestAnswerMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onCancel(String messageId, WeasMigrationCancelMessage message) {
        processMessage(messageId, message);
    }

    @Override
    public void onErrorFound(String messageId, WeasMigrationErrorFoundMessage message) {
        processMessage(messageId, message);
    }

    private void processMessage(String messageId, WeasMigrationMessage message) {
        lastReceivedMessage = message;
        System.out.println("#########################################################");
        System.out.println("messageId: " + messageId);
        System.out.println("dossierId: " + message.getDossierid());
        System.out.println("header: " + message.getHeader());
        System.out.println("body: " + message.getBody());
        weasMigrationService.sendConfirmation(messageId);
        if (latch != null) latch.countDown();
    }
}
