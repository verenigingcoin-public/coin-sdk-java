package nl.coin.sdk.wm.sample;

import nl.coin.sdk.wm.messages.v1.MessageType;
import nl.coin.sdk.wm.sample.util.ExampleInMemoryOffsetPersister;
import nl.coin.sdk.wm.sample.util.ExampleLoggingWeasMigrationMessageListener;
import nl.coin.sdk.wm.service.impl.WeasMigrationMessageConsumer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CountDownLatch;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WeasMigrationSamplesApplication.class)
public class OpenStreamExamples {

    @Autowired
    private WeasMigrationMessageConsumer weasMigrationMessageConsumer;

    @Autowired
    private ExampleLoggingWeasMigrationMessageListener listener;

    private final Runnable doAfterAllRetriesFailed = () -> {
        // Make sure someone is notified about this fatal error.
        // Also consider stopping the application completely in some way.
    };

    @Test
    public void openStream() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(100);
        listener.setLatch(latch);
        weasMigrationMessageConsumer.startConsumingUnconfirmed(listener, doAfterAllRetriesFailed);
        latch.await();
    }

    @Test
    public void openSpecificStream() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(100);
        listener.setLatch(latch);
        weasMigrationMessageConsumer.startConsumingUnconfirmed(listener, doAfterAllRetriesFailed, MessageType.REQUEST_V1, MessageType.CANCEL_V1);
        latch.await();
    }

    @Test
    public void openSpecificStreamForAllMessages() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(100);
        listener.setLatch(latch);
        weasMigrationMessageConsumer.startConsumingAll(listener, new ExampleInMemoryOffsetPersister(), doAfterAllRetriesFailed, MessageType.REQUEST_V1);
        latch.await();
    }
}
