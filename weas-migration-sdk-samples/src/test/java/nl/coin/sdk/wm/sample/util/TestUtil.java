package nl.coin.sdk.wm.sample.util;

public class TestUtil {

    public static String generateDossierId(String operatorCode) {
        return operatorCode + "-A-" + (Math.round(Math.random() * 10000) + 9999) + "-0";
    }

}
