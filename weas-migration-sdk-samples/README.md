# COIN Weas Migration Java SDK Samples

For extensive documentation of the Java Weas Migration SDK, please have a look at the following [README](https://gitlab.com/verenigingcoin-public/coin-sdk-java/blob/master/weas-migration-sdk/README.md).

# Configure Examples
Besides the credentials configuration (see link above), the examples require a sender and receiver provider to be configured.

The `/src/test/resources/application-test.properties` properties file contains:

```properties
# Sender / Receiver Config
# ---------------
# Sender configuration
sender.serviceprovider.name=<configure-sender-service-provider-name!!!>

# Receiver configuration
receiver.serviceprovider.name=<configure-receiver-service-provider-name!!!>
```
