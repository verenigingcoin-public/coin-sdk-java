# Changelog

## Version 3.0.2

Changed:

- started using package manager of gitlab https://gitlab.com/verenigingcoin-public/coin-sdk-java/-/packages/ to manage packages and accessibility for java sdk
- Changed READMEs of all SDKs to reflect new status

## Version 3.0.0

Changed:

- Effective JDK change to JDK17

## Version 2.0.0

Changed:

- Updated Spring Boot version from 2.7 to 3.1
- Required Java version is now at least Java 17

## Version 1.7.0

Changed:

- Improved error handling in the `SseConsumer`
- All `SseConsumer.startConsuming...()` methods now require a `Runnable doAfterAllRetriesFailed` parameter.
Instead of interrupting the calling thread, this runnable is called when all stream consumption retries have failed.

Deprecated:

- All old `...MessageConsumer.startConsuming...()` methods

Added:

- new `...MessageConsumer.startConsuming...()` methods with an additional parameter `Runnable doAfterAllRetriesFailed`.
  Instead of interrupting the calling thread, this runnable is called when all stream consumption retries have failed.

## Version 1.6.2

Added:

- Support for event stream messages larger than 256K (max is now 10M)

## Version 1.6.0

Added:

- Support for Bundle Switching v5; removes the business field from contract termination requests
  and contract termination request answers

## Version 1.5.0

Added:

- If a Spring bean of type `org.springframework.transaction.support.TransactionTemplate` is found,
handling an SSE and persisting its id with an `IOffsetPersister` is done in one transaction.

## Version 1.4.4

Added:

- updated log4j included in spring-boot to 2.16.0

## Version 1.4.0

Added:

- Support for Number Portability v3; adds the contract field to porting requests
