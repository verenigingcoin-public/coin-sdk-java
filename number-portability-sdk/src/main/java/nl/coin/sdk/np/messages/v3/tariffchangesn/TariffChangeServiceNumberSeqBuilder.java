package nl.coin.sdk.np.messages.v3.tariffchangesn;

import nl.coin.sdk.np.messages.v3.IPortingSeqCreator;
import nl.coin.sdk.np.messages.v3.common.NumberSeries;

public class TariffChangeServiceNumberSeqBuilder {

    private final TariffChangeServiceNumberSeq tariffChangeServiceNumberSeq = new TariffChangeServiceNumberSeq();
    private IPortingSeqCreator<TariffChangeServiceNumberBuilder, TariffChangeServiceNumberSeq> finishFunction;

    public TariffChangeServiceNumberSeqBuilder setNumberSeries(String startNumber, String endNumber) {
        NumberSeries numberSeries = new NumberSeries()
                .start(startNumber)
                .end(endNumber);
        this.tariffChangeServiceNumberSeq.setNumberseries(numberSeries);
        return this;
    }

    public TariffChangeServiceNumberSeqBuilder setTariffInfo(String peak, String offPeak, TariffInfo.CurrencyEnum currency, String type, String vat) {
        TariffInfo tariffInfo = new TariffInfo();
        tariffInfo.setPeak(peak);
        tariffInfo.setOffpeak(offPeak);
        tariffInfo.setCurrency(currency);
        tariffInfo.setType(type);
        tariffInfo.setVat(vat);
        this.tariffChangeServiceNumberSeq.setTariffinfonew(tariffInfo);
        return this;
    }

    TariffChangeServiceNumberSeqBuilder onFinish(IPortingSeqCreator<TariffChangeServiceNumberBuilder, TariffChangeServiceNumberSeq> f) {
        finishFunction = f;
        return this;
    }

    public TariffChangeServiceNumberBuilder finish() {
        return finishFunction.setBuild(this.tariffChangeServiceNumberSeq);
    }
}
