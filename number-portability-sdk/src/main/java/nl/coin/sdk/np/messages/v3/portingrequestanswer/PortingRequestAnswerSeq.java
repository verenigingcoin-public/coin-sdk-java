/*
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package nl.coin.sdk.np.messages.v3.portingrequestanswer;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.oas.annotations.media.Schema;
import nl.coin.sdk.np.messages.v3.common.NumberSeries;

import java.util.Objects;

/**
 * PortingRequestAnswerSeq
 */

@jakarta.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2021-04-19T08:22:26.566049+02:00[Europe/Amsterdam]")
public class PortingRequestAnswerSeq {
  @JsonProperty("numberseries")
  private NumberSeries numberseries = null;

  @JsonProperty("blockingcode")
  private String blockingcode = null;

  @JsonProperty("firstpossibledate")
  private String firstpossibledate = null;

  @JsonProperty("note")
  private String note = null;

  @JsonProperty("donornetworkoperator")
  private String donornetworkoperator = null;

  @JsonProperty("donorserviceprovider")
  private String donorserviceprovider = null;

  public PortingRequestAnswerSeq numberseries(NumberSeries numberseries) {
    this.numberseries = numberseries;
    return this;
  }

   /**
   * Get numberseries
   * @return numberseries
  **/
  @Schema(required = true, description = "")
  public NumberSeries getNumberseries() {
    return numberseries;
  }

  public void setNumberseries(NumberSeries numberseries) {
    this.numberseries = numberseries;
  }

  public PortingRequestAnswerSeq blockingcode(String blockingcode) {
    this.blockingcode = blockingcode;
    return this;
  }

   /**
   * Get blockingcode
   * @return blockingcode
  **/
  @Schema(example = "63", description = "")
  public String getBlockingcode() {
    return blockingcode;
  }

  public void setBlockingcode(String blockingcode) {
    this.blockingcode = blockingcode;
  }

  public PortingRequestAnswerSeq firstpossibledate(String firstpossibledate) {
    this.firstpossibledate = firstpossibledate;
    return this;
  }

   /**
   * Get firstpossibledate
   * @return firstpossibledate
  **/
  @Schema(example = "20190926093223", description = "")
  public String getFirstpossibledate() {
    return firstpossibledate;
  }

  public void setFirstpossibledate(String firstpossibledate) {
    this.firstpossibledate = firstpossibledate;
  }

  public PortingRequestAnswerSeq note(String note) {
    this.note = note;
    return this;
  }

   /**
   * Get note
   * @return note
  **/
  @Schema(example = "A short note", description = "")
  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public PortingRequestAnswerSeq donornetworkoperator(String donornetworkoperator) {
    this.donornetworkoperator = donornetworkoperator;
    return this;
  }

   /**
   * Get donornetworkoperator
   * @return donornetworkoperator
  **/
  @Schema(example = "XENO", description = "")
  public String getDonornetworkoperator() {
    return donornetworkoperator;
  }

  public void setDonornetworkoperator(String donornetworkoperator) {
    this.donornetworkoperator = donornetworkoperator;
  }

  public PortingRequestAnswerSeq donorserviceprovider(String donorserviceprovider) {
    this.donorserviceprovider = donorserviceprovider;
    return this;
  }

   /**
   * Get donorserviceprovider
   * @return donorserviceprovider
  **/
  @Schema(example = "XENO", description = "")
  public String getDonorserviceprovider() {
    return donorserviceprovider;
  }

  public void setDonorserviceprovider(String donorserviceprovider) {
    this.donorserviceprovider = donorserviceprovider;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PortingRequestAnswerSeq portingRequestAnswerSeq = (PortingRequestAnswerSeq) o;
    return Objects.equals(this.numberseries, portingRequestAnswerSeq.numberseries) &&
        Objects.equals(this.blockingcode, portingRequestAnswerSeq.blockingcode) &&
        Objects.equals(this.firstpossibledate, portingRequestAnswerSeq.firstpossibledate) &&
        Objects.equals(this.note, portingRequestAnswerSeq.note) &&
        Objects.equals(this.donornetworkoperator, portingRequestAnswerSeq.donornetworkoperator) &&
        Objects.equals(this.donorserviceprovider, portingRequestAnswerSeq.donorserviceprovider);
  }

  @Override
  public int hashCode() {
    return Objects.hash(numberseries, blockingcode, firstpossibledate, note, donornetworkoperator, donorserviceprovider);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PortingRequestAnswerSeq {\n");

    sb.append("    numberseries: ").append(toIndentedString(numberseries)).append("\n");
    sb.append("    blockingcode: ").append(toIndentedString(blockingcode)).append("\n");
    sb.append("    firstpossibledate: ").append(toIndentedString(firstpossibledate)).append("\n");
    sb.append("    note: ").append(toIndentedString(note)).append("\n");
    sb.append("    donornetworkoperator: ").append(toIndentedString(donornetworkoperator)).append("\n");
    sb.append("    donorserviceprovider: ").append(toIndentedString(donorserviceprovider)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
