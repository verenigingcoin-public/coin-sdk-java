package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.IPortingSeqCreator;

public class EnumDeactivationOperatorSeqBuilder {

    private final EnumOperatorSeq enumOperatorSeq = new EnumOperatorSeq();
    private IPortingSeqCreator<EnumDeactivationOperatorBuilder, EnumOperatorSeq> finishFunction;

    public EnumDeactivationOperatorSeqBuilder setProfileid(String profileid) {
        this.enumOperatorSeq.setProfileid(profileid);
        return this;
    }

    public EnumDeactivationOperatorSeqBuilder setDefaultservice(String defaultservice) {
        this.enumOperatorSeq.setDefaultservice(defaultservice);
        return this;
    }

    EnumDeactivationOperatorSeqBuilder onFinish(IPortingSeqCreator<EnumDeactivationOperatorBuilder, EnumOperatorSeq> f) {
        finishFunction = f;
        return this;
    }

    public EnumDeactivationOperatorBuilder finish() {
        return finishFunction.setBuild(this.enumOperatorSeq);
    }
}
