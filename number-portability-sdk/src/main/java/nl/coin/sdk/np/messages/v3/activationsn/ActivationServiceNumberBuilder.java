package nl.coin.sdk.np.messages.v3.activationsn;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

public class ActivationServiceNumberBuilder extends HeaderCreator<ActivationServiceNumberBuilder> implements Builder<Message<nl.coin.sdk.np.messages.v3.activationsn.ActivationServiceNumberMessage>> {

    private nl.coin.sdk.np.messages.v3.activationsn.ActivationServiceNumber activationsn = new ActivationServiceNumber();

    @Override
    protected ActivationServiceNumberBuilder getThis() {
        return this;
    }

    public ActivationServiceNumberBuilder setDossierId(String dossierId) {
        activationsn.setDossierid(dossierId);
        return this;
    }

    public ActivationServiceNumberBuilder setPlatformProvider(String platformProvider) {
        activationsn.setPlatformprovider(platformProvider);
        return this;
    }

    public ActivationServiceNumberBuilder setPlannedDateTime(String plannedDateTime) {
        activationsn.setPlanneddatetime(plannedDateTime);
        return this;
    }

    public ActivationServiceNumberBuilder setNote(String note) {
        activationsn.setNote(note);
        return this;
    }

    public ActivationServiceNumberSeqBuilder addActivationServiceNumberSeq() {
        return new ActivationServiceNumberSeqBuilder()
                .onFinish(
                        activationsnSeq -> {
                            ActivationServiceNumberRepeats activationServiceNumberRepeats = new ActivationServiceNumberRepeats()
                                    .seq(activationsnSeq);
                            this.activationsn.addRepeatsItem(activationServiceNumberRepeats);
                            return this;
                        }
                );
    }

    public Message<nl.coin.sdk.np.messages.v3.activationsn.ActivationServiceNumberMessage> build() {
        nl.coin.sdk.np.messages.v3.activationsn.ActivationServiceNumberBody activationServiceNumberBody = new ActivationServiceNumberBody().activationsn(activationsn);
        nl.coin.sdk.np.messages.v3.activationsn.ActivationServiceNumberMessage message = new ActivationServiceNumberMessage()
                .header(header)
                .body(activationServiceNumberBody);
        return new Message<>(message, MessageType.ACTIVATION_SERVICE_NUMBER_V3);
    }
}
