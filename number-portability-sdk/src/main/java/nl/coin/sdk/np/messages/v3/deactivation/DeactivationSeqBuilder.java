package nl.coin.sdk.np.messages.v3.deactivation;

import nl.coin.sdk.np.messages.v3.IPortingSeqCreator;
import nl.coin.sdk.np.messages.v3.common.NumberSeries;

public class DeactivationSeqBuilder {

    private final DeactivationSeq deactivationSeq = new DeactivationSeq();
    private IPortingSeqCreator<DeactivationBuilder, DeactivationSeq> finishFunction;

    public DeactivationSeqBuilder setNumberSeries(String startNumber, String endNumber) {
        NumberSeries numberSeries = new NumberSeries()
                .start(startNumber)
                .end(endNumber);
        this.deactivationSeq.setNumberseries(numberSeries);
        return this;
    }

    DeactivationSeqBuilder onFinish(IPortingSeqCreator<DeactivationBuilder, DeactivationSeq> f) {
        finishFunction = f;
        return this;
    }

    public DeactivationBuilder finish() {
        return finishFunction.setBuild(this.deactivationSeq);
    }
}
