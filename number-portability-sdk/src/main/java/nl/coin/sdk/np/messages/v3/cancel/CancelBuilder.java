package nl.coin.sdk.np.messages.v3.cancel;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

public class CancelBuilder extends HeaderCreator<CancelBuilder> implements Builder<Message<CancelMessage>> {

    private Cancel cancel = new Cancel();

    @Override
    protected CancelBuilder getThis() {
        return this;
    }

    public CancelBuilder setDossierId(String dossierId) {
        cancel.setDossierid(dossierId);
        return this;
    }

    public CancelBuilder setNote(String note) {
        cancel.setNote(note);
        return this;
    }

    public Message<CancelMessage> build() {
        CancelBody cancelBody = new CancelBody().cancel(cancel);
        CancelMessage message = new CancelMessage()
                .header(header)
                .body(cancelBody);
        return new Message<>(message, MessageType.CANCEL_V3);
    }
}
