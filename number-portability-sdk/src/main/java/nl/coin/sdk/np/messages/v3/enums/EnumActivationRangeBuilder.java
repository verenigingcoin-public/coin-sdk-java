package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

import java.util.ArrayList;
import java.util.List;

public class EnumActivationRangeBuilder extends HeaderCreator<EnumActivationRangeBuilder> implements Builder<Message<EnumActivationRangeMessage>> {
    private EnumContent enumContent = new EnumContent();
    private List<EnumNumberRepeats> enumNumberRepeats = new ArrayList<>();

    @Override
    protected EnumActivationRangeBuilder getThis() {
        return this;
    }

    public EnumActivationRangeBuilder setCurrentnetworkoperator(String currentnetworkoperator) {
        enumContent.setCurrentnetworkoperator(currentnetworkoperator);
        return this;
    }

    public EnumActivationRangeBuilder setTypeofnumber(String typeofnumber) {
        enumContent.setTypeofnumber(typeofnumber);
        return this;
    }

    public EnumActivationRangeBuilder setDossierId(String dossierid) {
        enumContent.setDossierid(dossierid);
        return this;
    }

    public EnumActivationRangeSeqBuilder addEnumNumberSeq() {
        return new EnumActivationRangeSeqBuilder()
                .onFinish(
                        enumNumberSeq -> {
                            EnumNumberRepeats enumNumberRepeats = new EnumNumberRepeats()
                                    .seq(enumNumberSeq);
                            this.enumContent.addRepeatsItem(enumNumberRepeats);
                            return this;
                        }
                );
    }

    public Message<EnumActivationRangeMessage> build() {
        if (enumNumberRepeats.size() > 0) {
            enumContent.setRepeats(enumNumberRepeats);
        }
        EnumActivationRangeBody enumActivationRangeBody = new EnumActivationRangeBody().enumactivationrange(enumContent);
        EnumActivationRangeMessage message = new EnumActivationRangeMessage()
                .header(header)
                .body(enumActivationRangeBody);
        return new Message<>(message, MessageType.ENUM_ACTIVATION_RANGE_V3);
    }
}
