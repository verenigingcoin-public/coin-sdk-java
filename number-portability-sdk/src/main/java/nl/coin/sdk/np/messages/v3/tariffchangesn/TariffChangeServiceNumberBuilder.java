package nl.coin.sdk.np.messages.v3.tariffchangesn;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

public class TariffChangeServiceNumberBuilder extends HeaderCreator<TariffChangeServiceNumberBuilder> implements Builder<Message<TariffChangeServiceNumberMessage>> {

    private TariffChangeServiceNumber tariffChangeServiceNumber = new TariffChangeServiceNumber();

    @Override
    protected TariffChangeServiceNumberBuilder getThis() {
        return this;
    }

    public TariffChangeServiceNumberBuilder setDossierId(String dossierId) {
        tariffChangeServiceNumber.setDossierid(dossierId);
        return this;
    }

    public TariffChangeServiceNumberBuilder setPlatformProvider(String platformProvider) {
        tariffChangeServiceNumber.setPlatformprovider(platformProvider);
        return this;
    }

    public TariffChangeServiceNumberBuilder setPlannedDateTime(String plannedDateTime) {
        tariffChangeServiceNumber.setPlanneddatetime(plannedDateTime);
        return this;
    }

    public TariffChangeServiceNumberSeqBuilder addTariffChangeServiceNumberSeq() {
        return new TariffChangeServiceNumberSeqBuilder()
                .onFinish(
                        tariffChangeServiceNumberSeq -> {
                            TariffChangeServiceNumberRepeats tariffChangeServiceNumberRepeats = new TariffChangeServiceNumberRepeats()
                                    .seq(tariffChangeServiceNumberSeq);
                            this.tariffChangeServiceNumber.addRepeatsItem(tariffChangeServiceNumberRepeats);
                            return this;
                        }
                );
    }

    public Message<TariffChangeServiceNumberMessage> build() {
        TariffChangeServiceNumberBody tariffChangeServiceNumberBody = new TariffChangeServiceNumberBody().tariffchangesn(tariffChangeServiceNumber);
        TariffChangeServiceNumberMessage message = new TariffChangeServiceNumberMessage()
                .header(header)
                .body(tariffChangeServiceNumberBody);
        return new Message<>(message, MessageType.TARIFF_CHANGE_SERVICE_NUMBER_V3);
    }
}
