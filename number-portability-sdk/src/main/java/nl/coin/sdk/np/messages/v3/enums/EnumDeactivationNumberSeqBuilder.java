package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.IPortingSeqCreator;
import nl.coin.sdk.np.messages.v3.common.NumberSeries;

import java.util.ArrayList;

public class EnumDeactivationNumberSeqBuilder {

    private final EnumNumberSeq enumNumberSeq = new EnumNumberSeq();
    private IPortingSeqCreator<EnumDeactivationNumberBuilder, EnumNumberSeq> finishFunction;

    public EnumDeactivationNumberSeqBuilder setNumberSeries(String startNumber, String endNumber) {
        NumberSeries numberSeries = new NumberSeries()
                .start(startNumber)
                .end(endNumber);
        this.enumNumberSeq.setNumberseries(numberSeries);
        return this;
    }

    public EnumDeactivationNumberSeqBuilder setProfileids(String... profileids) {
        ArrayList<EnumRepeats> enumRepeats = new EnumRepeatsBuilder().setProfileIds(profileids).build();
        enumNumberSeq.setRepeats(enumRepeats);
        return this;
    }

    EnumDeactivationNumberSeqBuilder onFinish(IPortingSeqCreator<EnumDeactivationNumberBuilder, EnumNumberSeq> f) {
        finishFunction = f;
        return this;
    }

    public EnumDeactivationNumberBuilder finish() {
        return finishFunction.setBuild(this.enumNumberSeq);
    }
}
