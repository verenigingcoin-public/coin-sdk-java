package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.IPortingSeqCreator;
import nl.coin.sdk.np.messages.v3.common.NumberSeries;

import java.util.ArrayList;

public class EnumActivationRangeSeqBuilder {

    private final EnumNumberSeq enumNumberSeq = new EnumNumberSeq();
    private IPortingSeqCreator<EnumActivationRangeBuilder, EnumNumberSeq> finishFunction;

    public EnumActivationRangeSeqBuilder setNumberSeries(String startNumber, String endNumber) {
        NumberSeries numberSeries = new NumberSeries()
                .start(startNumber)
                .end(endNumber);
        this.enumNumberSeq.setNumberseries(numberSeries);
        return this;
    }

    public EnumActivationRangeSeqBuilder setProfileids(String... profileids) {
        ArrayList<EnumRepeats> enumRepeats = new EnumRepeatsBuilder().setProfileIds(profileids).build();
        enumNumberSeq.setRepeats(enumRepeats);
        return this;
    }

    EnumActivationRangeSeqBuilder onFinish(IPortingSeqCreator<EnumActivationRangeBuilder, EnumNumberSeq> f) {
        finishFunction = f;
        return this;
    }

    public EnumActivationRangeBuilder finish() {
        return finishFunction.setBuild(this.enumNumberSeq);
    }
}
