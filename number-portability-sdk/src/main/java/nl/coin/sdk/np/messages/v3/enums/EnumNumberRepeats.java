/*
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package nl.coin.sdk.np.messages.v3.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.oas.annotations.media.Schema;

import java.util.Objects;
/**
 * EnumNumberRepeats
 */

@jakarta.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2021-04-19T08:22:26.566049+02:00[Europe/Amsterdam]")
public class EnumNumberRepeats {
  @JsonProperty("seq")
  private EnumNumberSeq seq = null;

  public EnumNumberRepeats seq(EnumNumberSeq seq) {
    this.seq = seq;
    return this;
  }

   /**
   * Get seq
   * @return seq
  **/
  @Schema(required = true, description = "")
  public EnumNumberSeq getSeq() {
    return seq;
  }

  public void setSeq(EnumNumberSeq seq) {
    this.seq = seq;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EnumNumberRepeats enumNumberRepeats = (EnumNumberRepeats) o;
    return Objects.equals(this.seq, enumNumberRepeats.seq);
  }

  @Override
  public int hashCode() {
    return Objects.hash(seq);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EnumNumberRepeats {\n");

    sb.append("    seq: ").append(toIndentedString(seq)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
