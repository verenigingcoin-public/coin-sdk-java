package nl.coin.sdk.np.messages.v3.portingrequestanswer;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

public class PortingRequestAnswerBuilder extends HeaderCreator<PortingRequestAnswerBuilder> implements Builder<Message<PortingRequestAnswerMessage>> {
    private final PortingRequestAnswer portingRequestAnswer = new PortingRequestAnswer();

    @Override
    protected PortingRequestAnswerBuilder getThis() {
        return this;
    }

    public PortingRequestAnswerBuilder setDossierId(String dossierId) {
        portingRequestAnswer.setDossierid(dossierId);
        return this;
    }

    public PortingRequestAnswerBuilder setBlocking(String blocking) {
        portingRequestAnswer.setBlocking(blocking);
        return this;
    }

    public PortingRequestAnswerSeqBuilder addPortingRequestAnswerSeq() {
        return new PortingRequestAnswerSeqBuilder()
                .onFinish(
                    portingRequestAnswerSeq -> {
                        PortingRequestAnswerRepeats portingRequestAnswerRepeats = new PortingRequestAnswerRepeats()
                            .seq(portingRequestAnswerSeq);
                        this.portingRequestAnswer.addRepeatsItem(portingRequestAnswerRepeats);
                        return this;
                    }
        );
    }

    public Message<PortingRequestAnswerMessage> build() {
        PortingRequestAnswerBody body = new PortingRequestAnswerBody()
                .portingrequestanswer(portingRequestAnswer);

        PortingRequestAnswerMessage message = new PortingRequestAnswerMessage()
                .header(header)
                .body(body);

        return new Message<>(message, MessageType.PORTING_REQUEST_ANSWER_V3);
    }
}
