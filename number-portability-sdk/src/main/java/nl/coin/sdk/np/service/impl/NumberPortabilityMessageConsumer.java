package nl.coin.sdk.np.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.coin.sdk.common.client.IOffsetPersister;
import nl.coin.sdk.common.client.SseConsumer;
import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.NumberPortabilityMessage;
import nl.coin.sdk.np.messages.v3.activationsn.ActivationServiceNumberMessage;
import nl.coin.sdk.np.messages.v3.cancel.CancelMessage;
import nl.coin.sdk.np.messages.v3.deactivation.DeactivationMessage;
import nl.coin.sdk.np.messages.v3.deactivationsn.DeactivationServiceNumberMessage;
import nl.coin.sdk.np.messages.v3.enums.*;
import nl.coin.sdk.np.messages.v3.errorfound.ErrorFoundMessage;
import nl.coin.sdk.np.messages.v3.portingperformed.PortingPerformedMessage;
import nl.coin.sdk.np.messages.v3.portingrequest.PortingRequestMessage;
import nl.coin.sdk.np.messages.v3.portingrequestanswer.PortingRequestAnswerMessage;
import nl.coin.sdk.np.messages.v3.portingrequestanswerdelayed.PortingRequestAnswerDelayedMessage;
import nl.coin.sdk.np.messages.v3.range.RangeActivationMessage;
import nl.coin.sdk.np.messages.v3.range.RangeDeactivationMessage;
import nl.coin.sdk.np.messages.v3.tariffchangesn.TariffChangeServiceNumberMessage;
import nl.coin.sdk.np.service.INumberPortabilityMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

@Service
public class NumberPortabilityMessageConsumer {

    private final SseConsumer sseConsumer;
    private final ObjectMapper mapper;

    @Autowired
    public NumberPortabilityMessageConsumer(SseConsumer sseConsumer, ObjectMapper mapper) {
        this.sseConsumer = sseConsumer;
        this.mapper = mapper;
    }


    /**
     * Recommended method for consuming messages. On connect or reconnect
     * it will consume all unconfirmed messages.
     */
    public void startConsumingUnconfirmed(
        INumberPortabilityMessageListener listener,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmed(
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Method for consuming messages. On connect or reconnect
     * it will consume all unconfirmed messages.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmed(INumberPortabilityMessageListener, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmed(
        INumberPortabilityMessageListener listener,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmed(
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }


    /**
     * Consume all messages, both confirmed and unconfirmed.
     * Only use for special cases if {@link #startConsumingUnconfirmed(INumberPortabilityMessageListener, Runnable, MessageType...)} does not meet needs.
     */
    public void startConsumingAll(
        INumberPortabilityMessageListener listener,
        IOffsetPersister offsetPersister,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingAll(
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed.
     * Only use for special cases if {@link #startConsumingUnconfirmed(INumberPortabilityMessageListener, MessageType...)} does not meet needs.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingAll(INumberPortabilityMessageListener, IOffsetPersister, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingAll(
        INumberPortabilityMessageListener listener,
        IOffsetPersister offsetPersister,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingAll(
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed, from a certain offset.
     * Only use for special cases if {@link #startConsumingUnconfirmed(INumberPortabilityMessageListener, Runnable, MessageType...)} does not meet needs.
     */
    public void startConsumingAll(
        INumberPortabilityMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingAll(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed, from a certain offset.
     * Only use for special cases if {@link #startConsumingUnconfirmed(INumberPortabilityMessageListener, MessageType...)} does not meet needs.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingAll(INumberPortabilityMessageListener, String, IOffsetPersister, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingAll(
        INumberPortabilityMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingAll(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(INumberPortabilityMessageListener, Runnable, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     */
    public void startConsumingUnconfirmedWithOffsetPersistence(
        INumberPortabilityMessageListener listener,
        IOffsetPersister offsetPersister,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(INumberPortabilityMessageListener, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmedWithOffsetPersistence(INumberPortabilityMessageListener, IOffsetPersister, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmedWithOffsetPersistence(
        INumberPortabilityMessageListener listener,
        IOffsetPersister offsetPersister,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(INumberPortabilityMessageListener, Runnable, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     */
    public void startConsumingUnconfirmedWithOffsetPersistence(
        INumberPortabilityMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(INumberPortabilityMessageListener, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmedWithOffsetPersistence(INumberPortabilityMessageListener, String, IOffsetPersister, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmedWithOffsetPersistence(
        INumberPortabilityMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    public void stopConsuming() {
        sseConsumer.stopConsuming();
    }

    private List<String> toStrings(MessageType... messageTypes) {
        return Arrays.stream(messageTypes).map(MessageType::getType).collect(Collectors.toList());
    }

    protected String handleSSE(INumberPortabilityMessageListener listener, List<ServerSentEvent<String>> sses) {
        if (sses.size() != 1) throw new RuntimeException("Stream buffer size should be 1");
        ServerSentEvent<String> sse = sses.get(0);
        String event = sse.event();
        String messageId = sse.id();
        String data = sse.data();
        try {
            if (MessageType.PORTING_REQUEST_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<PortingRequestMessage>>() {}, listener::onPortingRequest);
            } else if (MessageType.PORTING_REQUEST_ANSWER_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<PortingRequestAnswerMessage>>() {}, listener::onPortingRequestAnswer);
            } else if (MessageType.PORTING_PERFORMED_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<PortingPerformedMessage>>() {}, listener::onPortingPerformed);
            } else if (MessageType.PORTING_REQUEST_ANSWER_DELAYED_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<PortingRequestAnswerDelayedMessage>>() {}, listener::onPortingRequestAnswerDelayed);
            } else if (MessageType.CANCEL_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<CancelMessage>>() {}, listener::onCancel);
            } else if (MessageType.DEACTIVATION_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<DeactivationMessage>>() {}, listener::onDeactivation);
            } else if (MessageType.ERROR_FOUND_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<ErrorFoundMessage>>() {}, listener::onErrorFound);
            } else if (MessageType.ACTIVATION_SERVICE_NUMBER_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<ActivationServiceNumberMessage>>() {}, listener::onActivationServiceNumber);
            } else if (MessageType.DEACTIVATION_SERVICE_NUMBER_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<DeactivationServiceNumberMessage>>() {}, listener::onDeactivationServiceNumber);
            } else if (MessageType.TARIFF_CHANGE_SERVICE_NUMBER_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<TariffChangeServiceNumberMessage>>() {}, listener::onTariffChangeServiceNumber);
            } else if (MessageType.ENUM_ACTIVATION_NUMBER_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<EnumActivationNumberMessage>>() {}, listener::onEnumActivationNumber);
            } else if (MessageType.ENUM_ACTIVATION_OPERATOR_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<EnumActivationOperatorMessage>>() {}, listener::onEnumActivationOperator);
            } else if (MessageType.ENUM_ACTIVATION_RANGE_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<EnumActivationRangeMessage>>() {}, listener::onEnumActivationRange);
            } else if (MessageType.ENUM_DEACTIVATION_NUMBER_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<EnumDeactivationNumberMessage>>() {}, listener::onEnumDeactivationNumber);
            } else if (MessageType.ENUM_DEACTIVATION_OPERATOR_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<EnumDeactivationOperatorMessage>>() {}, listener::onEnumDeactivationOperator);
            } else if (MessageType.ENUM_DEACTIVATION_RANGE_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<EnumDeactivationRangeMessage>>() {}, listener::onEnumDeactivationRange);
            } else if (MessageType.ENUM_PROFILE_ACTIVATION_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<EnumProfileActivationMessage>>() {}, listener::onEnumProfileActivation);
            } else if (MessageType.ENUM_PROFILE_DEACTIVATION_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<EnumProfileDeactivationMessage>>() {}, listener::onEnumProfileDeactivation);
            } else if (MessageType.RANGE_ACTIVATION_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<RangeActivationMessage>>() {}, listener::onRangeActivation);
            } else if (MessageType.RANGE_DEACTIVATION_V3.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<RangeDeactivationMessage>>() {}, listener::onRangeDeactivation);
            } else if (sse.event() != null) {
                Message message = mapper.readValue(data, new TypeReference<Message>(){});
                listener.onUnknownMessage(messageId, message);
                return null;
            } else {
                listener.onKeepAlive();
                return null;
            }
            return messageId;
        } catch (IOException exception) {
            listener.onException(exception);
            return null;
        }
    }

    protected <T extends NumberPortabilityMessage> void handleEvent(String messageId, String data, TypeReference<Message<T>> typeReference, BiConsumer<String, T> consumer) throws IOException {
        Message<T> message = mapper.readValue(data, typeReference);
        consumer.accept(messageId, message.getMessage());
    }
}

