package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.IPortingSeqCreator;
import nl.coin.sdk.np.messages.v3.common.NumberSeries;

import java.util.ArrayList;

public class EnumActivationNumberSeqBuilder {

    private final EnumNumberSeq enumNumberSeq = new EnumNumberSeq();
    private IPortingSeqCreator<EnumActivationNumberBuilder, EnumNumberSeq> finishFunction;

    public EnumActivationNumberSeqBuilder setNumberSeries(String startNumber, String endNumber) {
        NumberSeries numberSeries = new NumberSeries()
                .start(startNumber)
                .end(endNumber);
        this.enumNumberSeq.setNumberseries(numberSeries);
        return this;
    }

    public EnumActivationNumberSeqBuilder setProfileids(String... profileids) {
        ArrayList<EnumRepeats> enumRepeats = new EnumRepeatsBuilder().setProfileIds(profileids).build();
        enumNumberSeq.setRepeats(enumRepeats);
        return this;
    }

    EnumActivationNumberSeqBuilder onFinish(IPortingSeqCreator<EnumActivationNumberBuilder, EnumNumberSeq> f) {
        finishFunction = f;
        return this;
    }

    public EnumActivationNumberBuilder finish() {
        return finishFunction.setBuild(this.enumNumberSeq);
    }
}
