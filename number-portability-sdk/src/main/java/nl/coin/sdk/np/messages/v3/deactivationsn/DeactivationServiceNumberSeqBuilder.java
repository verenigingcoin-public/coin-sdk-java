package nl.coin.sdk.np.messages.v3.deactivationsn;

import nl.coin.sdk.np.messages.v3.IPortingSeqCreator;
import nl.coin.sdk.np.messages.v3.common.NumberSeries;

public class DeactivationServiceNumberSeqBuilder {

    private final DeactivationServiceNumberSeq deactivationServiceNumberSeq = new DeactivationServiceNumberSeq();
    private IPortingSeqCreator<DeactivationServiceNumberBuilder, DeactivationServiceNumberSeq> finishFunction;

    public DeactivationServiceNumberSeqBuilder setNumberSeries(String startNumber, String endNumber) {
        NumberSeries numberSeries = new NumberSeries()
                .start(startNumber)
                .end(endNumber);
        this.deactivationServiceNumberSeq.setNumberseries(numberSeries);
        return this;
    }

    public DeactivationServiceNumberSeqBuilder setPop(String pop) {
        this.deactivationServiceNumberSeq.setPop(pop);
        return this;
    }

    DeactivationServiceNumberSeqBuilder onFinish(IPortingSeqCreator<DeactivationServiceNumberBuilder, DeactivationServiceNumberSeq> f) {
        finishFunction = f;
        return this;
    }

    public DeactivationServiceNumberBuilder finish() {
        return finishFunction.setBuild(this.deactivationServiceNumberSeq);
    }
}
