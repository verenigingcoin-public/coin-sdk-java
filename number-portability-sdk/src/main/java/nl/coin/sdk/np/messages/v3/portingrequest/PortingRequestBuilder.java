package nl.coin.sdk.np.messages.v3.portingrequest;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.CustomerInfo;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

import java.util.ArrayList;
import java.util.List;

public class PortingRequestBuilder extends HeaderCreator<PortingRequestBuilder> implements Builder<Message<PortingRequestMessage>> {
    private PortingRequest portingRequest = new PortingRequest();
    private List<PortingRequestRepeats> portingRequestRepeats = new ArrayList<>();

    @Override
    protected PortingRequestBuilder getThis() {
        return this;
    }

    public PortingRequestBuilder setRecipientnetworkoperator(String recipientnetworkoperator) {
        portingRequest.setRecipientnetworkoperator(recipientnetworkoperator);
        return this;
    }

    public PortingRequestBuilder setRecipientserviceprovider(String recipientserviceprovider) {
        portingRequest.setRecipientserviceprovider(recipientserviceprovider);
        return this;
    }

    public PortingRequestBuilder setDonornetworkoperator(String donornetworkoperator) {
        portingRequest.setDonornetworkoperator(donornetworkoperator);
        return this;
    }

    public PortingRequestBuilder setDonorserviceprovider(String donorserviceprovider) {
        portingRequest.setDonorserviceprovider(donorserviceprovider);
        return this;
    }

    public PortingRequestBuilder setDossierId(String dossierid) {
        portingRequest.setDossierid(dossierid);
        return this;
    }

    public PortingRequestBuilder setContract(String contract) {
        portingRequest.setContract(contract);
        return this;
    }

    public PortingRequestSeqBuilder addPortingRequestSeq() {
        return new PortingRequestSeqBuilder()
                .onFinish(
                        portingRequestSeq -> {
                            PortingRequestRepeats portingRequestRepeats = new PortingRequestRepeats()
                                    .seq(portingRequestSeq);
                            this.portingRequest.addRepeatsItem(portingRequestRepeats);
                            return this;
                        }
                );
    }

    public PortingRequestBuilder setNote(String note) {
        portingRequest.setNote(note);
        return this;
    }

    public PortingRequestBuilder setCustomerInfo(String lastname, String companyname, String housenr, String housenrext, String postcode, String customerid) {
        CustomerInfo customerInfo = new CustomerInfo()
                .lastname(lastname)
                .companyname(companyname)
                .housenr(housenr)
                .housenrext(housenrext)
                .postcode(postcode)
                .customerid(customerid);
        portingRequest.setCustomerinfo(customerInfo);
        return this;
    }

    public Message<PortingRequestMessage> build() {
        if (portingRequestRepeats.size() > 0) {
            portingRequest.setRepeats(portingRequestRepeats);
        }
        PortingRequestBody portingRequestBody = new PortingRequestBody().portingrequest(portingRequest);
        PortingRequestMessage message = new PortingRequestMessage()
                .header(header)
                .body(portingRequestBody);
        return new Message<>(message, MessageType.PORTING_REQUEST_V3);
    }
}
