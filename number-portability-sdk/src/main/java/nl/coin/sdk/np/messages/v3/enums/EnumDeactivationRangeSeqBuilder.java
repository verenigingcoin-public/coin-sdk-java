package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.IPortingSeqCreator;
import nl.coin.sdk.np.messages.v3.common.NumberSeries;

import java.util.ArrayList;

public class EnumDeactivationRangeSeqBuilder {

    private final EnumNumberSeq enumNumberSeq = new EnumNumberSeq();
    private IPortingSeqCreator<EnumDeactivationRangeBuilder, EnumNumberSeq> finishFunction;

    public EnumDeactivationRangeSeqBuilder setNumberSeries(String startNumber, String endNumber) {
        NumberSeries numberSeries = new NumberSeries()
                .start(startNumber)
                .end(endNumber);
        this.enumNumberSeq.setNumberseries(numberSeries);
        return this;
    }

    public EnumDeactivationRangeSeqBuilder setProfileids(String... profileids) {
        ArrayList<EnumRepeats> enumRepeats = new EnumRepeatsBuilder().setProfileIds(profileids).build();
        enumNumberSeq.setRepeats(enumRepeats);
        return this;
    }

    EnumDeactivationRangeSeqBuilder onFinish(IPortingSeqCreator<EnumDeactivationRangeBuilder, EnumNumberSeq> f) {
        finishFunction = f;
        return this;
    }

    public EnumDeactivationRangeBuilder finish() {
        return finishFunction.setBuild(this.enumNumberSeq);
    }
}
