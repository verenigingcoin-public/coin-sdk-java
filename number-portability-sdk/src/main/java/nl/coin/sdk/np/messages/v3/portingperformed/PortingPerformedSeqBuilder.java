package nl.coin.sdk.np.messages.v3.portingperformed;

import nl.coin.sdk.np.messages.v3.IPortingSeqCreator;
import nl.coin.sdk.np.messages.v3.common.EnumRepeatsBuilder;
import nl.coin.sdk.np.messages.v3.common.NumberSeries;
import nl.coin.sdk.np.messages.v3.enums.EnumRepeats;

import java.util.ArrayList;

public class PortingPerformedSeqBuilder {

    private final PortingPerformedSeq portingPerformedSeq = new PortingPerformedSeq();
    private IPortingSeqCreator<PortingPerformedBuilder, PortingPerformedSeq> finishFunction;

    public PortingPerformedSeqBuilder setNumberSeries(String startNumber, String endNumber) {
        NumberSeries numberSeries = new NumberSeries()
                .start(startNumber)
                .end(endNumber);
        this.portingPerformedSeq.setNumberseries(numberSeries);
        return this;
    }

    public PortingPerformedSeqBuilder setProfileids(String... profileids) {
        ArrayList<EnumRepeats> enumRepeats = new EnumRepeatsBuilder().setProfileIds(profileids).build();
        portingPerformedSeq.setRepeats(enumRepeats);
        return this;
    }

    public PortingPerformedSeqBuilder setBackporting(String backporting) {
        portingPerformedSeq.setBackporting(backporting);
        return this;
    }

    public PortingPerformedSeqBuilder setPop(String pop) {
        portingPerformedSeq.setPop(pop);
        return this;
    }

    PortingPerformedSeqBuilder onFinish(IPortingSeqCreator<PortingPerformedBuilder, PortingPerformedSeq> f) {
        finishFunction = f;
        return this;
    }

    public PortingPerformedBuilder finish() {
        return finishFunction.setBuild(this.portingPerformedSeq);
    }
}
