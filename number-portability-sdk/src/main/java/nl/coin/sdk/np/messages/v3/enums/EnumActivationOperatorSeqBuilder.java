package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.IPortingSeqCreator;

public class EnumActivationOperatorSeqBuilder {

    private final EnumOperatorSeq enumOperatorSeq = new EnumOperatorSeq();
    private IPortingSeqCreator<EnumActivationOperatorBuilder, EnumOperatorSeq> finishFunction;

    public EnumActivationOperatorSeqBuilder setProfileid(String profileid) {
        this.enumOperatorSeq.setProfileid(profileid);
        return this;
    }

    public EnumActivationOperatorSeqBuilder setDefaultservice(String defaultservice) {
        this.enumOperatorSeq.setDefaultservice(defaultservice);
        return this;
    }

    EnumActivationOperatorSeqBuilder onFinish(IPortingSeqCreator<EnumActivationOperatorBuilder, EnumOperatorSeq> f) {
        finishFunction = f;
        return this;
    }

    public EnumActivationOperatorBuilder finish() {
        return finishFunction.setBuild(this.enumOperatorSeq);
    }
}
