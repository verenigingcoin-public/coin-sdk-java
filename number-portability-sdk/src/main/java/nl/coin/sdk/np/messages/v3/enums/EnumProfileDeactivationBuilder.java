package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

public class EnumProfileDeactivationBuilder extends HeaderCreator<EnumProfileDeactivationBuilder> implements Builder<Message<EnumProfileDeactivationMessage>> {
    private EnumProfileDeactivation enumProfileDeactivation = new EnumProfileDeactivation();

    @Override
    protected EnumProfileDeactivationBuilder getThis() {
        return this;
    }

    public EnumProfileDeactivationBuilder setDossierId(String dossierid) {
        enumProfileDeactivation.setDossierid(dossierid);
        return this;
    }

    public EnumProfileDeactivationBuilder setCurrentnetworkoperator(String currentnetworkoperator) {
        enumProfileDeactivation.setCurrentnetworkoperator(currentnetworkoperator);
        return this;
    }

    public EnumProfileDeactivationBuilder setTypeofnumber(String typeofnumber) {
        enumProfileDeactivation.setTypeofnumber(typeofnumber);
        return this;
    }

    public EnumProfileDeactivationBuilder setProfileid(String profileid) {
        enumProfileDeactivation.setProfileid(profileid);
        return this;
    }

    public Message<EnumProfileDeactivationMessage> build() {
        EnumProfileDeactivationBody enumProfileDeactivationBody = new EnumProfileDeactivationBody().enumprofiledeactivation(enumProfileDeactivation);
        EnumProfileDeactivationMessage message = new EnumProfileDeactivationMessage()
                .header(header)
                .body(enumProfileDeactivationBody);
        return new Message<>(message, MessageType.ENUM_PROFILE_DEACTIVATION_V3);
    }
}
