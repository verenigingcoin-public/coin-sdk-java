package nl.coin.sdk.np.messages.v3.activationsn;

import nl.coin.sdk.np.messages.v3.IPortingSeqCreator;
import nl.coin.sdk.np.messages.v3.common.NumberSeries;
import nl.coin.sdk.np.messages.v3.tariffchangesn.TariffInfo;

public class ActivationServiceNumberSeqBuilder {

    private final ActivationServiceNumberSeq activationServiceNumberSeq = new ActivationServiceNumberSeq();
    private IPortingSeqCreator<ActivationServiceNumberBuilder, ActivationServiceNumberSeq> finishFunction;

    public ActivationServiceNumberSeqBuilder setNumberSeries(String startNumber, String endNumber) {
        NumberSeries numberSeries = new NumberSeries()
                .start(startNumber)
                .end(endNumber);
        this.activationServiceNumberSeq.setNumberseries(numberSeries);
        return this;
    }

    public ActivationServiceNumberSeqBuilder setPop(String pop) {
        this.activationServiceNumberSeq.setPop(pop);
        return this;
    }

    public ActivationServiceNumberSeqBuilder setTariffInfo(String peak, String offPeak, TariffInfo.CurrencyEnum currency, String type, String vat) {
        TariffInfo tariffInfo = new TariffInfo();
        tariffInfo.setPeak(peak);
        tariffInfo.setOffpeak(offPeak);
        tariffInfo.setCurrency(currency);
        tariffInfo.setType(type);
        tariffInfo.setVat(vat);
        this.activationServiceNumberSeq.setTariffinfo(tariffInfo);
        return this;
    }

    ActivationServiceNumberSeqBuilder onFinish(IPortingSeqCreator<ActivationServiceNumberBuilder, ActivationServiceNumberSeq> f) {
        finishFunction = f;
        return this;
    }

    public ActivationServiceNumberBuilder finish() {
        return finishFunction.setBuild(this.activationServiceNumberSeq);
    }
}
