package nl.coin.sdk.np.messages.v3.enums;

import org.apache.commons.lang3.builder.Builder;

import java.util.ArrayList;

public class EnumRepeatsBuilder implements Builder<ArrayList<EnumRepeats>> {
    private String[] profileIds;

    EnumRepeatsBuilder setProfileIds(String... profileIds) {
        this.profileIds = profileIds;
        return this;
    }

    public ArrayList<EnumRepeats> build() {
        ArrayList<EnumRepeats> enumRepeats = new ArrayList<>();
        for (String profileid : profileIds) {
            EnumProfileSeq enumProfileSeq = new EnumProfileSeq().profileid(profileid);
            enumRepeats.add(new EnumRepeats().seq(enumProfileSeq));
        }
        return enumRepeats.size() == 0 ? null : enumRepeats;
    }
}
