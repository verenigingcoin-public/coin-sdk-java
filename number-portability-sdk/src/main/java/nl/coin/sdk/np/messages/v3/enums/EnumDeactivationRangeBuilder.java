package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

import java.util.ArrayList;
import java.util.List;

public class EnumDeactivationRangeBuilder extends HeaderCreator<EnumDeactivationRangeBuilder> implements Builder<Message<EnumDeactivationRangeMessage>> {
    private EnumContent enumContent = new EnumContent();
    private List<EnumNumberRepeats> enumNumberRepeats = new ArrayList<>();

    @Override
    protected EnumDeactivationRangeBuilder getThis() {
        return this;
    }

    public EnumDeactivationRangeBuilder setCurrentnetworkoperator(String currentnetworkoperator) {
        enumContent.setCurrentnetworkoperator(currentnetworkoperator);
        return this;
    }

    public EnumDeactivationRangeBuilder setTypeofnumber(String typeofnumber) {
        enumContent.setTypeofnumber(typeofnumber);
        return this;
    }

    public EnumDeactivationRangeBuilder setDossierId(String dossierid) {
        enumContent.setDossierid(dossierid);
        return this;
    }

    public EnumDeactivationRangeSeqBuilder addEnumNumberSeq() {
        return new EnumDeactivationRangeSeqBuilder()
                .onFinish(
                        enumNumberSeq -> {
                            EnumNumberRepeats enumNumberRepeats = new EnumNumberRepeats()
                                    .seq(enumNumberSeq);
                            this.enumContent.addRepeatsItem(enumNumberRepeats);
                            return this;
                        }
                );
    }

    public Message<EnumDeactivationRangeMessage> build() {
        if (enumNumberRepeats.size() > 0) {
            enumContent.setRepeats(enumNumberRepeats);
        }
        EnumDeactivationRangeBody enumDeactivationRangeBody = new EnumDeactivationRangeBody().enumdeactivationrange(enumContent);
        EnumDeactivationRangeMessage message = new EnumDeactivationRangeMessage()
                .header(header)
                .body(enumDeactivationRangeBody);
        return new Message<>(message, MessageType.ENUM_DEACTIVATION_RANGE_V3);
    }
}
