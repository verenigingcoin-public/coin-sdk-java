package nl.coin.sdk.np.messages.v3;

public enum MessageType {

    ACTIVATION_SERVICE_NUMBER_V3("activationsn"),
    CANCEL_V3("cancel"),
    DEACTIVATION_V3("deactivation"),
    DEACTIVATION_SERVICE_NUMBER_V3("deactivationsn"),
    ENUM_ACTIVATION_NUMBER_V3("enumactivationnumber"),
    ENUM_ACTIVATION_OPERATOR_V3("enumactivationoperator"),
    ENUM_ACTIVATION_RANGE_V3("enumactivationrange"),
    ENUM_DEACTIVATION_NUMBER_V3("enumdeactivationnumber"),
    ENUM_DEACTIVATION_OPERATOR_V3("enumdeactivationoperator"),
    ENUM_DEACTIVATION_RANGE_V3("enumdeactivationrange"),
    ENUM_PROFILE_ACTIVATION_V3("enumprofileactivation"),
    ENUM_PROFILE_DEACTIVATION_V3("enumprofiledeactivation"),
    ERROR_FOUND_V3 ("errorfound"),
    PORTING_REQUEST_V3("portingrequest"),
    PORTING_REQUEST_ANSWER_V3("portingrequestanswer"),
    PORTING_PERFORMED_V3("portingperformed"),
    PORTING_REQUEST_ANSWER_DELAYED_V3("pradelayed"),
    RANGE_ACTIVATION_V3("rangeactivation"),
    RANGE_DEACTIVATION_V3("rangedeactivation"),
    TARIFF_CHANGE_SERVICE_NUMBER_V3("tariffchangesn");

    final private String type;
    final private String eventName;

    private static final String VERSION_SUFFIX_V3 = "-v3";

    MessageType(String type) {
        this.type = type;
        this.eventName = type + VERSION_SUFFIX_V3;
    }

    public String getType() {
        return type;
    }

    public String getEventName() {
        return eventName;
    }

}
