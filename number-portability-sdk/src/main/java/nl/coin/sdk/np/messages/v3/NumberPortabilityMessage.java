package nl.coin.sdk.np.messages.v3;

import nl.coin.sdk.np.messages.v3.common.Header;

public interface NumberPortabilityMessage<T> {
    Header getHeader();
    void setHeader(Header header);
    String getDossierid();
    T getBody();
}
