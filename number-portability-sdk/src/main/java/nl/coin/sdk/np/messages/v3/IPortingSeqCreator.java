package nl.coin.sdk.np.messages.v3;

public interface IPortingSeqCreator<BUILDER, SEQUENCE> {
    BUILDER setBuild(SEQUENCE p);
}
