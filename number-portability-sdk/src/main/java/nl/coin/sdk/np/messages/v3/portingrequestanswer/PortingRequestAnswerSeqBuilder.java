package nl.coin.sdk.np.messages.v3.portingrequestanswer;

import nl.coin.sdk.np.messages.v3.IPortingSeqCreator;
import nl.coin.sdk.np.messages.v3.common.NumberSeries;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class PortingRequestAnswerSeqBuilder {

    private final PortingRequestAnswerSeq portingRequestAnswerSeq = new PortingRequestAnswerSeq();
    private IPortingSeqCreator<PortingRequestAnswerBuilder, PortingRequestAnswerSeq> finishFunction;

    public PortingRequestAnswerSeqBuilder setNumberSeries(String startNumber, String endNumber) {
        NumberSeries numberSeries = new NumberSeries()
                .start(startNumber)
                .end(endNumber);
        this.portingRequestAnswerSeq.setNumberseries(numberSeries);
        return this;
    }

    public PortingRequestAnswerSeqBuilder setFirstPossibleDate(LocalDateTime firstPossibleDate) {
        this.portingRequestAnswerSeq.setFirstpossibledate(firstPossibleDate.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        return this;
    }

    public PortingRequestAnswerSeqBuilder setNote(String note) {
        this.portingRequestAnswerSeq.setNote(note);
        return this;
    }

    public PortingRequestAnswerSeqBuilder setBlockingcode(String blocking) {
        this.portingRequestAnswerSeq.setBlockingcode(blocking);
        return this;
    }

    public PortingRequestAnswerSeqBuilder setDonornetworkoperator(String donorNetworkOperator) {
        this.portingRequestAnswerSeq.setDonornetworkoperator(donorNetworkOperator);
        return this;
    }

    public PortingRequestAnswerSeqBuilder setDonorserviceprovider(String donorServiceProvider) {
        this.portingRequestAnswerSeq.setDonorserviceprovider(donorServiceProvider);
        return this;
    }

    PortingRequestAnswerSeqBuilder onFinish(IPortingSeqCreator<PortingRequestAnswerBuilder, PortingRequestAnswerSeq> f) {
        finishFunction = f;
        return this;
    }

    public PortingRequestAnswerBuilder finish() {
        return finishFunction.setBuild(this.portingRequestAnswerSeq);
    }
}
