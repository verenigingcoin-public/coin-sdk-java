package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

import java.util.ArrayList;
import java.util.List;

public class EnumActivationNumberBuilder extends HeaderCreator<EnumActivationNumberBuilder> implements Builder<Message<EnumActivationNumberMessage>> {
    private EnumContent enumContent = new EnumContent();
    private List<EnumNumberRepeats> enumNumberRepeats = new ArrayList<>();

    @Override
    protected EnumActivationNumberBuilder getThis() {
        return this;
    }

    public EnumActivationNumberBuilder setCurrentnetworkoperator(String currentnetworkoperator) {
        enumContent.setCurrentnetworkoperator(currentnetworkoperator);
        return this;
    }

    public EnumActivationNumberBuilder setTypeofnumber(String typeofnumber) {
        enumContent.setTypeofnumber(typeofnumber);
        return this;
    }

    public EnumActivationNumberBuilder setDossierId(String dossierid) {
        enumContent.setDossierid(dossierid);
        return this;
    }

    public EnumActivationNumberSeqBuilder addEnumNumberSeq() {
        return new EnumActivationNumberSeqBuilder()
                .onFinish(
                        enumNumberSeq -> {
                            EnumNumberRepeats enumNumberRepeats = new EnumNumberRepeats()
                                    .seq(enumNumberSeq);
                            this.enumContent.addRepeatsItem(enumNumberRepeats);
                            return this;
                        }
                );
    }

    public Message<EnumActivationNumberMessage> build() {
        if (enumNumberRepeats.size() > 0) {
            enumContent.setRepeats(enumNumberRepeats);
        }
        EnumActivationNumberBody enumActivationNumberBody = new EnumActivationNumberBody().enumactivationnumber(enumContent);
        EnumActivationNumberMessage message = new EnumActivationNumberMessage()
                .header(header)
                .body(enumActivationNumberBody);
        return new Message<>(message, MessageType.ENUM_ACTIVATION_NUMBER_V3);
    }
}
