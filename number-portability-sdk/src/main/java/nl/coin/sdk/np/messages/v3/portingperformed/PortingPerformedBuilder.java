package nl.coin.sdk.np.messages.v3.portingperformed;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

import java.util.ArrayList;
import java.util.List;

public class PortingPerformedBuilder extends HeaderCreator<PortingPerformedBuilder> implements Builder<Message<PortingPerformedMessage>> {
    private PortingPerformed portingPerformed = new PortingPerformed();
    private List<PortingPerformedRepeats> portingPerformedRepeats = new ArrayList<>();

    @Override
    protected PortingPerformedBuilder getThis() {
        return this;
    }

    public PortingPerformedBuilder setPortingPerformed(String recipientnetworkoperator, String donornetworkoperator) {
        portingPerformed
                .recipientnetworkoperator(recipientnetworkoperator)
                .donornetworkoperator(donornetworkoperator);
        return this;
    }

    public PortingPerformedBuilder setDossierId(String dossierId) {
        portingPerformed.setDossierid(dossierId);
        return this;
    }

    public PortingPerformedBuilder setActualDatetime(String actualdatetime) {
        portingPerformed.setActualdatetime(actualdatetime);
        return this;
    }

    public PortingPerformedBuilder setBatchporting(String batchporting) {
        portingPerformed.setBatchporting(batchporting);
        return this;
    }

    public PortingPerformedSeqBuilder addPortingPerformedSeq() {
        return new PortingPerformedSeqBuilder()
                .onFinish(
                        portingPerformedSeq -> {
                            PortingPerformedRepeats portingPerformedRepeats = new PortingPerformedRepeats()
                                    .seq(portingPerformedSeq);
                            this.portingPerformed.addRepeatsItem(portingPerformedRepeats);
                            return this;
                        }
                );
    }

    public Message<PortingPerformedMessage> build() {
        if (portingPerformedRepeats.size() > 0) {
            portingPerformed.setRepeats(portingPerformedRepeats);
        }
        PortingPerformedBody portingPerformedBody = new PortingPerformedBody().portingperformed(portingPerformed);
        PortingPerformedMessage message = new PortingPerformedMessage()
                .header(header)
                .body(portingPerformedBody);
        return new Message<>(message, MessageType.PORTING_PERFORMED_V3);
    }
}
