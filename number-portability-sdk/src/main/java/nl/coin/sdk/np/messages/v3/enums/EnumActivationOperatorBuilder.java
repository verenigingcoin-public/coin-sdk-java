package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

import java.util.ArrayList;
import java.util.List;

public class EnumActivationOperatorBuilder extends HeaderCreator<EnumActivationOperatorBuilder> implements Builder<Message<EnumActivationOperatorMessage>> {
    private EnumOperatorContent enumOperatorContent = new EnumOperatorContent();
    private List<nl.coin.sdk.np.messages.v3.enums.EnumOperatorRepeats> enumOperatorRepeats = new ArrayList<>();

    @Override
    protected EnumActivationOperatorBuilder getThis() {
        return this;
    }

    public EnumActivationOperatorBuilder setCurrentnetworkoperator(String currentnetworkoperator) {
        enumOperatorContent.setCurrentnetworkoperator(currentnetworkoperator);
        return this;
    }

    public EnumActivationOperatorBuilder setTypeofnumber(String typeofnumber) {
        enumOperatorContent.setTypeofnumber(typeofnumber);
        return this;
    }

    public EnumActivationOperatorBuilder setDossierId(String dossierid) {
        enumOperatorContent.setDossierid(dossierid);
        return this;
    }

    public EnumActivationOperatorSeqBuilder addEnumOperatorSeq() {
        return new EnumActivationOperatorSeqBuilder()
                .onFinish(
                        enumOperatorSeq -> {
                            nl.coin.sdk.np.messages.v3.enums.EnumOperatorRepeats enumOperatorRepeats = new EnumOperatorRepeats()
                                    .seq(enumOperatorSeq);
                            this.enumOperatorContent.addRepeatsItem(enumOperatorRepeats);
                            return this;
                        }
                );
    }

    public Message<EnumActivationOperatorMessage> build() {
        if (enumOperatorRepeats.size() > 0) {
            enumOperatorContent.setRepeats(enumOperatorRepeats);
        }
        EnumActivationOperatorBody enumActivationOperatorBody = new EnumActivationOperatorBody().enumactivationoperator(enumOperatorContent);
        EnumActivationOperatorMessage message = new EnumActivationOperatorMessage()
                .header(header)
                .body(enumActivationOperatorBody);
        return new Message<>(message, MessageType.ENUM_ACTIVATION_OPERATOR_V3);
    }
}
