package nl.coin.sdk.np.messages.v3.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public abstract class HeaderCreator<T> {
    protected final Header header = new Header();

    protected abstract T getThis();

    public T setHeader(String sender, String receiver) {
        return setHeader(sender, null, receiver, null);
    }

    public T setHeader(String senderNetworkoperator, String senderServiceprovider, String receiverNetworkoperator, String receiverServiceprovider) {
        Sender sender = new Sender()
                .networkoperator(senderNetworkoperator)
                .serviceprovider(senderServiceprovider);
        Receiver receiver = new Receiver()
                .networkoperator(receiverNetworkoperator)
                .serviceprovider(receiverServiceprovider);
        header
                .timestamp(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")))
                .sender(sender)
                .receiver(receiver);
        return getThis();

    }
}
