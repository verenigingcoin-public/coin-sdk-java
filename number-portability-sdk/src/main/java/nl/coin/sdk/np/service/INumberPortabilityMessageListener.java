package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.activationsn.ActivationServiceNumberMessage;
import nl.coin.sdk.np.messages.v3.cancel.CancelMessage;
import nl.coin.sdk.np.messages.v3.deactivation.DeactivationMessage;
import nl.coin.sdk.np.messages.v3.deactivationsn.DeactivationServiceNumberMessage;
import nl.coin.sdk.np.messages.v3.enums.*;
import nl.coin.sdk.np.messages.v3.errorfound.ErrorFoundMessage;
import nl.coin.sdk.np.messages.v3.portingperformed.PortingPerformedMessage;
import nl.coin.sdk.np.messages.v3.portingrequest.PortingRequestMessage;
import nl.coin.sdk.np.messages.v3.portingrequestanswer.PortingRequestAnswerMessage;
import nl.coin.sdk.np.messages.v3.portingrequestanswerdelayed.PortingRequestAnswerDelayedMessage;
import nl.coin.sdk.np.messages.v3.range.RangeActivationMessage;
import nl.coin.sdk.np.messages.v3.range.RangeDeactivationMessage;
import nl.coin.sdk.np.messages.v3.tariffchangesn.TariffChangeServiceNumberMessage;

public interface INumberPortabilityMessageListener {
    void onKeepAlive();

    void onUnknownMessage(String messageId, Message message);

    void onException(Exception exception);

    void onPortingRequest(String messageId, PortingRequestMessage message);

    void onPortingRequestAnswer(String messageId, PortingRequestAnswerMessage message);

    void onPortingRequestAnswerDelayed(String messageId, PortingRequestAnswerDelayedMessage message);

    void onPortingPerformed(String messageId, PortingPerformedMessage message);

    void onDeactivation(String messageId, DeactivationMessage message);

    void onCancel(String messageId, CancelMessage message);

    void onErrorFound(String messageId, ErrorFoundMessage message);

    void onActivationServiceNumber(String messageId, ActivationServiceNumberMessage message);

    void onDeactivationServiceNumber(String messageId, DeactivationServiceNumberMessage message);

    void onTariffChangeServiceNumber(String messageId, TariffChangeServiceNumberMessage message);

    void onRangeActivation(String messageId, RangeActivationMessage message);

    void onRangeDeactivation(String messageId, RangeDeactivationMessage message);

    void onEnumActivationNumber(String messageId, EnumActivationNumberMessage message);

    void onEnumActivationOperator(String messageId, EnumActivationOperatorMessage message);

    void onEnumActivationRange(String messageId, EnumActivationRangeMessage message);

    void onEnumDeactivationNumber(String messageId, EnumDeactivationNumberMessage message);

    void onEnumDeactivationOperator(String messageId, EnumDeactivationOperatorMessage message);

    void onEnumDeactivationRange(String messageId, EnumDeactivationRangeMessage message);

    void onEnumProfileActivation(String messageId, EnumProfileActivationMessage message);

    void onEnumProfileDeactivation(String messageId, EnumProfileDeactivationMessage message);
}
