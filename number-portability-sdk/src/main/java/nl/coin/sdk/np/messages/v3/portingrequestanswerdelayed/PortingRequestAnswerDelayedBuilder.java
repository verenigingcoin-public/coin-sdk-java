package nl.coin.sdk.np.messages.v3.portingrequestanswerdelayed;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

public class PortingRequestAnswerDelayedBuilder extends HeaderCreator<PortingRequestAnswerDelayedBuilder> implements Builder<Message<PortingRequestAnswerDelayedMessage>> {
    private final PortingRequestAnswerDelayed portingRequestAnswerDelayed = new PortingRequestAnswerDelayed();

    @Override
    protected PortingRequestAnswerDelayedBuilder getThis() {
        return this;
    }

    public PortingRequestAnswerDelayedBuilder setDossierId(String dossierId) {
        portingRequestAnswerDelayed.setDossierid(dossierId);
        return this;
    }

    public PortingRequestAnswerDelayedBuilder setDonornetworkoperator(String donornetworkoperator) {
        portingRequestAnswerDelayed.setDonornetworkoperator(donornetworkoperator);
        return this;
    }

    public PortingRequestAnswerDelayedBuilder setDonorserviceprovider(String setDonorserviceprovider) {
        portingRequestAnswerDelayed.setDonorserviceprovider(setDonorserviceprovider);
        return this;
    }

    public PortingRequestAnswerDelayedBuilder setAnswerduedatetime(String answerduedatetime) {
        portingRequestAnswerDelayed.answerduedatetime(answerduedatetime);
        return this;
    }

    public PortingRequestAnswerDelayedBuilder setReasoncode(String reasoncode) {
        portingRequestAnswerDelayed.setReasoncode(reasoncode);
        return this;
    }

    public Message<PortingRequestAnswerDelayedMessage> build() {
        PortingRequestAnswerDelayedBody body = new PortingRequestAnswerDelayedBody()
                .pradelayed(portingRequestAnswerDelayed);

        PortingRequestAnswerDelayedMessage message = new PortingRequestAnswerDelayedMessage()
                .header(header)
                .body(body);

        return new Message<>(message, MessageType.PORTING_REQUEST_ANSWER_DELAYED_V3);
    }
}

