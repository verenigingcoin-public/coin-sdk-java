package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

public class EnumProfileActivationBuilder extends HeaderCreator<EnumProfileActivationBuilder> implements Builder<Message<EnumProfileActivationMessage>> {
    private EnumProfileActivation enumProfileActivation = new EnumProfileActivation();

    @Override
    protected EnumProfileActivationBuilder getThis() {
        return this;
    }

    public EnumProfileActivationBuilder setDossierId(String dossierid) {
        enumProfileActivation.setDossierid(dossierid);
        return this;
    }

    public EnumProfileActivationBuilder setCurrentnetworkoperator(String currentnetworkoperator) {
        enumProfileActivation.setCurrentnetworkoperator(currentnetworkoperator);
        return this;
    }

    public EnumProfileActivationBuilder setTypeofnumber(String typeofnumber) {
        enumProfileActivation.setTypeofnumber(typeofnumber);
        return this;
    }

    public EnumProfileActivationBuilder setScope(String scope) {
        enumProfileActivation.setScope(scope);
        return this;
    }

    public EnumProfileActivationBuilder setProfileid(String profileid) {
        enumProfileActivation.setProfileid(profileid);
        return this;
    }

    public EnumProfileActivationBuilder setTtl(String ttl) {
        enumProfileActivation.setTtl(ttl);
        return this;
    }

    public EnumProfileActivationBuilder setDnsclass(String dnsclass) {
        enumProfileActivation.setDnsclass(dnsclass);
        return this;
    }

    public EnumProfileActivationBuilder setRectype(String rectype) {
        enumProfileActivation.setRectype(rectype);
        return this;
    }

    public EnumProfileActivationBuilder setOrder(String order) {
        enumProfileActivation.setOrder(order);
        return this;
    }

    public EnumProfileActivationBuilder setPreference(String preference) {
        enumProfileActivation.setPreference(preference);
        return this;
    }

    public EnumProfileActivationBuilder setFlags(String flags) {
        enumProfileActivation.setFlags(flags);
        return this;
    }

    public EnumProfileActivationBuilder setEnumservice(String enumservice) {
        enumProfileActivation.setEnumservice(enumservice);
        return this;
    }

    public EnumProfileActivationBuilder setRegexp(String regexp) {
        enumProfileActivation.setRegexp(regexp);
        return this;
    }

    public EnumProfileActivationBuilder setUsertag(String usertag) {
        enumProfileActivation.setUsertag(usertag);
        return this;
    }

    public EnumProfileActivationBuilder setDomain(String domain) {
        enumProfileActivation.setDomain(domain);
        return this;
    }

    public EnumProfileActivationBuilder setSpcode(String spcode) {
        enumProfileActivation.setSpcode(spcode);
        return this;
    }

    public EnumProfileActivationBuilder setProcesstype(String processtype) {
        enumProfileActivation.setProcesstype(processtype);
        return this;
    }

    public EnumProfileActivationBuilder setGateway(String gateway) {
        enumProfileActivation.setGateway(gateway);
        return this;
    }

    public EnumProfileActivationBuilder setService(String service) {
        enumProfileActivation.setService(service);
        return this;
    }

    public EnumProfileActivationBuilder setDomaintag(String domaintag) {
        enumProfileActivation.setDomaintag(domaintag);
        return this;
    }

    public EnumProfileActivationBuilder setReplacement(String replacement) {
        enumProfileActivation.setReplacement(replacement);
        return this;
    }

    public Message<EnumProfileActivationMessage> build() {
        EnumProfileActivationBody enumProfileActivationBody = new EnumProfileActivationBody().enumprofileactivation(enumProfileActivation);
        EnumProfileActivationMessage message = new EnumProfileActivationMessage()
                .header(header)
                .body(enumProfileActivationBody);
        return new Message<>(message, MessageType.ENUM_PROFILE_ACTIVATION_V3);
    }
}
