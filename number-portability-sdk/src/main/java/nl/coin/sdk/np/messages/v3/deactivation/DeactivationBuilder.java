package nl.coin.sdk.np.messages.v3.deactivation;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

public class DeactivationBuilder extends HeaderCreator<DeactivationBuilder> implements Builder<Message<DeactivationMessage>> {
    private Deactivation deactivation = new Deactivation();

    @Override
    protected DeactivationBuilder getThis() {
        return this;
    }

    public DeactivationBuilder setCurrentNetworkOperator(String currentNetworkOperator) {
        deactivation.setCurrentnetworkoperator(currentNetworkOperator);
        return this;
    }

    public DeactivationBuilder setDossierId(String cdossierid) {
        deactivation.setDossierid(cdossierid);
        return this;
    }

    public DeactivationBuilder setOriginalNetworkOperator(String originalnetworkoperator) {
        deactivation.setOriginalnetworkoperator(originalnetworkoperator);
        return this;
    }

    public DeactivationSeqBuilder addDeactivationSeq() {
        return new DeactivationSeqBuilder()
                .onFinish(
                        deactivationSeq -> {
                            DeactivationRepeats deactivationRepeats = new DeactivationRepeats()
                                    .seq(deactivationSeq);
                            this.deactivation.addRepeatsItem(deactivationRepeats);
                            return this;
                        }
                );
    }

    @Override
    public Message<DeactivationMessage> build() {
        DeactivationBody deactivationBody = new DeactivationBody()
                .deactivation(deactivation);
        DeactivationMessage message = new DeactivationMessage()
                .header(header)
                .body(deactivationBody);
        return new Message<>(message, MessageType.DEACTIVATION_V3);
    }
}
