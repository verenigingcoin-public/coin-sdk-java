package nl.coin.sdk.np.service.impl;

import nl.coin.sdk.common.client.CtpApiRestTemplateSupport;
import nl.coin.sdk.common.crypto.HmacSha256Signer;
import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.ConfirmationMessage;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.service.INumberPortabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.PrivateKey;

@Service
public class NumberPortabilityService extends CtpApiRestTemplateSupport implements INumberPortabilityService {

    private final String apiUrl;

    @Autowired
    public NumberPortabilityService(
            @Value("${api.url}") String apiUrl,
            @Value("${consumer.name}") String consumerName,
            HmacSha256Signer signer,
            PrivateKey privateKey,
            NumberPortabilityClientErrorHandler errorHandler) {
        super(consumerName, signer, privateKey);
        this.apiUrl = apiUrl;
        restTemplate.setErrorHandler(errorHandler);
    }

    @Override
    public MessageResponse sendMessage(Message message) {
        return postMessage(message, message.getMessageType());
    }

    @Override
    public void sendConfirmation(String id) {
        ConfirmationMessage confirmationMessage = new ConfirmationMessage().transactionId(id);
        String url = apiUrl + "/dossiers/confirmations/" + id;
        sendWithToken(String.class, HttpMethod.PUT, url, confirmationMessage);
    }

    private MessageResponse postMessage(Message message, MessageType messageType) {
        String url = apiUrl + "/dossiers/" + messageType.getType();
        String responseBody = sendWithToken(String.class, HttpMethod.POST, url, message);
        try {
            return mapper.readValue(responseBody, MessageResponse.class);
        } catch (IOException e) {
            return new MessageResponse();
        }
    }
}
