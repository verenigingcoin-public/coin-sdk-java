package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;

public interface INumberPortabilityService {

    void sendConfirmation(String id);

    MessageResponse sendMessage(Message message);
}
