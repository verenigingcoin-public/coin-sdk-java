package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

import java.util.ArrayList;
import java.util.List;

public class EnumDeactivationOperatorBuilder extends HeaderCreator<EnumDeactivationOperatorBuilder> implements Builder<Message<EnumDeactivationOperatorMessage>> {
    private EnumOperatorContent enumOperatorContent = new EnumOperatorContent();
    private List<nl.coin.sdk.np.messages.v3.enums.EnumOperatorRepeats> enumOperatorRepeats = new ArrayList<>();

    @Override
    protected EnumDeactivationOperatorBuilder getThis() {
        return this;
    }

    public EnumDeactivationOperatorBuilder setCurrentnetworkoperator(String currentnetworkoperator) {
        enumOperatorContent.setCurrentnetworkoperator(currentnetworkoperator);
        return this;
    }

    public EnumDeactivationOperatorBuilder setTypeofnumber(String typeofnumber) {
        enumOperatorContent.setTypeofnumber(typeofnumber);
        return this;
    }

    public EnumDeactivationOperatorBuilder setDossierId(String dossierid) {
        enumOperatorContent.setDossierid(dossierid);
        return this;
    }

    public EnumDeactivationOperatorSeqBuilder addEnumOperatorSeq() {
        return new EnumDeactivationOperatorSeqBuilder()
                .onFinish(
                        enumOperatorSeq -> {
                            nl.coin.sdk.np.messages.v3.enums.EnumOperatorRepeats enumOperatorRepeats = new EnumOperatorRepeats()
                                    .seq(enumOperatorSeq);
                            this.enumOperatorContent.addRepeatsItem(enumOperatorRepeats);
                            return this;
                        }
                );
    }

    public Message<EnumDeactivationOperatorMessage> build() {
        if (enumOperatorRepeats.size() > 0) {
            enumOperatorContent.setRepeats(enumOperatorRepeats);
        }
        EnumDeactivationOperatorBody enumDeactivationOperatorBody = new EnumDeactivationOperatorBody().enumdeactivationoperator(enumOperatorContent);
        EnumDeactivationOperatorMessage message = new EnumDeactivationOperatorMessage()
                .header(header)
                .body(enumDeactivationOperatorBody);
        return new Message<>(message, MessageType.ENUM_DEACTIVATION_OPERATOR_V3);
    }
}
