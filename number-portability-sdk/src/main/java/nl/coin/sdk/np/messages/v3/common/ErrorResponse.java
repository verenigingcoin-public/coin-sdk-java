package nl.coin.sdk.np.messages.v3.common;

import java.util.List;

public class ErrorResponse extends MessageResponse {
    private List<ErrorContent> errors = null;

    public List<ErrorContent> getErrors() {
        return errors;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("(Transaction: ").append(getTransactionId()).append(")");
        sb.append("\n");
        for (ErrorContent errorContent : getErrors()) {
            sb.append(errorContent);
        }
        return sb.toString();
    }
}
