/*
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package nl.coin.sdk.np.messages.v3.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.oas.annotations.media.Schema;

import java.util.Objects;
/**
 * Sender
 */

@jakarta.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2021-04-19T08:22:26.566049+02:00[Europe/Amsterdam]")
public class Sender {
  @JsonProperty("networkoperator")
  private String networkoperator = null;

  @JsonProperty("serviceprovider")
  private String serviceprovider = null;

  public Sender networkoperator(String networkoperator) {
    this.networkoperator = networkoperator;
    return this;
  }

   /**
   * Get networkoperator
   * @return networkoperator
  **/
  @Schema(example = "XENO", required = true, description = "")
  public String getNetworkoperator() {
    return networkoperator;
  }

  public void setNetworkoperator(String networkoperator) {
    this.networkoperator = networkoperator;
  }

  public Sender serviceprovider(String serviceprovider) {
    this.serviceprovider = serviceprovider;
    return this;
  }

   /**
   * Get serviceprovider
   * @return serviceprovider
  **/
  @Schema(example = "XENO", description = "")
  public String getServiceprovider() {
    return serviceprovider;
  }

  public void setServiceprovider(String serviceprovider) {
    this.serviceprovider = serviceprovider;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Sender sender = (Sender) o;
    return Objects.equals(this.networkoperator, sender.networkoperator) &&
        Objects.equals(this.serviceprovider, sender.serviceprovider);
  }

  @Override
  public int hashCode() {
    return Objects.hash(networkoperator, serviceprovider);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Sender {\n");

    sb.append("    networkoperator: ").append(toIndentedString(networkoperator)).append("\n");
    sb.append("    serviceprovider: ").append(toIndentedString(serviceprovider)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
