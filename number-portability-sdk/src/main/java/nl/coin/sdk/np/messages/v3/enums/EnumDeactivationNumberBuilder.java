package nl.coin.sdk.np.messages.v3.enums;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

import java.util.ArrayList;
import java.util.List;

public class EnumDeactivationNumberBuilder extends HeaderCreator<EnumDeactivationNumberBuilder> implements Builder<Message<EnumDeactivationNumberMessage>> {
    private EnumContent enumContent = new EnumContent();
    private List<EnumNumberRepeats> enumNumberRepeats = new ArrayList<>();

    @Override
    protected EnumDeactivationNumberBuilder getThis() {
        return this;
    }

    public EnumDeactivationNumberBuilder setCurrentnetworkoperator(String currentnetworkoperator) {
        enumContent.setCurrentnetworkoperator(currentnetworkoperator);
        return this;
    }

    public EnumDeactivationNumberBuilder setTypeofnumber(String typeofnumber) {
        enumContent.setTypeofnumber(typeofnumber);
        return this;
    }

    public EnumDeactivationNumberBuilder setDossierId(String dossierid) {
        enumContent.setDossierid(dossierid);
        return this;
    }

    public EnumDeactivationNumberSeqBuilder addEnumNumberSeq() {
        return new EnumDeactivationNumberSeqBuilder()
                .onFinish(
                        enumNumberSeq -> {
                            EnumNumberRepeats enumNumberRepeats = new EnumNumberRepeats()
                                    .seq(enumNumberSeq);
                            this.enumContent.addRepeatsItem(enumNumberRepeats);
                            return this;
                        }
                );
    }

    public Message<EnumDeactivationNumberMessage> build() {
        if (enumNumberRepeats.size() > 0) {
            enumContent.setRepeats(enumNumberRepeats);
        }
        EnumDeactivationNumberBody enumDeactivationNumberBody = new EnumDeactivationNumberBody().enumdeactivationnumber(enumContent);
        EnumDeactivationNumberMessage message = new EnumDeactivationNumberMessage()
                .header(header)
                .body(enumDeactivationNumberBody);
        return new Message<>(message, MessageType.ENUM_DEACTIVATION_NUMBER_V3);
    }
}
