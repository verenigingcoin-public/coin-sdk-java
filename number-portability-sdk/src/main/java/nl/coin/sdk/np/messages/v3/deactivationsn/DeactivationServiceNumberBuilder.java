package nl.coin.sdk.np.messages.v3.deactivationsn;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

public class DeactivationServiceNumberBuilder extends HeaderCreator<DeactivationServiceNumberBuilder> implements Builder<Message<DeactivationServiceNumberMessage>> {

    private DeactivationServiceNumber deactivationsn = new DeactivationServiceNumber();

    @Override
    protected DeactivationServiceNumberBuilder getThis() {
        return this;
    }

    public DeactivationServiceNumberBuilder setDossierId(String dossierId) {
        deactivationsn.setDossierid(dossierId);
        return this;
    }

    public DeactivationServiceNumberBuilder setPlatformProvider(String platformProvider) {
        deactivationsn.setPlatformprovider(platformProvider);
        return this;
    }

    public DeactivationServiceNumberBuilder setPlannedDateTime(String plannedDateTime) {
        deactivationsn.setPlanneddatetime(plannedDateTime);
        return this;
    }

    public DeactivationServiceNumberSeqBuilder addDeactivationServiceNumberSeq() {
        return new DeactivationServiceNumberSeqBuilder()
                .onFinish(
                        deactivationsnSeq -> {
                            DeactivationServiceNumberRepeats deactivationServiceNumberRepeats = new DeactivationServiceNumberRepeats()
                                    .seq(deactivationsnSeq);
                            this.deactivationsn.addRepeatsItem(deactivationServiceNumberRepeats);
                            return this;
                        }
                );
    }

    public Message<DeactivationServiceNumberMessage> build() {
        DeactivationServiceNumberBody deactivationServiceNumberBody = new DeactivationServiceNumberBody().deactivationsn(deactivationsn);
        DeactivationServiceNumberMessage message = new DeactivationServiceNumberMessage()
                .header(header)
                .body(deactivationServiceNumberBody);
        return new Message<>(message, MessageType.DEACTIVATION_SERVICE_NUMBER_V3);
    }
}
