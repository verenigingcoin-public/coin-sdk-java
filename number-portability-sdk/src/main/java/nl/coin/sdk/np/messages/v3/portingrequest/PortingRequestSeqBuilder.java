package nl.coin.sdk.np.messages.v3.portingrequest;

import nl.coin.sdk.np.messages.v3.IPortingSeqCreator;
import nl.coin.sdk.np.messages.v3.common.EnumRepeatsBuilder;
import nl.coin.sdk.np.messages.v3.common.NumberSeries;
import nl.coin.sdk.np.messages.v3.enums.EnumRepeats;

import java.util.ArrayList;

public class PortingRequestSeqBuilder {

    private final PortingRequestSeq portingRequestSeq = new PortingRequestSeq();
    private IPortingSeqCreator<PortingRequestBuilder, PortingRequestSeq> finishFunction;

    public PortingRequestSeqBuilder setNumberSeries(String startNumber, String endNumber) {
        NumberSeries numberSeries = new NumberSeries()
                .start(startNumber)
                .end(endNumber);
        this.portingRequestSeq.setNumberseries(numberSeries);
        return this;
    }

    public PortingRequestSeqBuilder setProfileids(String... profileids) {
        ArrayList<EnumRepeats> enumRepeats = new EnumRepeatsBuilder().setProfileIds(profileids).build();
        portingRequestSeq.setRepeats(enumRepeats);
        return this;
    }

    PortingRequestSeqBuilder onFinish(IPortingSeqCreator<PortingRequestBuilder, PortingRequestSeq> f) {
        finishFunction = f;
        return this;
    }

    public PortingRequestBuilder finish() {
        return finishFunction.setBuild(this.portingRequestSeq);
    }
}
