package nl.coin.sdk.np.service.util;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.NumberPortabilityMessage;
import nl.coin.sdk.np.messages.v3.activationsn.ActivationServiceNumberMessage;
import nl.coin.sdk.np.messages.v3.cancel.CancelMessage;
import nl.coin.sdk.np.messages.v3.deactivation.DeactivationMessage;
import nl.coin.sdk.np.messages.v3.deactivationsn.DeactivationServiceNumberMessage;
import nl.coin.sdk.np.messages.v3.enums.*;
import nl.coin.sdk.np.messages.v3.errorfound.ErrorFoundMessage;
import nl.coin.sdk.np.messages.v3.portingperformed.PortingPerformedMessage;
import nl.coin.sdk.np.messages.v3.portingrequest.PortingRequestMessage;
import nl.coin.sdk.np.messages.v3.portingrequestanswer.PortingRequestAnswerMessage;
import nl.coin.sdk.np.messages.v3.portingrequestanswerdelayed.PortingRequestAnswerDelayedMessage;
import nl.coin.sdk.np.messages.v3.range.RangeActivationMessage;
import nl.coin.sdk.np.messages.v3.range.RangeDeactivationMessage;
import nl.coin.sdk.np.messages.v3.tariffchangesn.TariffChangeServiceNumberMessage;
import nl.coin.sdk.np.service.INumberPortabilityMessageListener;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class ExampleLoggingNumberPortabilityMessageListener implements INumberPortabilityMessageListener {
    private final NumberPortabilityService numberPortabilityService;
    private StreamTestResultTracker resultTracker;

    @Autowired
    public ExampleLoggingNumberPortabilityMessageListener(
            NumberPortabilityService numberPortabilityService) {
        this.numberPortabilityService = numberPortabilityService;
    }

    private CountDownLatch latch;
    private SideEffect sideEffect = messagesLeft -> {};

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }

    public void setSideEffect(SideEffect sideEffect) {
        this.sideEffect = sideEffect;
    }

    public void setResultTracker(StreamTestResultTracker resultTracker) {
        this.resultTracker = resultTracker;
    }

    @Override
    public void onKeepAlive() {
    }

    @Override
    public void onUnknownMessage(String messageId, Message message) {

    }

    @Override
    public void onException(Exception exception) {

    }

    @Override
    public void onPortingRequest(String messageId, PortingRequestMessage message) {
        setMessageAsReceivedInResult(MessageType.PORTING_REQUEST_V3);
        processMessage(messageId, message);
    }

    void setMessageAsReceivedInResult(MessageType portingRequestV3) {
        if (this.resultTracker != null) {
            this.resultTracker.setResult(portingRequestV3.getType(), Boolean.TRUE);
        }
    }

    @Override
    public void onPortingRequestAnswer(String messageId, PortingRequestAnswerMessage message) {
        setMessageAsReceivedInResult(MessageType.PORTING_REQUEST_ANSWER_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onPortingRequestAnswerDelayed(String messageId, PortingRequestAnswerDelayedMessage message) {
        setMessageAsReceivedInResult(MessageType.PORTING_REQUEST_ANSWER_DELAYED_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onPortingPerformed(String messageId, PortingPerformedMessage message) {
        setMessageAsReceivedInResult(MessageType.PORTING_PERFORMED_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onCancel(String messageId, CancelMessage message) {
        setMessageAsReceivedInResult(MessageType.CANCEL_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onDeactivation(String messageId, DeactivationMessage message) {
        setMessageAsReceivedInResult(MessageType.DEACTIVATION_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onErrorFound(String messageId, ErrorFoundMessage message) {
        setMessageAsReceivedInResult(MessageType.ERROR_FOUND_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onActivationServiceNumber(String messageId, ActivationServiceNumberMessage message) {
        setMessageAsReceivedInResult(MessageType.ACTIVATION_SERVICE_NUMBER_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onDeactivationServiceNumber(String messageId, DeactivationServiceNumberMessage message) {
        setMessageAsReceivedInResult(MessageType.DEACTIVATION_SERVICE_NUMBER_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onTariffChangeServiceNumber(String messageId, TariffChangeServiceNumberMessage message) {
        setMessageAsReceivedInResult(MessageType.TARIFF_CHANGE_SERVICE_NUMBER_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onRangeActivation(String messageId, RangeActivationMessage message) {
        setMessageAsReceivedInResult(MessageType.RANGE_ACTIVATION_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onRangeDeactivation(String messageId, RangeDeactivationMessage message) {
        setMessageAsReceivedInResult(MessageType.RANGE_DEACTIVATION_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onEnumActivationNumber(String messageId, EnumActivationNumberMessage message) {
        setMessageAsReceivedInResult(MessageType.ENUM_ACTIVATION_NUMBER_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onEnumActivationOperator(String messageId, EnumActivationOperatorMessage message) {
        setMessageAsReceivedInResult(MessageType.ENUM_ACTIVATION_OPERATOR_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onEnumActivationRange(String messageId, EnumActivationRangeMessage message) {
        setMessageAsReceivedInResult(MessageType.ENUM_ACTIVATION_RANGE_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onEnumDeactivationNumber(String messageId, EnumDeactivationNumberMessage message) {
        setMessageAsReceivedInResult(MessageType.ENUM_DEACTIVATION_NUMBER_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onEnumDeactivationOperator(String messageId, EnumDeactivationOperatorMessage message) {
        setMessageAsReceivedInResult(MessageType.ENUM_DEACTIVATION_OPERATOR_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onEnumDeactivationRange(String messageId, EnumDeactivationRangeMessage message) {
        setMessageAsReceivedInResult(MessageType.ENUM_DEACTIVATION_RANGE_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onEnumProfileActivation(String messageId, EnumProfileActivationMessage message) {
        setMessageAsReceivedInResult(MessageType.ENUM_PROFILE_ACTIVATION_V3);
        processMessage(messageId, message);
    }

    @Override
    public void onEnumProfileDeactivation(String messageId, EnumProfileDeactivationMessage message) {
        setMessageAsReceivedInResult(MessageType.ENUM_PROFILE_DEACTIVATION_V3);
        processMessage(messageId, message);
    }

    private void processMessage(String messageId, NumberPortabilityMessage message) {
        numberPortabilityService.sendConfirmation(messageId);
        if (latch != null) {
            latch.countDown();
            sideEffect.execute(latch.getCount());
        }
    }
}
