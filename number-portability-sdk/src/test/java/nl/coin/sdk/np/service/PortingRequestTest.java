package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.portingrequest.PortingRequestBuilder;
import nl.coin.sdk.np.messages.v3.portingrequest.PortingRequestMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class PortingRequestTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendPortingRequestMinimal() {
        sendMessage(createPortingRequestMinimal());
    }

    @Test
    public void sendPortingRequestFull() {
        sendMessage(createPortingRequestFull());
    }

    private Message<PortingRequestMessage> createPortingRequestMinimal() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new PortingRequestBuilder()
                .setHeader("LOADA", "LOADB")
                .setRecipientnetworkoperator("LOADB")
                .setDossierId(dossierId)
                .addPortingRequestSeq()
                    .setNumberSeries("0612345678", "0612345678").finish()
                .build();
    }

    private Message<PortingRequestMessage> createPortingRequestFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        PortingRequestBuilder portingRequestBuilder = new PortingRequestBuilder()
                .setHeader("LOADA", "LOADB")
                .setRecipientserviceprovider("LOADB")
                .setRecipientnetworkoperator("LOADB")
                .setDonornetworkoperator("LOADA")
                .setDonorserviceprovider("LOADA")
                .setDossierId(dossierId)
                .setContract("EARLY_TERMINATION")
                .setCustomerInfo("test", "test bv", "1", "a", "1234AB", "1")
                .setNote("This is a note");
        for (int i = 0; i < 250; i++) {
            String lastDigits = String.format("%03d", i);
            String phoneNumber = "0612345" + lastDigits;
            portingRequestBuilder.addPortingRequestSeq()
                    .setNumberSeries(phoneNumber, phoneNumber)
                    .setProfileids("PROF1", "PROF2", "PROF3", "PROF4", "PROF5").finish();
        }
        return portingRequestBuilder.build();
    }

    private void sendMessage(Message<PortingRequestMessage> message) {
        Assert.assertEquals(MessageType.PORTING_REQUEST_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
