package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.portingperformed.PortingPerformedBuilder;
import nl.coin.sdk.np.messages.v3.portingperformed.PortingPerformedMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class PortingPerformedTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendPortingPerformedMinimal() {
        sendAndAssertMessage(createPortingPerformedMinimal());
    }

    @Test
    public void sendPortingPerformedFull() {
        sendAndAssertMessage(createPortingPerformedFull());

    }

    private Message<PortingPerformedMessage> createPortingPerformedMinimal() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new PortingPerformedBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setPortingPerformed("LOADA", "LOADB")
                .addPortingPerformedSeq()
                    .setNumberSeries("0612345678", "0612345678")
                    .setProfileids("PROF1", "PROF2").finish()
                .build();
    }

    private Message<PortingPerformedMessage> createPortingPerformedFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        PortingPerformedBuilder portingPerformedBuilder = new PortingPerformedBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setPortingPerformed("LOADA", "LOADB")
                .setActualDatetime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")))
                .setBatchporting("Y");
        for (int i = 0; i < 250; i++) {
            String lastDigits = String.format("%03d", i);
            String phoneNumber = "0612345" + lastDigits;
            portingPerformedBuilder.addPortingPerformedSeq()
                    .setNumberSeries(phoneNumber, phoneNumber).finish();
        }
        return portingPerformedBuilder.build();
    }

    private void sendAndAssertMessage(Message<PortingPerformedMessage> message) {
        Assert.assertEquals(MessageType.PORTING_PERFORMED_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
