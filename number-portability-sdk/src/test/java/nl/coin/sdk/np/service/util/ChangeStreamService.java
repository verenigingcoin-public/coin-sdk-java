package nl.coin.sdk.np.service.util;

import nl.coin.sdk.common.client.CtpApiRestTemplateSupport;
import nl.coin.sdk.common.crypto.HmacSha256Signer;
import nl.coin.sdk.np.service.impl.NumberPortabilityClientErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.security.PrivateKey;

@Service
public class ChangeStreamService extends CtpApiRestTemplateSupport {

    private final String apiUrl;

    @Autowired
    public ChangeStreamService(
            @Value("${api.url}") String apiUrl,
            @Value("${consumer.name}") String consumerName,
            HmacSha256Signer signer,
            PrivateKey privateKey,
            NumberPortabilityClientErrorHandler errorHandler) {
        super(consumerName, signer, privateKey);
        this.apiUrl = apiUrl;
        restTemplate.setErrorHandler(errorHandler);
    }

    public void stopStream() {
        String url = apiUrl + "/dossiers/stopstream";
        sendWithToken(String.class, HttpMethod.GET, url, "");
    }

    public void changeStreamSpeed(int speed) {
        String url = apiUrl + "/dossiers/changestreamspeed/" + speed;
        sendWithToken(String.class, HttpMethod.PUT, url, "");
    }
}
