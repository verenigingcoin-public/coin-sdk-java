package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.enums.EnumActivationOperatorBuilder;
import nl.coin.sdk.np.messages.v3.enums.EnumActivationOperatorMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class EnumActivationOperatorTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendEnumActivationOperatorMinimal() {
        sendAndAssertMessage(createEnumActivationOperatorMinimal());
    }

    @Test
    public void sendEnumActivationOperatorFull() {
        sendAndAssertMessage(createEnumActivationOperatorFull());

    }

    private Message<EnumActivationOperatorMessage> createEnumActivationOperatorMinimal() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new EnumActivationOperatorBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setCurrentnetworkoperator("LOADB")
                .setTypeofnumber("3")
                .addEnumOperatorSeq()
                .setProfileid("PROF-12")
                .setDefaultservice("Y").finish()
                .build();
    }

    private Message<EnumActivationOperatorMessage> createEnumActivationOperatorFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        EnumActivationOperatorBuilder enumActivationOperatorBuilder = new EnumActivationOperatorBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setCurrentnetworkoperator("LOADB")
                .setTypeofnumber("3");
        for (int i = 0; i < 250; i++) {
            String lastDigits = String.format("%03d", i);
            String profileid = "PROF-" + lastDigits;
            enumActivationOperatorBuilder.addEnumOperatorSeq()
                    .setProfileid(profileid)
                    .setDefaultservice("Y").finish();
        }
        return enumActivationOperatorBuilder.build();
    }

    private void sendAndAssertMessage(Message<EnumActivationOperatorMessage> message) {
        Assert.assertEquals(MessageType.ENUM_ACTIVATION_OPERATOR_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
