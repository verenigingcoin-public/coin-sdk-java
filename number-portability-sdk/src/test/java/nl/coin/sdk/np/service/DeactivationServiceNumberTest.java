package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.deactivationsn.DeactivationServiceNumberBuilder;
import nl.coin.sdk.np.messages.v3.deactivationsn.DeactivationServiceNumberMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class DeactivationServiceNumberTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendMinimalActivationServiceNumber() {
        sendMessage(createMinimalDeactivationServiceNumber());
    }

    @Test
    public void sendFullActivationServiceNumber() {
        sendMessage(createLargeDeactivationServiceNumber());
    }

    private void sendMessage(Message<DeactivationServiceNumberMessage> message) {
        Assert.assertEquals(MessageType.DEACTIVATION_SERVICE_NUMBER_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }

    public Message<DeactivationServiceNumberMessage> createLargeDeactivationServiceNumber() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new DeactivationServiceNumberBuilder()
                .setHeader(
                        "LOADA",
                        "LOADA",
                        "LOADB",
                        "LOADB")
                .setDossierId(dossierId)
                .setPlannedDateTime("20190101111000")
                .setPlatformProvider("PLAT0")
                .addDeactivationServiceNumberSeq()
                .setNumberSeries("0800111111", "0800222222")
                .setPop("pop")
                .finish()
                .addDeactivationServiceNumberSeq()
                .setNumberSeries("0800333333", "0800444444")
                .setPop("pop")
                .finish()
                .build();
    }

    public Message<DeactivationServiceNumberMessage> createMinimalDeactivationServiceNumber() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new DeactivationServiceNumberBuilder()
                .setHeader(
                        "LOADA",
                        "LOADA",
                        "LOADB",
                        "LOADB")
                .setDossierId(dossierId)
                .setPlannedDateTime("20190101111000")
                .setPlatformProvider("PLAT0")
                .addDeactivationServiceNumberSeq()
                .setNumberSeries("0800111111", "0800222222")
                .setPop("pop")
                .finish()
                .build();
    }
}
