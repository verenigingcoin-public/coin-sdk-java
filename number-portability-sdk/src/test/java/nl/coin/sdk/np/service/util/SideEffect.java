package nl.coin.sdk.np.service.util;

public interface SideEffect {
    public void execute(long messagesLeft);
}