package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.enums.EnumDeactivationOperatorBuilder;
import nl.coin.sdk.np.messages.v3.enums.EnumDeactivationOperatorMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class EnumDeactivationOperatorTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendEnumDeactivationOperatorMinimal() {
        sendAndAssertMessage(createEnumDeactivationOperatorMinimal());
    }

    @Test
    public void sendEnumDeactivationOperatorFull() {
        sendAndAssertMessage(createEnumDeactivationOperatorFull());

    }

    private Message<EnumDeactivationOperatorMessage> createEnumDeactivationOperatorMinimal() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new EnumDeactivationOperatorBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setCurrentnetworkoperator("LOADB")
                .setTypeofnumber("3")
                .addEnumOperatorSeq()
                .setProfileid("PROF-12")
                .setDefaultservice("Y").finish()
                .build();
    }

    private Message<EnumDeactivationOperatorMessage> createEnumDeactivationOperatorFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        EnumDeactivationOperatorBuilder enumDeactivationOperatorBuilder = new EnumDeactivationOperatorBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setCurrentnetworkoperator("LOADB")
                .setTypeofnumber("3");
        for (int i = 0; i < 250; i++) {
            String lastDigits = String.format("%03d", i);
            String profileid = "PROF-" + lastDigits;
            enumDeactivationOperatorBuilder.addEnumOperatorSeq()
                    .setProfileid(profileid)
                    .setDefaultservice("Y").finish();
        }
        return enumDeactivationOperatorBuilder.build();
    }

    private void sendAndAssertMessage(Message<EnumDeactivationOperatorMessage> message) {
        Assert.assertEquals(MessageType.ENUM_DEACTIVATION_OPERATOR_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
