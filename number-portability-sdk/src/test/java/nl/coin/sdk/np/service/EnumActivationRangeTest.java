package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.enums.EnumActivationRangeBuilder;
import nl.coin.sdk.np.messages.v3.enums.EnumActivationRangeMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class EnumActivationRangeTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendEnumActivationRangeMinimal() {
        sendAndAssertMessage(createEnumActivationRangeMinimal());
    }

    @Test
    public void sendEnumActivationRangeFull() {
        sendAndAssertMessage(createEnumActivationRangeFull());

    }

    private Message<EnumActivationRangeMessage> createEnumActivationRangeMinimal() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new EnumActivationRangeBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setCurrentnetworkoperator("LOADB")
                .setTypeofnumber("3")
                .addEnumNumberSeq()
                .setNumberSeries("0612345678", "0612345678")
                .setProfileids("PROF1", "PROF2").finish()
                .build();
    }

    private Message<EnumActivationRangeMessage> createEnumActivationRangeFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        EnumActivationRangeBuilder enumActivationRangeBuilder = new EnumActivationRangeBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setCurrentnetworkoperator("LOADB")
                .setTypeofnumber("3");
        for (int i = 0; i < 250; i++) {
            String lastDigits = String.format("%03d", i);
            String phoneNumber = "0612345" + lastDigits;
            enumActivationRangeBuilder.addEnumNumberSeq()
                    .setNumberSeries(phoneNumber, phoneNumber)
                    .setProfileids("PROF1", "PROF2").finish();
        }
        return enumActivationRangeBuilder.build();
    }

    private void sendAndAssertMessage(Message<EnumActivationRangeMessage> message) {
        Assert.assertEquals(MessageType.ENUM_ACTIVATION_RANGE_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
