package nl.coin.sdk.np.service.util;

import nl.coin.sdk.np.messages.v3.MessageType;

import java.util.HashMap;
import java.util.Map;

public class StreamTestResultTracker {
    private Map<String, Boolean> results = new HashMap<>();

    public StreamTestResultTracker() {
        for (MessageType value : MessageType.values()) {
            results.put(value.getType(), Boolean.FALSE);
        }
    }

    public Map<String, Boolean> getResults() {
        return results;
    }

    public void setResult(String key, Boolean value) {
        this.results.put(key, value);
    }
}
