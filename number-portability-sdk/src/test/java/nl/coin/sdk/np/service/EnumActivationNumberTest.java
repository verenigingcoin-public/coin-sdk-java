package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.enums.EnumActivationNumberBuilder;
import nl.coin.sdk.np.messages.v3.enums.EnumActivationNumberMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class EnumActivationNumberTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendEnumActivationNumberMinimal() {
        sendAndAssertMessage(createEnumActivationNumberMinimal());
    }

    @Test
    public void sendEnumActivationNumberFull() {
        sendAndAssertMessage(createEnumActivationNumberFull());

    }

    private Message<EnumActivationNumberMessage> createEnumActivationNumberMinimal() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new EnumActivationNumberBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setCurrentnetworkoperator("LOADB")
                .setTypeofnumber("3")
                .addEnumNumberSeq()
                .setNumberSeries("0612345678", "0612345678")
                .setProfileids("PROF1", "PROF2").finish()
                .build();
    }

    private Message<EnumActivationNumberMessage> createEnumActivationNumberFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        EnumActivationNumberBuilder enumActivationNumberBuilder = new EnumActivationNumberBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setCurrentnetworkoperator("LOADB")
                .setTypeofnumber("3");
        for (int i = 0; i < 250; i++) {
            String lastDigits = String.format("%03d", i);
            String phoneNumber = "0612345" + lastDigits;
            enumActivationNumberBuilder.addEnumNumberSeq()
                    .setNumberSeries(phoneNumber, phoneNumber)
                    .setProfileids("PROF1", "PROF2").finish();
        }
        return enumActivationNumberBuilder.build();
    }

    private void sendAndAssertMessage(Message<EnumActivationNumberMessage> message) {
        Assert.assertEquals(MessageType.ENUM_ACTIVATION_NUMBER_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
