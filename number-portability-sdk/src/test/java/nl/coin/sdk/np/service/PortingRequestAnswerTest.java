package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.portingrequestanswer.PortingRequestAnswerBuilder;
import nl.coin.sdk.np.messages.v3.portingrequestanswer.PortingRequestAnswerMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class PortingRequestAnswerTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendPortingRequestAnswerMinimal() {
        sendMessage(createPortingRequestAnswerMinimal());
    }

    @Test
    public void sendPortingRequestAnswerFull() {
        sendMessage(createPortingRequestAnswerFull());
    }

    private Message<PortingRequestAnswerMessage> createPortingRequestAnswerMinimal() {
        String dossierId = TestUtil.generateRandomDossierId();

        return new PortingRequestAnswerBuilder()
                .setHeader(
                        "LOADA",
                        "LOADA",
                        "LOADB",
                        "LOADB")
                .setDossierId(dossierId)
                .setBlocking("N")
                .addPortingRequestAnswerSeq()
                    .setNumberSeries("0612345678", "0612345678").finish()
                .build();
    }

    private Message<PortingRequestAnswerMessage> createPortingRequestAnswerFull() {
        String dossierId = TestUtil.generateRandomDossierId();

        return new PortingRequestAnswerBuilder()
                .setHeader(
                        "LOADA",
                        "LOADA",
                        "LOADB",
                        "LOADB")
                .setDossierId(dossierId)
                .setBlocking("N")
                .addPortingRequestAnswerSeq()
                    .setDonornetworkoperator("LOADA")
                    .setDonorserviceprovider("LOADA")
                    .setFirstPossibleDate(LocalDateTime.now())
                    .setNumberSeries("0612345678", "0612345678")
                    .setNote("This is a note")
                    .setBlockingcode("99").finish()
                .addPortingRequestAnswerSeq()
                    .setDonornetworkoperator("LOADA")
                    .setDonorserviceprovider("LOADA")
                    .setFirstPossibleDate(LocalDateTime.now())
                    .setNumberSeries("0687654321", "0687654321")
                    .setNote("This is a note")
                    .setBlockingcode("99").finish()
                .build();
    }

    private void sendMessage(Message<PortingRequestAnswerMessage> message) {
        Assert.assertEquals(MessageType.PORTING_REQUEST_ANSWER_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
