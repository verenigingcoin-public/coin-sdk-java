package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.enums.EnumProfileActivationBuilder;
import nl.coin.sdk.np.messages.v3.enums.EnumProfileActivationMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class EnumProfileActivationTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendEnumProfileActivationFull() {
        sendAndAssertMessage(createEnumProfileActivationFull());

    }

    private Message<EnumProfileActivationMessage> createEnumProfileActivationFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new EnumProfileActivationBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setCurrentnetworkoperator("LOADB")
                .setTypeofnumber("3")
                .setScope("SCOPE-123")
                .setProfileid("5879AF-GNU9480N")
                .setTtl("7")
                .setDnsclass("abc def")
                .setRectype("uvw xyz")
                .setOrder("13")
                .setPreference("13")
                .setFlags("abc, def")
                .setEnumservice("Enum Service 123")
                .setRegexp("[A-Z][a-z]+\\\\d")
                .setUsertag("tag123123kjkjk")
                .setDomain("domain123")
                .setSpcode("23kjkj324")
                .setProcesstype("processTypeX")
                .setGateway("6i3k")
                .setService("sdf sdfdsf")
                .setDomaintag("fdksalfds2132")
                .setReplacement("sdf dsjhsfd sdfsfd")
                .build();
    }

    private void sendAndAssertMessage(Message<EnumProfileActivationMessage> message) {
        Assert.assertEquals(MessageType.ENUM_PROFILE_ACTIVATION_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
