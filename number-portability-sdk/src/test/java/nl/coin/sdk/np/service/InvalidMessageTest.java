package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.portingrequest.PortingRequestBuilder;
import nl.coin.sdk.np.messages.v3.portingrequest.PortingRequestMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.impl.RestClientErrorException;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class InvalidMessageTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendInvalidMessage() {
        Message<PortingRequestMessage> message = new PortingRequestBuilder()
                .addPortingRequestSeq()
                .setNumberSeries("0612345678", "0612345678")
                .setProfileids("PROF1", "PROF2").finish()
                .addPortingRequestSeq()
                .setNumberSeries("0612345678", "0612345678").finish()
                .setRecipientnetworkoperator("LOADB")
                .setCustomerInfo("test", "test bv", "1", "a", "1234AB", "1")
                .build();
        try {
            numberPortabilityService.sendMessage(message);
        } catch (HttpClientErrorException e) {
            Assert.assertFalse(((RestClientErrorException) e).getTransactionId().isEmpty());
            Assert.assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
            Assert.assertThat(
                    e.getMessage(),
                    CoreMatchers.containsString("1000: [Path '/message/body/portingrequest'] Object has missing required properties ([\"dossierid\"])")
            );
            Assert.assertThat(
                    e.getMessage(),
                    CoreMatchers.containsString("1000: [Path '/message/header'] Object has missing required properties ([\"receiver\",\"sender\",\"timestamp\"])")
            );
        }
    }
}
