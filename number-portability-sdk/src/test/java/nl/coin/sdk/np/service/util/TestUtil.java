package nl.coin.sdk.np.service.util;

import java.util.concurrent.CountDownLatch;

public class TestUtil {
    public static String generateRandomDossierId() {
        return generateRandomDossierId("");
    }

    public static String generateRandomDossierId(String operatorCode) {
        return operatorCode + "-" + (Math.round(Math.random() * 10000) + 9999);
    }

    public static void awaitCount(CountDownLatch latch, int count) {
        try {
            while (!Thread.interrupted() && latch.getCount() > count) {
                Thread.sleep(500);
                Thread.yield();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
