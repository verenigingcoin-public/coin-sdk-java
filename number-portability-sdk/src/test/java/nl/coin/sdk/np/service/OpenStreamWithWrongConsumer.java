package nl.coin.sdk.np.service;

import nl.coin.sdk.common.client.SseConsumer;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.service.impl.NumberPortabilityMessageConsumer;
import nl.coin.sdk.np.service.util.ExampleLoggingNumberPortabilityMessageListener;
import nl.coin.sdk.np.service.util.InMemoryOffsetPersister;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.concurrent.CountDownLatch;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class OpenStreamWithWrongConsumer {

    @Autowired
    private SseConsumer sseConsumer;

    @Autowired
    private NumberPortabilityMessageConsumer numberPortabilityMessageConsumer;

    @Autowired
    private ExampleLoggingNumberPortabilityMessageListener listener;

    @Test(timeout = 20000)
    public void stopListeningWhenConnectFails() throws InterruptedException {
        Object originalValue = ReflectionTestUtils.getField(sseConsumer, "consumerName");
        CountDownLatch latch = new CountDownLatch(1);
        ReflectionTestUtils.setField(sseConsumer, "consumerName", "ne-consumer");
        try {
            numberPortabilityMessageConsumer.startConsumingAll(listener, new InMemoryOffsetPersister(), latch::countDown,
                MessageType.PORTING_REQUEST_V3,
                MessageType.CANCEL_V3,
                MessageType.DEACTIVATION_V3,
                MessageType.PORTING_PERFORMED_V3,
                MessageType.PORTING_REQUEST_ANSWER_DELAYED_V3,
                MessageType.PORTING_REQUEST_ANSWER_V3);
            latch.await();
        } finally {
            ReflectionTestUtils.setField(sseConsumer, "consumerName", originalValue);
        }
    }
}
