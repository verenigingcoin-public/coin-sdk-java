package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.enums.EnumDeactivationRangeBuilder;
import nl.coin.sdk.np.messages.v3.enums.EnumDeactivationRangeMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class EnumDeactivationRangeTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendEnumDeactivationRangeMinimal() {
        sendAndAssertMessage(createEnumDeactivationRangeMinimal());
    }

    @Test
    public void sendEnumDeactivationRangeFull() {
        sendAndAssertMessage(createEnumDeactivationRangeFull());

    }

    private Message<EnumDeactivationRangeMessage> createEnumDeactivationRangeMinimal() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new EnumDeactivationRangeBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setCurrentnetworkoperator("LOADB")
                .setTypeofnumber("3")
                .addEnumNumberSeq()
                .setNumberSeries("0612345678", "0612345678")
                .setProfileids("PROF1", "PROF2").finish()
                .build();
    }

    private Message<EnumDeactivationRangeMessage> createEnumDeactivationRangeFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        EnumDeactivationRangeBuilder enumDeactivationRangeBuilder = new EnumDeactivationRangeBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setCurrentnetworkoperator("LOADB")
                .setTypeofnumber("3");
        for (int i = 0; i < 250; i++) {
            String lastDigits = String.format("%03d", i);
            String phoneNumber = "0612345" + lastDigits;
            enumDeactivationRangeBuilder.addEnumNumberSeq()
                    .setNumberSeries(phoneNumber, phoneNumber)
                    .setProfileids("PROF1", "PROF2").finish();
        }
        return enumDeactivationRangeBuilder.build();
    }

    private void sendAndAssertMessage(Message<EnumDeactivationRangeMessage> message) {
        Assert.assertEquals(MessageType.ENUM_DEACTIVATION_RANGE_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
