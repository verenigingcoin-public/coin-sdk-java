package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.service.impl.NumberPortabilityMessageConsumer;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.ChangeStreamService;
import nl.coin.sdk.np.service.util.ExampleLoggingNumberPortabilityMessageListener;
import nl.coin.sdk.np.service.util.InMemoryOffsetPersister;
import nl.coin.sdk.np.service.util.StreamTestResultTracker;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

import static nl.coin.sdk.np.service.util.TestUtil.awaitCount;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class OpenStreamTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Autowired
    private NumberPortabilityMessageConsumer numberPortabilityMessageConsumer;

    @Autowired
    private ExampleLoggingNumberPortabilityMessageListener listener;

    @Autowired
    private ChangeStreamService changeStreamService;

    @AfterEach
    public void stopStream() {
        numberPortabilityMessageConsumer.stopConsuming();
        listener.setLatch(null);
        listener.setSideEffect(messagesLeft -> {});
    }

    @Test(timeout = 20000)
    public void openStream() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(5);
        listener.setLatch(latch);
        numberPortabilityMessageConsumer.startConsumingUnconfirmed(listener, Assertions::fail);
        latch.await();
    }

    @Test(timeout = 20000)
    public void openStreamWithInterrupt() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(5);
        listener.setLatch(latch);
        listener.setSideEffect(messagesLeft -> {
            if (messagesLeft == 3)
                changeStreamService.stopStream();
        });
        numberPortabilityMessageConsumer.startConsumingUnconfirmed(listener, Assertions::fail);
        latch.await();
    }

    @Test(timeout = 30000)
    public void openStreamAndHandleAllMessageTypes() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(30);
        listener.setLatch(latch);

        StreamTestResultTracker tracker = new StreamTestResultTracker();
        listener.setResultTracker(tracker);
        numberPortabilityMessageConsumer.startConsumingUnconfirmedWithOffsetPersistence(listener, new InMemoryOffsetPersister(), Assertions::fail);
        latch.await();

        Assert.assertFalse("For at least one message the handler isn't called: " + tracker.getResults(), tracker.getResults().containsValue(Boolean.FALSE));
    }

    @Test(timeout = 20000)
    public void openSpecificStreamForAllMessages() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(5);
        listener.setLatch(latch);
        numberPortabilityMessageConsumer.startConsumingAll(listener, new InMemoryOffsetPersister(), Assertions::fail,
            MessageType.PORTING_REQUEST_V3,
            MessageType.CANCEL_V3,
            MessageType.DEACTIVATION_V3,
            MessageType.PORTING_PERFORMED_V3,
            MessageType.PORTING_REQUEST_ANSWER_DELAYED_V3,
            MessageType.PORTING_REQUEST_ANSWER_V3);
        latch.await();
    }

    @Test(timeout = 30000)
    public void stopAndResumeConsuming() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(30);
        listener.setLatch(latch);

        StreamTestResultTracker tracker = new StreamTestResultTracker();
        listener.setResultTracker(tracker);
        numberPortabilityMessageConsumer.startConsumingUnconfirmed(listener, Assertions::fail);
        awaitCount(latch, 25);
        numberPortabilityMessageConsumer.stopConsuming();
        Thread.sleep(1000);
        Assert.assertEquals(25, latch.getCount());
        numberPortabilityMessageConsumer.startConsumingUnconfirmed(listener, Assertions::fail);
        latch.await();

        Assert.assertFalse("For at least one message the handler isn't called: " + tracker.getResults(), tracker.getResults().containsValue(Boolean.FALSE));
    }

    @Test(timeout = 30000)
    public void stopConsumingUnderHeavyLoad() throws InterruptedException {
        final AtomicBoolean isProcessing = new AtomicBoolean(false);
        listener.setSideEffect(messagesLeft -> {
            Assert.assertFalse(isProcessing.get());
            isProcessing.set(true);
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            isProcessing.set(false);
        });

        changeStreamService.changeStreamSpeed(1_000_000);
        try {
            for(int i = 0; i < 3; i++) {
                CountDownLatch latch = new CountDownLatch(100);
                listener.setLatch(latch);
                numberPortabilityMessageConsumer.startConsumingUnconfirmed(listener, Assertions::fail);
                latch.await();
                numberPortabilityMessageConsumer.stopConsuming();
                Assert.assertFalse(isProcessing.get());
            }
        } finally {
            changeStreamService.changeStreamSpeed(2);
        }
    }

    @Test(timeout = 30000)
    public void doesNotConsumeTwice() throws InterruptedException {
        ExampleLoggingNumberPortabilityMessageListener firstListener = new ExampleLoggingNumberPortabilityMessageListener(numberPortabilityService);
        CountDownLatch latch = new CountDownLatch(30);
        StreamTestResultTracker tracker = new StreamTestResultTracker();
        listener.setResultTracker(tracker);
        listener.setLatch(latch);
        firstListener.setLatch(latch);
        numberPortabilityMessageConsumer.startConsumingUnconfirmed(firstListener, Assertions::fail);
        numberPortabilityMessageConsumer.startConsumingUnconfirmed(listener, Assertions::fail);
        firstListener.setSideEffect(l -> Assert.fail("First event stream has not been closed"));
        latch.await();
        Assert.assertFalse("For at least one message the handler isn't called: " + tracker.getResults(), tracker.getResults().containsValue(Boolean.FALSE));
    }
}
