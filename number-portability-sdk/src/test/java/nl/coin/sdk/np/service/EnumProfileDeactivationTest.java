package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.enums.EnumProfileDeactivationBuilder;
import nl.coin.sdk.np.messages.v3.enums.EnumProfileDeactivationMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class EnumProfileDeactivationTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendEnumProfileDeactivationFull() {
        sendAndAssertMessage(createEnumProfileDeactivationFull());

    }

    private Message<EnumProfileDeactivationMessage> createEnumProfileDeactivationFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new EnumProfileDeactivationBuilder()
                .setHeader("LOADA", "LOADB")
                .setDossierId(dossierId)
                .setCurrentnetworkoperator("LOADB")
                .setTypeofnumber("3")
                .setProfileid("PROF-123")
                .build();
    }

    private void sendAndAssertMessage(Message<EnumProfileDeactivationMessage> message) {
        Assert.assertEquals(MessageType.ENUM_PROFILE_DEACTIVATION_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
