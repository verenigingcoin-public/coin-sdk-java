package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.deactivation.DeactivationBuilder;
import nl.coin.sdk.np.messages.v3.deactivation.DeactivationMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class DeactivationTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendMinimalDeactivation() {
        sendMessage(createMinimalMessage());
    }

    @Test
    public void sendExtendedDeactivation() {
        sendMessage(createExtendedMessage());
    }

    private Message<DeactivationMessage> createMinimalMessage() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new DeactivationBuilder()
                .setHeader(
                        "LOADA",
                        "LOADA",
                        "LOADB",
                        "LOADB")
                .setDossierId(dossierId)
                .setOriginalNetworkOperator("LOADA")
                .setCurrentNetworkOperator("LOADB")
                .addDeactivationSeq()
                .setNumberSeries("0612345600", "0612345700").finish()
                .build();
    }

    private Message<DeactivationMessage> createExtendedMessage() {
        String dossierId = TestUtil.generateRandomDossierId();
        DeactivationBuilder deactivationBuilder =  new DeactivationBuilder()
                .setHeader(
                        "LOADA",
                        "LOADA",
                        "LOADB",
                        "LOADB")
                .setDossierId(dossierId)
                .setOriginalNetworkOperator("LOADA")
                .setCurrentNetworkOperator("LOADB");

        for (int i = 0; i < 250; i++) {
            String lastDigits = String.format("%03d", i);
            String phoneNumber = "0612345" + lastDigits;
            deactivationBuilder.addDeactivationSeq()
                    .setNumberSeries(phoneNumber, phoneNumber).finish();
        }
        return deactivationBuilder.build();
    }

    private void sendMessage(Message<DeactivationMessage> message) {
        Assert.assertEquals(MessageType.DEACTIVATION_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
