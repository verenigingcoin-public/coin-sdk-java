package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.cancel.CancelBuilder;
import nl.coin.sdk.np.messages.v3.cancel.CancelMessage;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class CancelTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendMinimalCancel() {
        sendMessage(createMinimalCancel());
    }

    @Test
    public void sendFullCancel() {
        sendMessage(createFullCancel());
    }

    private Message<CancelMessage> createMinimalCancel() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new CancelBuilder()
                .setHeader(
                        "LOADA",
                        "LOADA",
                        "LOADB",
                        "LOADB")
                .setDossierId(dossierId)
                .build();
    }

    private Message<CancelMessage> createFullCancel() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new CancelBuilder()
                .setHeader(
                        "LOADA",
                        "LOADA",
                        "LOADB",
                        "LOADB")
                .setDossierId(dossierId)
                .setNote("Just a note")
                .build();
    }

    private void sendMessage(Message<CancelMessage> message) {
        Assert.assertEquals(MessageType.CANCEL_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
