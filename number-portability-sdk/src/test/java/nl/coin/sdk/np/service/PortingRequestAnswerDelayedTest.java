package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.portingrequestanswerdelayed.PortingRequestAnswerDelayedBuilder;
import nl.coin.sdk.np.messages.v3.portingrequestanswerdelayed.PortingRequestAnswerDelayedMessage;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class PortingRequestAnswerDelayedTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendPortingRequestAnswerDelayedMinimal() {
        sendMessage(createPortingRequestAnswerDelayedMessageMinimal());
    }

    @Test
    public void sendPortingRequestAnswerDelayedFull() {
        sendMessage(createPortingRequestAnswerDelayedMessageFull());
    }

    private Message<PortingRequestAnswerDelayedMessage> createPortingRequestAnswerDelayedMessageMinimal() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new PortingRequestAnswerDelayedBuilder()
                .setHeader(
                        "LOADA",
                        "LOADA",
                        "LOADB",
                        "LOADB")
                .setDossierId(dossierId)
                .setDonornetworkoperator("LOADA")
                .build();
    }

    private Message<PortingRequestAnswerDelayedMessage> createPortingRequestAnswerDelayedMessageFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new PortingRequestAnswerDelayedBuilder()
                .setHeader(
                        "LOADA",
                        "LOADA",
                        "LOADB",
                        "LOADB")
                .setDossierId(dossierId)
                .setDonornetworkoperator("LOADA")
                .setDonorserviceprovider("LOADB")
                .setAnswerduedatetime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")))
                .setReasoncode("3")
                .build();
    }

    private void sendMessage(Message<PortingRequestAnswerDelayedMessage> message) {
        Assert.assertEquals(MessageType.PORTING_REQUEST_ANSWER_DELAYED_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
