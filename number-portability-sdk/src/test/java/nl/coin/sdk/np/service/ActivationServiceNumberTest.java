package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.MessageType;
import nl.coin.sdk.np.messages.v3.activationsn.ActivationServiceNumberBuilder;
import nl.coin.sdk.np.messages.v3.activationsn.ActivationServiceNumberMessage;
import nl.coin.sdk.np.messages.v3.common.MessageResponse;
import nl.coin.sdk.np.messages.v3.tariffchangesn.TariffInfo;
import nl.coin.sdk.np.service.impl.NumberPortabilityService;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class ActivationServiceNumberTest {

    @Autowired
    private NumberPortabilityService numberPortabilityService;

    @Test
    public void sendMinimalActivationServiceNumber() {
        sendMessage(createMinimalActivationServiceNumber());
    }

    @Test
    public void sendFullActivationServiceNumber() {
        sendMessage(createLargeActivationServiceNumber());
    }

    private void sendMessage(Message<ActivationServiceNumberMessage> message) {
        Assert.assertEquals(MessageType.ACTIVATION_SERVICE_NUMBER_V3, message.getMessageType());
        MessageResponse messageResponse = numberPortabilityService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }

    public Message<ActivationServiceNumberMessage> createLargeActivationServiceNumber() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new ActivationServiceNumberBuilder()
                .setHeader(
                        "LOADA",
                        "LOADA",
                        "LOADB",
                        "LOADB")
                .setDossierId(dossierId)
                .setPlannedDateTime("20190101111000")
                .setPlatformProvider("PLAT0")
                .setNote("Test note")
                .addActivationServiceNumberSeq()
                    .setNumberSeries("0800111111", "0800222222")
                    .setPop("pop")
                    .setTariffInfo("0,02000", "0,02000", TariffInfo.CurrencyEnum._0, "3", "3")
                    .finish()
                .addActivationServiceNumberSeq()
                    .setNumberSeries("0800333333", "0800444444")
                    .setPop("pop")
                    .setTariffInfo("0,02000", "0,02000", TariffInfo.CurrencyEnum._0, "3", "3")
                    .finish()
                .build();
    }

    public Message<ActivationServiceNumberMessage> createMinimalActivationServiceNumber() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new ActivationServiceNumberBuilder()
                .setHeader(
                        "LOADA",
                        "LOADA",
                        "LOADB",
                        "LOADB")
                .setDossierId(dossierId)
                .setPlannedDateTime("20190101111000")
                .setPlatformProvider("PLAT0")
                .addActivationServiceNumberSeq()
                    .setNumberSeries("0800111111", "0800222222")
                    .setPop("pop")
                    .setTariffInfo("0,02000", "0,02000", TariffInfo.CurrencyEnum._0, "3", "3")
                    .finish()
                .build();
    }
}
