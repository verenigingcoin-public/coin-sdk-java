package nl.coin.sdk.np.service;

import nl.coin.sdk.np.messages.v3.Message;
import nl.coin.sdk.np.messages.v3.portingrequest.PortingRequestBuilder;
import nl.coin.sdk.np.messages.v3.portingrequest.PortingRequestMessage;
import nl.coin.sdk.np.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;

public class PortingRequestBuilderTest {

    @Test
    public void createPortingRequestFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        PortingRequestBuilder portingRequestBuilder = new PortingRequestBuilder()
                .setHeader("LOADA", "LOADB")
                .setRecipientserviceprovider("LOADB")
                .setRecipientnetworkoperator("LOADB")
                .setDonornetworkoperator("LOADA")
                .setDonorserviceprovider("LOADA")
                .setDossierId(dossierId)
                .setContract("EARLY_TERMINATION")
                .setCustomerInfo("test", "test bv", "1", "a", "1234AB", "1")
                .setNote("This is a note");
        for (int i = 0; i < 250; i++) {
            String lastDigits = String.format("%03d", i);
            String phoneNumber = "0612345" + lastDigits;
            portingRequestBuilder.addPortingRequestSeq()
                    .setNumberSeries(phoneNumber, phoneNumber)
                    .setProfileids("PROF1", "PROF2", "PROF3", "PROF4", "PROF5").finish();
        }
        Message<PortingRequestMessage> message = portingRequestBuilder.build();

        Assert.assertEquals("Donor serviceprovider should be LOADA", "LOADA", message.getMessage().getBody().getPortingrequest().getDonorserviceprovider());
    }
}
