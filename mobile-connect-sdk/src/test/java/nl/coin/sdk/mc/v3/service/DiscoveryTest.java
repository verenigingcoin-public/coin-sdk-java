package nl.coin.sdk.mc.v3.service;

import nl.coin.sdk.mc.v3.MobileConnectAutoConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {MobileConnectAutoConfiguration.class})
public class DiscoveryTest {

    @Autowired
    private IMobileConnectService mobileConnectService;

    @Test
    public void sendDiscoveryRequest() {
        Assert.assertNotNull(mobileConnectService.sendDiscoveryRequest("123456789"));
    }

    @Test
    public void sendDiscoveryRequestNotFound() {
        Assert.assertNull(mobileConnectService.sendDiscoveryRequest("123456789", "404"));
    }

    @Test
    public void sendDiscoveryRequestOtherError() {
        HttpClientErrorException exception = Assert.assertThrows(
                HttpClientErrorException.class,
                () -> mobileConnectService.sendDiscoveryRequest("123456789", "403")
        );
        Assert.assertEquals(403, exception.getStatusCode().value());
    }
}
