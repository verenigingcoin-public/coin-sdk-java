package nl.coin.sdk.mc.v3.service;

import nl.coin.sdk.mc.v3.MobileConnectAutoConfiguration;
import nl.coin.sdk.mc.v3.domain.DiscoveryRequest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.*;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {MobileConnectAutoConfiguration.class})
public class DiscoveryCustomErrorHandlerTest {

    @Autowired
    private IMobileConnectService mobileConnectService;

    @MockitoBean
    IMobileConnectClientErrorHandler clientErrorHandler;

    @AfterEach
    public void resetMock() {
        verifyNoMoreInteractions(clientErrorHandler);
        reset(clientErrorHandler);
    }

    @Test
    public void sendDiscoveryRequest() {
        Assert.assertNotNull(mobileConnectService.sendDiscoveryRequest("123456789"));
    }

    @Test
    public void sendDiscoveryRequestNotFound() {
        DiscoveryRequest request = new DiscoveryRequest("123456789", "404");
        Assert.assertNull(mobileConnectService.sendDiscoveryRequest(request));
        verify(clientErrorHandler, times(1)).onNotFound(same(request), any());
    }

    @Test
    public void sendDiscoveryRequestOtherError() {
        DiscoveryRequest request = new DiscoveryRequest("123456789", "403");
        Assert.assertNull(mobileConnectService.sendDiscoveryRequest(request));
        verify(clientErrorHandler, times(1)).onOtherError(same(request), any());
    }
}
