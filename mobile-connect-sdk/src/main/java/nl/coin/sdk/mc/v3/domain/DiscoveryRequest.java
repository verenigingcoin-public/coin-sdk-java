package nl.coin.sdk.mc.v3.domain;

import org.jetbrains.annotations.NotNull;

public record DiscoveryRequest(@NotNull String msisdn, String correlationId) {
}
