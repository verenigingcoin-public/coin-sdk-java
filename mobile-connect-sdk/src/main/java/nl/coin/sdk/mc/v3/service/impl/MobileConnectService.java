package nl.coin.sdk.mc.v3.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import nl.coin.sdk.common.client.CtpApiRestTemplateSupport;
import nl.coin.sdk.common.crypto.HmacSha256Signer;
import nl.coin.sdk.mc.v3.domain.DiscoveryRequest;
import nl.coin.sdk.mc.v3.domain.DiscoveryResponse;
import nl.coin.sdk.mc.v3.domain.ErrorResponse;
import nl.coin.sdk.mc.v3.service.IMobileConnectClientErrorHandler;
import nl.coin.sdk.mc.v3.service.IMobileConnectService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.NoOpResponseErrorHandler;

import java.io.IOException;
import java.security.PrivateKey;

@Service
public class MobileConnectService extends CtpApiRestTemplateSupport implements IMobileConnectService {

    private final String discoveryUrl;
    private final IMobileConnectClientErrorHandler errorHandler;

    @Autowired
    public MobileConnectService(
            @NotNull @Value("${discovery.url}") String discoveryUrl,
            @NotNull @Value("${consumer.name}") String consumerName,
            @NotNull HmacSha256Signer signer,
            @NotNull PrivateKey privateKey,
            @NotNull IMobileConnectClientErrorHandler errorHandler
    ) {
        super(consumerName, signer, privateKey);
        this.discoveryUrl = discoveryUrl;
        this.errorHandler = errorHandler;
        restTemplate.setErrorHandler(new NoOpResponseErrorHandler());
    }

    @Override
    public DiscoveryResponse sendDiscoveryRequest(@NotNull DiscoveryRequest discoveryRequest) {
        ResponseEntity<String> response = doSendWithToken(String.class, HttpMethod.POST, discoveryUrl, discoveryRequest);
        String responseBody = response.getBody();

        if (!response.getStatusCode().isError()) {
            try {
                return mapper.readValue(responseBody, DiscoveryResponse.class);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        } else if (response.getStatusCode().value() == 404) {
            try {
                ErrorResponse errorResponse = mapper.readValue(responseBody, ErrorResponse.class);
                errorHandler.onNotFound(discoveryRequest, errorResponse);
                return null;
            } catch (IOException ignored) {
            }
        }

        errorHandler.onOtherError(discoveryRequest, response);
        return null;
    }
}
