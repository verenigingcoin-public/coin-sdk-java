package nl.coin.sdk.mc.v3.domain;

import org.jetbrains.annotations.NotNull;

import java.util.Set;

public record SupportedServices(@NotNull Set<Link> match, @NotNull Set<Link> numberVerify, @NotNull Set<Link> accountTakeoverProtection) {
}
