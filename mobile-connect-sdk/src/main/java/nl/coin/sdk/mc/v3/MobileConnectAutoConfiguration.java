package nl.coin.sdk.mc.v3;

import nl.coin.sdk.common.client.SseConsumer;
import nl.coin.sdk.mc.v3.service.impl.MobileConnectClientErrorHandler;
import nl.coin.sdk.mc.v3.service.IMobileConnectClientErrorHandler;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(value = "nl.coin", excludeFilters = {@ComponentScan.Filter(classes = {SseConsumer.class}, type = FilterType.ASSIGNABLE_TYPE)})
public class MobileConnectAutoConfiguration {
    @Bean
    @ConditionalOnMissingBean(IMobileConnectClientErrorHandler.class)
    public IMobileConnectClientErrorHandler mobileConnectClientErrorHandler() {
        return new MobileConnectClientErrorHandler();
    }
}
