package nl.coin.sdk.mc.v3.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;

public record ErrorResponse(@NotNull String error, @NotNull String description, @JsonProperty("correlation_id") String correlationId) {
}
