package nl.coin.sdk.mc.v3.domain;

import org.jetbrains.annotations.NotNull;

public record Link(@NotNull String rel, @NotNull String href) {
}
