package nl.coin.sdk.mc.v3.service.impl;

import nl.coin.sdk.mc.v3.domain.DiscoveryRequest;
import nl.coin.sdk.mc.v3.domain.ErrorResponse;
import nl.coin.sdk.mc.v3.service.IMobileConnectClientErrorHandler;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Objects;

public class MobileConnectClientErrorHandler implements IMobileConnectClientErrorHandler {
    @Override
    public void onNotFound(@NotNull DiscoveryRequest request, @NotNull ErrorResponse errorResponse) {
    }

    @Override
    public void onOtherError(@NotNull DiscoveryRequest request, @NotNull ResponseEntity<String> response) {
        String responseBody = Objects.requireNonNullElse(response.getBody(), "");
        throw new HttpClientErrorException(response.getStatusCode(), response.getStatusCode().toString(), response.getHeaders(), responseBody.getBytes(), null);
    }
}
