package nl.coin.sdk.mc.v3.service;

import nl.coin.sdk.mc.v3.domain.DiscoveryRequest;
import nl.coin.sdk.mc.v3.domain.ErrorResponse;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;

public interface IMobileConnectClientErrorHandler {
    void onNotFound(@NotNull DiscoveryRequest request, @NotNull ErrorResponse errorResponse);

    void onOtherError(@NotNull DiscoveryRequest request, @NotNull ResponseEntity<String> response);
}

