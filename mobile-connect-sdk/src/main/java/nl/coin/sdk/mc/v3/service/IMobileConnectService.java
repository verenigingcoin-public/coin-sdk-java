package nl.coin.sdk.mc.v3.service;

import nl.coin.sdk.mc.v3.domain.DiscoveryRequest;
import nl.coin.sdk.mc.v3.domain.DiscoveryResponse;
import org.jetbrains.annotations.NotNull;

public interface IMobileConnectService {

    default DiscoveryResponse sendDiscoveryRequest(@NotNull String msisdn) {
        return sendDiscoveryRequest(new DiscoveryRequest(msisdn, null));
    }

    default DiscoveryResponse sendDiscoveryRequest(@NotNull String msisdn, String correlationId) {
        return sendDiscoveryRequest(new DiscoveryRequest(msisdn, correlationId));
    }

    DiscoveryResponse sendDiscoveryRequest(@NotNull DiscoveryRequest discoveryRequest);
}
