package nl.coin.sdk.mc.v3.domain;

import org.jetbrains.annotations.NotNull;

public record DiscoveryResponse(@NotNull String networkOperatorCode, @NotNull SupportedServices supportedServices, String correlationId, String clientId, String clientSecret) {
}

