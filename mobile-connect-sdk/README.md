# Java Mobile Connect SDK

## Introduction

The Java SDK supports secured access to the Mobile Connect API using the Spring Framework (Spring Boot 3.x).

## Setup

### Samples Project for the Mobile Connect API
COIN provides a sample project in the `mobile-connect-sdk-samples` directory.

### Manual Installation
For manual installation of the Mobile Connect SDK, add the `mobile-connect-sdk` dependency to your build file.

#### Maven
If you are using Maven:
- add the COIN repository
```xml
<repositories>
    <repository>
        <id>gitlab-repo</id>
        <url>https://gitlab.com/api/v4/projects/12541046/packages/maven</url>
        <layout>default</layout>
    </repository>
</repositories>
```
- add the dependency to your pom.xml file:
```xml
<dependency>
    <groupId>nl.coin</groupId>
    <artifactId>mobile-connect-sdk</artifactId>
    <version>{latest-version}</version>
</dependency>
```

#### Gradle
If you are using Gradle:
- add the COIN repository:

```
repositories {
    maven {
        url 'https://gitlab.com/api/v4/projects/12541046/packages/maven'
    }
}

```
- add the dependency to the dependencies block:

```
compile 'nl.coin:mobile-conect-sdk:{latest-version}'
```

## Code
Import `MobileConnectAutoConfiguration` in your Spring Boot application. The `IMobileConnectService` bean can be used to send discovery requests.
By default, it returns `null` if the API returns a `404 not found`; other errors result in an exception being thrown.
If desired, this behavior can be overridden by providing a bean of type `IMobileConnectClientErrorHandler`.

## Configure Credentials
For secure access various credentials are required.
- Check [this README](https://gitlab.com/verenigingcoin-public/consumer-configuration/-/blob/master/README.md) to find out how to configure these.
- To summarize, you will need:
    - Consumer name
    - `private-key.pem` file
    - `sharedkey.encrypted` encrypted (by public key) HMAC secret file

Add the following properties to the `/src/main/resources/application.properties` file :
```properties
#Name of the consumer as configured in: https://test-portal.coin.nl/iam#/
consumer.name=<your-consumer-name>
#Path to private key file
private.key.file.name=path-to/private-key.pem
#Path to encrypted HMAC secret for given consumer as copied from:  https://test-portal.coin.nl/iam#/
secret.file.name=path-to/sharedkey.encrypted
```

## Configure Logging
The implementation provided uses the HttpClient from Apache Commons to send and receive messages. Logging of the messages (header or content) can be enabled by setting the 
following properties to the desired log-level:
```properties
#Http Logging settings
org.apache.commons.logging.simplelog.log.httpclient.wire.header=WARN
org.apache.commons.logging.simplelog.log.httpclient.wire.content=WARN
```
More information about the possibilities can be found at the site of the [Apache Commons HttpClient](https://hc.apache.org/httpcomponents-client-4.5.x/logging.html)
