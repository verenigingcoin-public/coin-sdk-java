package nl.coin.sdk.wm.service;

import nl.coin.sdk.wm.messages.v1.MessageType;
import nl.coin.sdk.wm.service.impl.WeasMigrationMessageConsumer;
import nl.coin.sdk.wm.service.util.ExampleLoggingWeasMigrationMessageListener;
import nl.coin.sdk.wm.service.util.InMemoryOffsetPersister;
import nl.coin.sdk.common.client.SseConsumer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.concurrent.CountDownLatch;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class OpenStreamWithWrongConsumer {

    @Autowired
    private SseConsumer sseConsumer;

    @Autowired
    private WeasMigrationMessageConsumer weasMigrationMessageConsumer;

    @Autowired
    private ExampleLoggingWeasMigrationMessageListener listener;

    @Test(timeout = 20000)
    public void stopListeningWhenConnectFails() throws InterruptedException {
        Object originalValue = ReflectionTestUtils.getField(sseConsumer, "consumerName");
        ReflectionTestUtils.setField(sseConsumer, "consumerName", "ne-consumer");
        CountDownLatch latch = new CountDownLatch(1);
        try {
            weasMigrationMessageConsumer.startConsumingAll(listener, new InMemoryOffsetPersister(), latch::countDown,
                MessageType.REQUEST_V1,
                MessageType.CANCEL_V1,
                MessageType.REQUEST_ANSWER_V1);
            latch.await();
        } finally {
            ReflectionTestUtils.setField(sseConsumer, "consumerName", originalValue);
        }
    }
}
