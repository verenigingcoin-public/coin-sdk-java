package nl.coin.sdk.wm.service;

import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.request.WeasMigrationRequestBuilder;
import nl.coin.sdk.wm.messages.v1.request.WeasMigrationRequestMessage;
import nl.coin.sdk.wm.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;

public class WeasMigrationRequestBuilderTest {

    @Test
    public void createWeasMigrationRequestFull() {
        String dossierId = TestUtil.generateRandomDossierId();
        WeasMigrationRequestBuilder contractterminationRequestBuilder = new WeasMigrationRequestBuilder()
                .setHeader("LOADA", "LOADB")
                .setRecipientserviceprovider("LOADB")
                .setRecipientnetworkoperator("LOADB")
                .setDonornetworkoperator("LOADA")
                .setDonorserviceprovider("LOADA")
                .setDossierId(dossierId)
                .setAddress("1234AB", "1")
                .setNote("This is a note");
        for (int i = 0; i < 250; i++) {
            String lastDigits = String.format("%03d", i);
            String phoneNumber = "0612345" + lastDigits;
            contractterminationRequestBuilder.addNumberSeries(phoneNumber, phoneNumber);
        }
        Message<WeasMigrationRequestMessage> message = contractterminationRequestBuilder.build();

        Assert.assertEquals("Donor serviceprovider should be LOADA", "LOADA", message.getMessage().getBody().getContractterminationrequest().getDonorserviceprovider());
    }
}
