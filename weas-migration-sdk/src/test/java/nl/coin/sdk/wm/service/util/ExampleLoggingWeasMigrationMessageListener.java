package nl.coin.sdk.wm.service.util;

import nl.coin.sdk.wm.messages.v1.WeasMigrationMessage;
import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.MessageType;
import nl.coin.sdk.wm.messages.v1.cancel.WeasMigrationCancelMessage;
import nl.coin.sdk.wm.messages.v1.request.WeasMigrationRequestMessage;
import nl.coin.sdk.wm.messages.v1.requestanswer.WeasMigrationRequestAnswerMessage;
import nl.coin.sdk.wm.messages.v1.errorfound.WeasMigrationErrorFoundMessage;
import nl.coin.sdk.wm.service.IWeasMigrationMessageListener;
import nl.coin.sdk.wm.service.impl.WeasMigrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class ExampleLoggingWeasMigrationMessageListener implements IWeasMigrationMessageListener {
    private final WeasMigrationService weasMigrationService;
    private StreamTestResultTracker resultTracker;

    @Autowired
    public ExampleLoggingWeasMigrationMessageListener(
            WeasMigrationService weasMigrationService) {
        this.weasMigrationService = weasMigrationService;
    }

    private CountDownLatch latch;

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }

    public void setResultTracker(StreamTestResultTracker resultTracker) {
        this.resultTracker = resultTracker;
    }

    @Override
    public void onKeepAlive() {
    }

    @Override
    public void onUnknownMessage(String messageId, Message message) {

    }

    @Override
    public void onException(Exception exception) {

    }

    @Override
    public void onRequest(String messageId, WeasMigrationRequestMessage message) {
        setMessageAsReceivedInResult(MessageType.REQUEST_V1);
        processMessage(messageId, message);
    }

    void setMessageAsReceivedInResult(MessageType contractterminationRequestV1) {
        if (this.resultTracker != null) {
            this.resultTracker.setResult(contractterminationRequestV1.getType(), Boolean.TRUE);
        }
    }

    @Override
    public void onRequestAnswer(String messageId, WeasMigrationRequestAnswerMessage message) {
        setMessageAsReceivedInResult(MessageType.REQUEST_ANSWER_V1);
        processMessage(messageId, message);
    }

    @Override
    public void onCancel(String messageId, WeasMigrationCancelMessage message) {
        setMessageAsReceivedInResult(MessageType.CANCEL_V1);
        processMessage(messageId, message);
    }

    @Override
    public void onErrorFound(String messageId, WeasMigrationErrorFoundMessage message) {
        setMessageAsReceivedInResult(MessageType.ERROR_FOUND_V1);
        processMessage(messageId, message);
    }

    private void processMessage(String messageId, WeasMigrationMessage message) {
        System.out.println("message = " + message.toString());
        weasMigrationService.sendConfirmation(messageId);
        if (latch != null) latch.countDown();
    }
}
