package nl.coin.sdk.wm.service;

import nl.coin.sdk.wm.messages.v1.MessageType;
import nl.coin.sdk.wm.service.impl.WeasMigrationMessageConsumer;
import nl.coin.sdk.wm.service.util.ExampleLoggingWeasMigrationMessageListener;
import nl.coin.sdk.wm.service.util.InMemoryOffsetPersister;
import nl.coin.sdk.wm.service.util.StreamTestResultTracker;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.concurrent.CountDownLatch;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class OpenStreamTest {

    @Autowired
    private WeasMigrationMessageConsumer weasMigrationMessageConsumer;

    @Autowired
    private ExampleLoggingWeasMigrationMessageListener listener;

    @AfterEach
    public void stopStream() {
        weasMigrationMessageConsumer.stopConsuming();
    }

    @Test(timeout = 20000)
    public void openStream() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(5);
        listener.setLatch(latch);
        weasMigrationMessageConsumer.startConsumingUnconfirmed(listener, Assertions::fail);
        latch.await();
    }

    @Test(timeout = 30000)
    public void openStreamAndHandleAllMessageTypes() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(30);
        listener.setLatch(latch);

        StreamTestResultTracker tracker = new StreamTestResultTracker();
        listener.setResultTracker(tracker);
        weasMigrationMessageConsumer.startConsumingUnconfirmedWithOffsetPersistence(listener, new InMemoryOffsetPersister(), Assertions::fail);
        latch.await();

        Assert.assertFalse("For at least one message the handler isn't called: " + tracker.getResults(), tracker.getResults().containsValue(Boolean.FALSE));
    }

    @Test(timeout = 20000)
    public void openSpecificStreamForAllMessagesAndOneProvider() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(5);
        listener.setLatch(latch);
        weasMigrationMessageConsumer.startConsumingAll(listener, new InMemoryOffsetPersister(),
            Collections.singletonList("LOADA"),
            () -> {},
            MessageType.REQUEST_V1,
            MessageType.CANCEL_V1,
            MessageType.REQUEST_ANSWER_V1);
        latch.await();
    }
}
