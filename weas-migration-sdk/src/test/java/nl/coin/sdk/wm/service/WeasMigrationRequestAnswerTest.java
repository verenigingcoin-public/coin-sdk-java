package nl.coin.sdk.wm.service;

import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.MessageType;
import nl.coin.sdk.wm.messages.v1.common.MessageResponse;
import nl.coin.sdk.wm.messages.v1.requestanswer.WeasMigrationRequestAnswerBuilder;
import nl.coin.sdk.wm.messages.v1.requestanswer.WeasMigrationRequestAnswerMessage;
import nl.coin.sdk.wm.service.impl.WeasMigrationService;
import nl.coin.sdk.wm.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class WeasMigrationRequestAnswerTest {

    @Autowired
    private WeasMigrationService weasMigrationService;

    @Test
    public void sendWeasMigrationRequestAnswerMinimal() {
        sendMessage(createWeasMigrationRequestAnswerMinimal());
    }

    @Test
    public void sendWeasMigrationRequestAnswerFull() {
        sendMessage(createWeasMigrationRequestAnswerFull());
    }

    private Message<WeasMigrationRequestAnswerMessage> createWeasMigrationRequestAnswerMinimal() {
        String dossierId = TestUtil.generateRandomDossierId();

        return new WeasMigrationRequestAnswerBuilder()
            .setHeader("LOADA", "LOADB")
            .setDossierId(dossierId)
            .setBlocking(false)
            .setBlockingcode("0")
            .build();
    }

    private Message<WeasMigrationRequestAnswerMessage> createWeasMigrationRequestAnswerFull() {
        String dossierId = TestUtil.generateRandomDossierId();

        return new WeasMigrationRequestAnswerBuilder()
            .setHeader(
                "LOADA",
                "LOADA",
                "LOADB",
                "LOADB")
            .setDossierId(dossierId)
            .setBlocking(true)
            .setBlockingcode("43")
            .setFirstPossibleDate(LocalDateTime.now())
            .setInfraBlock("prov", "type", "4")
            .build();
    }

    private void sendMessage(Message<WeasMigrationRequestAnswerMessage> message) {
        Assert.assertEquals(MessageType.REQUEST_ANSWER_V1, message.getMessageType());
        MessageResponse messageResponse = weasMigrationService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
