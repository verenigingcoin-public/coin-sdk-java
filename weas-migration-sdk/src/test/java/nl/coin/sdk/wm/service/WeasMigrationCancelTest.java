package nl.coin.sdk.wm.service;

import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.MessageType;
import nl.coin.sdk.wm.messages.v1.cancel.WeasMigrationCancelBuilder;
import nl.coin.sdk.wm.messages.v1.cancel.WeasMigrationCancelMessage;
import nl.coin.sdk.wm.messages.v1.common.MessageResponse;
import nl.coin.sdk.wm.service.impl.WeasMigrationService;
import nl.coin.sdk.wm.service.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class WeasMigrationCancelTest {

    @Autowired
    private WeasMigrationService weasMigrationService;

    @Test
    public void sendMinimalCancel() {
        sendMessage(createMinimalCancel());
    }

    @Test
    public void sendFullCancel() {
        sendMessage(createFullCancel());
    }

    private Message<WeasMigrationCancelMessage> createMinimalCancel() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new WeasMigrationCancelBuilder()
            .setHeader("LOADA", "LOADB")
            .setDossierId(dossierId)
            .setNote("Just a note")
            .build();
    }

    private Message<WeasMigrationCancelMessage> createFullCancel() {
        String dossierId = TestUtil.generateRandomDossierId();
        return new WeasMigrationCancelBuilder()
            .setHeader(
                "LOADA",
                "LOADA",
                "LOADB",
                "LOADB")
            .setDossierId(dossierId)
            .setNote("Just a note")
            .build();
    }

    private void sendMessage(Message<WeasMigrationCancelMessage> message) {
        Assert.assertEquals(MessageType.CANCEL_V1, message.getMessageType());
        MessageResponse messageResponse = weasMigrationService.sendMessage(message);
        Assert.assertFalse(messageResponse.getTransactionId().isEmpty());
    }
}
