package nl.coin.sdk.wm.service;

import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.request.WeasMigrationRequestBuilder;
import nl.coin.sdk.wm.messages.v1.request.WeasMigrationRequestMessage;
import nl.coin.sdk.wm.service.impl.WeasMigrationService;
import nl.coin.sdk.wm.service.impl.RestClientErrorException;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

@ActiveProfiles({"default", "test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@IfProfileValue(name = "run-create-message-tests", value = "true")
public class InvalidMessageTest {

    @Autowired
    private WeasMigrationService weasMigrationService;

    @Test
    public void sendInvalidMessage() {
        Message<WeasMigrationRequestMessage> message = new WeasMigrationRequestBuilder()
            .addNumberSeries("0612345678", "0612345678")
            .addValidationBlock("name", "value")
            .setAddress("1234AB", "23")
            .setRecipientserviceprovider("LOADA")
            .setDonorserviceprovider("LOADB")
            .setName("a name")
            .setEarlytermination(true)
            .build();
        try {
            weasMigrationService.sendMessage(message);
        } catch (HttpClientErrorException e) {
            Assert.assertFalse(((RestClientErrorException) e).getTransactionId().isEmpty());
            Assert.assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
            Assert.assertThat(
                e.getMessage(),
                CoreMatchers.containsString("1000: Provide mandatory field message.header.receiver")
            );
        }
    }
}
