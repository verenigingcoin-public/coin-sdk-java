package nl.coin.sdk.wm.messages.v1;

public enum MessageType {

    CANCEL_V1("weasmigrationcancel"),
    REQUEST_V1("weasmigrationrequest"),
    REQUEST_ANSWER_V1("weasmigrationrequestanswer"),
    ERROR_FOUND_V1("weasmigrationerrorfound");

    final private String type;
    final private String eventName;

    private static final String VERSION_SUFFIX_V1 = "-v1";

    MessageType(String type) {
        this.type = type;
        this.eventName = type + VERSION_SUFFIX_V1;
    }

    public String getType() {
        return type;
    }

    public String getEventName() {
        return eventName;
    }

    public String getUrlName() {
        return type.replace("weasmigration", "");
    }

}
