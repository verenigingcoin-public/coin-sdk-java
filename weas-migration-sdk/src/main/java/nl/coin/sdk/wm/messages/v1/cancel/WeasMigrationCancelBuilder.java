package nl.coin.sdk.wm.messages.v1.cancel;

import nl.coin.sdk.wm.messages.v1.common.HeaderCreator;
import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.MessageType;
import org.apache.commons.lang3.builder.Builder;

public class WeasMigrationCancelBuilder extends HeaderCreator<WeasMigrationCancelBuilder> implements Builder<Message<WeasMigrationCancelMessage>> {

    private WeasMigrationCancel cancel = new WeasMigrationCancel();

    @Override
    protected WeasMigrationCancelBuilder getThis() {
        return this;
    }

    public WeasMigrationCancelBuilder setDossierId(String dossierId) {
        cancel.setDossierid(dossierId);
        return this;
    }

    public WeasMigrationCancelBuilder setNote(String note) {
        cancel.setNote(note);
        return this;
    }

    public Message<WeasMigrationCancelMessage> build() {
        WeasMigrationCancelBody cancelBody = new WeasMigrationCancelBody().cancel(cancel);
        WeasMigrationCancelMessage message = new WeasMigrationCancelMessage()
                .header(header)
                .body(cancelBody);
        return new Message<>(message, MessageType.CANCEL_V1);
    }
}
