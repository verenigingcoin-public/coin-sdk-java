/*
 * COIN CRDB Rest API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: devops@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package nl.coin.sdk.wm.messages.v1.errorfound;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * ErrorFound
 */
public class WeasMigrationErrorFound {
    private String dossierid = null;

    private String messagetype = null;

    private String messageid = null;

    private String originalmessage = null;

    private String errorlog = null;

    private List<WeasMigrationErrorFoundRepeats> repeats = new ArrayList<>();

    public WeasMigrationErrorFound dossierid(String dossierid) {
        this.dossierid = dossierid;
        return this;
    }

    /**
     * The identifier for the Contract Termination Request determined by the Recipient for communication between the Recipient and Donor. Defined as [RecipientSPCode]-[DonorSPCode]-[Identifier] The requirement is that the dossierid is unique and identifies the contract termination request.
     *
     * @return dossierid
     **/
    public String getDossierid() {
        return dossierid;
    }

    public void setDossierid(String dossierid) {
        this.dossierid = dossierid;
    }

    public WeasMigrationErrorFound messagetype(String messagetype) {
        this.messagetype = messagetype;
        return this;
    }

    /**
     * The message type of the message in which one or more errors have been found.
     *
     * @return messagetype
     **/
    public String getMessagetype() {
        return messagetype;
    }

    public void setMessagetype(String messagetype) {
        this.messagetype = messagetype;
    }

    public WeasMigrationErrorFound messageid(String messageid) {
        this.messageid = messageid;
        return this;
    }

    /**
     * The unique technical id of the message in which one or more errors have been found
     *
     * @return messageid
     **/
    public String getMessageid() {
        return messageid;
    }

    public void setMessageid(String messageid) {
        this.messageid = messageid;
    }

    public WeasMigrationErrorFound originalmessage(String originalmessage) {
        this.originalmessage = originalmessage;
        return this;
    }

    /**
     * The processed message in which one or more errors have been found.
     *
     * @return originalmessage
     **/
    public String getOriginalmessage() {
        return originalmessage;
    }

    public void setOriginalmessage(String originalmessage) {
        this.originalmessage = originalmessage;
    }

    public WeasMigrationErrorFound errorlog(String errorlog) {
        this.errorlog = errorlog;
        return this;
    }

    /**
     * A log of errors that has been found when processing the message.
     *
     * @return errorlog
     **/
    public String getErrorlog() {
        return errorlog;
    }

    public void setErrorlog(String errorlog) {
        this.errorlog = errorlog;
    }

    public WeasMigrationErrorFound repeats(List<WeasMigrationErrorFoundRepeats> repeats) {
        this.repeats = repeats;
        return this;
    }

    public WeasMigrationErrorFound addRepeatsItem(WeasMigrationErrorFoundRepeats repeatsItem) {
        this.repeats.add(repeatsItem);
        return this;
    }

    /**
     * Get repeats
     *
     * @return repeats
     **/
    public List<WeasMigrationErrorFoundRepeats> getRepeats() {
        return repeats;
    }

    public void setRepeats(List<WeasMigrationErrorFoundRepeats> repeats) {
        this.repeats = repeats;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WeasMigrationErrorFound errorFound = (WeasMigrationErrorFound) o;
        return Objects.equals(this.dossierid, errorFound.dossierid) &&
            Objects.equals(this.messagetype, errorFound.messagetype) &&
            Objects.equals(this.messageid, errorFound.messageid) &&
            Objects.equals(this.originalmessage, errorFound.originalmessage) &&
            Objects.equals(this.errorlog, errorFound.errorlog) &&
            Objects.equals(this.repeats, errorFound.repeats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dossierid, messagetype, messageid, originalmessage, errorlog, repeats);
    }


    @Override
    public String toString() {

        return "class ErrorFound {\n" +
            "    dossierid: " + toIndentedString(dossierid) + "\n" +
            "    messagetype: " + toIndentedString(messagetype) + "\n" +
            "    messageid: " + toIndentedString(messageid) + "\n" +
            "    originalmessage: " + toIndentedString(originalmessage) + "\n" +
            "    errorlog: " + toIndentedString(errorlog) + "\n" +
            "    repeats: " + toIndentedString(repeats) + "\n" +
            "}";
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}
