package nl.coin.sdk.wm.service;

import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.cancel.WeasMigrationCancelMessage;
import nl.coin.sdk.wm.messages.v1.request.WeasMigrationRequestMessage;
import nl.coin.sdk.wm.messages.v1.requestanswer.WeasMigrationRequestAnswerMessage;
import nl.coin.sdk.wm.messages.v1.errorfound.WeasMigrationErrorFoundMessage;

public interface IWeasMigrationMessageListener {
    void onKeepAlive();

    void onUnknownMessage(String messageId, Message message);

    void onException(Exception exception);

    void onRequest(String messageId, WeasMigrationRequestMessage message);

    void onRequestAnswer(String messageId, WeasMigrationRequestAnswerMessage message);

    void onCancel(String messageId, WeasMigrationCancelMessage message);

    void onErrorFound(String messageId, WeasMigrationErrorFoundMessage message);
}
