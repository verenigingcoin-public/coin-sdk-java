package nl.coin.sdk.wm.messages.v1.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public abstract class HeaderCreator<T> {
    protected final Header header = new Header();

    protected abstract T getThis();

    public T setHeader(String sender, String receiver) {
        return setHeader(null, sender, null, receiver);
    }

    public T setHeader(String senderNetworkoperator, String senderServiceprovider, String receiverNetworkoperator, String receiverServiceprovider) {
        Sender sender = new Sender()
                .networkoperator(senderNetworkoperator)
                .serviceprovider(senderServiceprovider);
        Receiver receiver = new Receiver()
                .networkoperator(receiverNetworkoperator)
                .serviceprovider(receiverServiceprovider);
        header
                .timestamp(LocalDateTime.now().withNano(0).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                .sender(sender)
                .receiver(receiver);
        return getThis();
    }

    public T setMessageId(String messageId) {
        header.setMessageid(messageId);
        return getThis();
    }
}
