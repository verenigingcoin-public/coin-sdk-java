package nl.coin.sdk.wm.messages.v1.request;

import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.MessageType;
import nl.coin.sdk.wm.messages.v1.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

public class WeasMigrationRequestBuilder extends HeaderCreator<WeasMigrationRequestBuilder> implements Builder<Message<WeasMigrationRequestMessage>> {
    private WeasMigrationRequest contractTerminationRequest = new WeasMigrationRequest();

    @Override
    protected WeasMigrationRequestBuilder getThis() {
        return this;
    }

    public WeasMigrationRequestBuilder setRecipientnetworkoperator(String recipientnetworkoperator) {
        contractTerminationRequest.setRecipientnetworkoperator(recipientnetworkoperator);
        return this;
    }

    public WeasMigrationRequestBuilder setRecipientserviceprovider(String recipientserviceprovider) {
        contractTerminationRequest.setRecipientserviceprovider(recipientserviceprovider);
        return this;
    }

    public WeasMigrationRequestBuilder setDonornetworkoperator(String donornetworkoperator) {
        contractTerminationRequest.setDonornetworkoperator(donornetworkoperator);
        return this;
    }

    public WeasMigrationRequestBuilder setDonorserviceprovider(String donorserviceprovider) {
        contractTerminationRequest.setDonorserviceprovider(donorserviceprovider);
        return this;
    }

    public WeasMigrationRequestBuilder setDossierId(String dossierid) {
        contractTerminationRequest.setDossierid(dossierid);
        return this;
    }

    public WeasMigrationRequestBuilder setNote(String note) {
        contractTerminationRequest.setNote(note);
        return this;
    }

    public WeasMigrationRequestBuilder setName(String name) {
        contractTerminationRequest.setName(name);
        return this;
    }

    public WeasMigrationRequestBuilder setEarlytermination(Boolean earlytermination) {
        contractTerminationRequest.setEarlytermination(earlytermination ? "Y" : "N");
        return this;
    }

    public WeasMigrationRequestBuilder setAddress(String postcode, String housenr) {
        return setAddress(postcode, housenr, null);
    }

    public WeasMigrationRequestBuilder setAddress(String postcode, String housenr, String housenrExt) {
        AddressBlock address = new AddressBlock();
        address.setPostcode(postcode);
        address.setHousenr(housenr);
        address.setHousenr_ext(housenrExt);
        contractTerminationRequest.setAddressblock(address);
        return this;
    }

    public WeasMigrationRequestBuilder addNumberSeries(String start, String end) {
        NumberSeries numberSeries = new NumberSeries();
        numberSeries.setStart(start);
        numberSeries.setEnd(end);
        contractTerminationRequest.addNumberseriesItem(numberSeries);
        return this;
    }

    public WeasMigrationRequestBuilder addValidationBlock(String name, String value) {
        ValidationBlock validation = new ValidationBlock();
        validation.setName(name);
        validation.setValue(value);
        contractTerminationRequest.addValidationblockItem(validation);
        return this;
    }

    public Message<WeasMigrationRequestMessage> build() {
        WeasMigrationRequestBody contractTerminationRequestBody = new WeasMigrationRequestBody().contractterminationrequest(contractTerminationRequest);
        WeasMigrationRequestMessage message = new WeasMigrationRequestMessage()
                .header(header)
                .body(contractTerminationRequestBody);
        return new Message<>(message, MessageType.REQUEST_V1);
    }
}
