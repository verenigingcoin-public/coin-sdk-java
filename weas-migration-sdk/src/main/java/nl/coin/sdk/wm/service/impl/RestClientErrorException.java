package nl.coin.sdk.wm.service.impl;

import nl.coin.sdk.wm.messages.v1.common.ErrorContent;
import nl.coin.sdk.wm.messages.v1.common.ErrorResponse;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

public class RestClientErrorException extends HttpClientErrorException {
    private ErrorResponse errorResponse = new ErrorResponse();

    public RestClientErrorException(HttpStatusCode statusCode) {
        super(statusCode);
    }

    public RestClientErrorException(HttpStatusCode statusCode, String message) {
        super(statusCode, message);
    }

    public RestClientErrorException(HttpStatusCode statusCode, ErrorResponse errorResponse) {
        this(statusCode, errorResponse.toString());
        this.errorResponse = errorResponse;
    }

    public String getTransactionId() {
        return errorResponse.getTransactionId();
    }

    public List<ErrorContent> getErrorMessages() {
        return errorResponse.getErrors();
    }
}
