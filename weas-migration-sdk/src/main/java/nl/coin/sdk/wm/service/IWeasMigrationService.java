package nl.coin.sdk.wm.service;

import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.common.MessageResponse;

public interface IWeasMigrationService {

    void sendConfirmation(String id);

    MessageResponse sendMessage(Message message);
}
