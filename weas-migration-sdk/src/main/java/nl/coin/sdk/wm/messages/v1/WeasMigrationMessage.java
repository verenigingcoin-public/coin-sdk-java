package nl.coin.sdk.wm.messages.v1;

import nl.coin.sdk.wm.messages.v1.common.Header;

public interface WeasMigrationMessage<T> {
    Header getHeader();
    void setHeader(Header header);
    String getDossierid();
    T getBody();
}
