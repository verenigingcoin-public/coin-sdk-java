package nl.coin.sdk.wm.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.coin.sdk.wm.messages.v1.WeasMigrationMessage;
import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.MessageType;
import nl.coin.sdk.wm.messages.v1.cancel.WeasMigrationCancelMessage;
import nl.coin.sdk.wm.messages.v1.request.WeasMigrationRequestMessage;
import nl.coin.sdk.wm.messages.v1.requestanswer.WeasMigrationRequestAnswerMessage;
import nl.coin.sdk.wm.messages.v1.errorfound.WeasMigrationErrorFoundMessage;
import nl.coin.sdk.wm.service.IWeasMigrationMessageListener;
import nl.coin.sdk.common.client.IOffsetPersister;
import nl.coin.sdk.common.client.SseConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

@Service
public class WeasMigrationMessageConsumer {

    private final SseConsumer sseConsumer;
    private final ObjectMapper mapper;

    @Autowired
    public WeasMigrationMessageConsumer(SseConsumer sseConsumer, ObjectMapper mapper) {
        this.sseConsumer = sseConsumer;
        this.mapper = mapper;
    }

    /**
     * Recommended method for consuming messages. On connect or reconnect
     * it will consume all unconfirmed messages.
     */
    public void startConsumingUnconfirmed(
        IWeasMigrationMessageListener listener,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmed(
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Method for consuming messages. On connect or reconnect
     * it will consume all unconfirmed messages.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmed(
        IWeasMigrationMessageListener listener,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmed(
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Method for consuming messages. On connect or reconnect
     * it will consume all unconfirmed messages for the provided service providers.
     * <br><br>
     * Use this method rather than {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, Runnable, MessageType...)} if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     */
    public void startConsumingUnconfirmed(
        IWeasMigrationMessageListener listener,
        List<String> serviceProviders,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmed(
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Method for consuming messages. On connect or reconnect
     * it will consume all unconfirmed messages for the provided service providers.
     * <br><br>
     * Use this method rather than {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, MessageType...)} if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, List, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmed(
        IWeasMigrationMessageListener listener,
        List<String> serviceProviders,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmed(
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, Runnable, MessageType...)} does not meet needs.
     */
    public void startConsumingAll(
        IWeasMigrationMessageListener listener,
        IOffsetPersister offsetPersister,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingAll(
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, MessageType...)} does not meet needs.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingAll(IWeasMigrationMessageListener, IOffsetPersister, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingAll(
        IWeasMigrationMessageListener listener,
        IOffsetPersister offsetPersister,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingAll(
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, List, Runnable, MessageType...)} does not meet needs.
     * <br><br>
     * Use this method rather than {@link #startConsumingAll(IWeasMigrationMessageListener, IOffsetPersister, Runnable, MessageType...)} if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     */
    public void startConsumingAll(
        IWeasMigrationMessageListener listener,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingAll(
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, List, MessageType...)} does not meet needs.
     * <br><br>
     * Use this method rather than {@link #startConsumingAll(IWeasMigrationMessageListener, IOffsetPersister, MessageType...)} if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingAll(IWeasMigrationMessageListener, IOffsetPersister, List, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingAll(
        IWeasMigrationMessageListener listener,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingAll(
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed, from a certain offset.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, Runnable, MessageType...)} does not meet needs.
     */
    public void startConsumingAll(
        IWeasMigrationMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingAll(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed, from a certain offset.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, MessageType...)} does not meet needs.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingAll(IWeasMigrationMessageListener, String, IOffsetPersister, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingAll(
        IWeasMigrationMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingAll(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed, from a certain offset.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, List, Runnable, MessageType...)} does not meet needs.
     * <br><br>
     * Use this method rather than {@link #startConsumingAll(IWeasMigrationMessageListener, String, IOffsetPersister, Runnable, MessageType...)}
     * if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     */
    public void startConsumingAll(
        IWeasMigrationMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingAll(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Consume all messages, both confirmed and unconfirmed, from a certain offset.
     * Only use for special cases if {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, List, MessageType...)} does not meet needs.
     * <br><br>
     * Use this method rather than {@link #startConsumingAll(IWeasMigrationMessageListener, String, IOffsetPersister, MessageType...)}
     * if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingAll(IWeasMigrationMessageListener, String, IOffsetPersister, List, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingAll(
        IWeasMigrationMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingAll(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, Runnable, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     */
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IWeasMigrationMessageListener listener,
        IOffsetPersister offsetPersister,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmedWithOffsetPersistence(IWeasMigrationMessageListener, IOffsetPersister, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IWeasMigrationMessageListener listener,
        IOffsetPersister offsetPersister,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, List, Runnable, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     * <br><br>
     * Use this method rather than {@link #startConsumingUnconfirmedWithOffsetPersistence(IWeasMigrationMessageListener, IOffsetPersister, Runnable, MessageType...)} if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     */
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IWeasMigrationMessageListener listener,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, List, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     * <br><br>
     * Use this method rather than {@link #startConsumingUnconfirmedWithOffsetPersistence(IWeasMigrationMessageListener, IOffsetPersister, MessageType...)} if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmedWithOffsetPersistence(IWeasMigrationMessageListener, IOffsetPersister, List, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IWeasMigrationMessageListener listener,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, List, Runnable, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     */
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IWeasMigrationMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, List, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmedWithOffsetPersistence(IWeasMigrationMessageListener, String, IOffsetPersister, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IWeasMigrationMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            new LinkedMultiValueMap<>(),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, List, Runnable, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     * <br><br>
     * Use this method rather than {@link #startConsumingUnconfirmedWithOffsetPersistence(IWeasMigrationMessageListener, String, IOffsetPersister, Runnable, MessageType...)}
     * if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     */
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IWeasMigrationMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        Runnable doAfterAllRetriesFailed,
        MessageType... messageTypes
    ) {
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            doAfterAllRetriesFailed
        );
    }

    /**
     * Only use this method for receiving unconfirmed messages if you make sure that all messages that are received through this
     * method will be confirmed otherwise, ideally in the stream opened by
     * {@link #startConsumingUnconfirmed(IWeasMigrationMessageListener, List, MessageType...)}. So this method should only be used for a secondary
     * stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
     * <br><br>
     * Use this method rather than {@link #startConsumingUnconfirmedWithOffsetPersistence(IWeasMigrationMessageListener, String, IOffsetPersister, MessageType...)}
     * if there are multiple service providers linked to your consumer
     * and you only want to consume messages for a subset of these providers.
     *
     * @deprecated As of release 1.7.0, deprecated in favor of {@link #startConsumingUnconfirmedWithOffsetPersistence(IWeasMigrationMessageListener, String, IOffsetPersister, List, Runnable, MessageType...)}.
     */
    @Deprecated
    public void startConsumingUnconfirmedWithOffsetPersistence(
        IWeasMigrationMessageListener listener,
        String offset,
        IOffsetPersister offsetPersister,
        List<String> serviceProviders,
        MessageType... messageTypes
    ) {
        Thread currentThread = Thread.currentThread();
        this.sseConsumer.startConsumingUnconfirmedWithOffsetPersistence(
            offset,
            offsetPersister,
            toStrings(messageTypes),
            toMap("serviceproviders", serviceProviders),
            sse -> handleSSE(listener, sse),
            currentThread::interrupt
        );
    }

    public void stopConsuming() {
        sseConsumer.stopConsuming();
    }

    private List<String> toStrings(MessageType... messageTypes) {
        return Arrays.stream(messageTypes).map(MessageType::getType).collect(Collectors.toList());
    }

    private LinkedMultiValueMap<String, String> toMap(String key, List<String> values) {
        LinkedMultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.put(key, values);
        return map;
    }

    protected String handleSSE(IWeasMigrationMessageListener listener, List<ServerSentEvent<String>> sses) {
        if (sses.size() != 1) throw new RuntimeException("Stream buffer size should be 1");
        ServerSentEvent<String> sse = sses.get(0);
        String event = sse.event();
        String messageId = sse.id();
        String data = sse.data();
        try {
            if (MessageType.REQUEST_V1.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<WeasMigrationRequestMessage>>() {
                }, listener::onRequest);
            } else if (MessageType.REQUEST_ANSWER_V1.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<WeasMigrationRequestAnswerMessage>>() {
                }, listener::onRequestAnswer);
            } else if (MessageType.CANCEL_V1.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<WeasMigrationCancelMessage>>() {
                }, listener::onCancel);
            } else if (MessageType.ERROR_FOUND_V1.getEventName().equalsIgnoreCase(event)) {
                handleEvent(messageId, data, new TypeReference<Message<WeasMigrationErrorFoundMessage>>() {
                }, listener::onErrorFound);
            } else if (sse.event() != null) {
                Message message = mapper.readValue(data, new TypeReference<Message>() {
                });
                listener.onUnknownMessage(messageId, message);
                return null;
            } else {
                listener.onKeepAlive();
                return null;
            }
            return messageId;
        } catch (IOException exception) {
            listener.onException(exception);
            return null;
        }
    }

    protected <T extends WeasMigrationMessage> void handleEvent(
        String messageId, String data, TypeReference<Message<T>> typeReference, BiConsumer<String, T> consumer) throws IOException {
        Message<T> message = mapper.readValue(data, typeReference);
        consumer.accept(messageId, message.getMessage());
    }
}
