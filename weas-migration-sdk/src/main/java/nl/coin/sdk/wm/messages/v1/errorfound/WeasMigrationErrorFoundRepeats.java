/*
 * COIN CRDB Rest API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: devops@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package nl.coin.sdk.wm.messages.v1.errorfound;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * ErrorRepeats
 */
public class WeasMigrationErrorFoundRepeats {
    @JsonProperty("seq")
    private WeasMigrationErrorFoundSeq seq = null;

    public WeasMigrationErrorFoundRepeats seq(WeasMigrationErrorFoundSeq seq) {
        this.seq = seq;
        return this;
    }

    /**
     * Get seq
     *
     * @return seq
     **/
    public WeasMigrationErrorFoundSeq getSeq() {
        return seq;
    }

    public void setSeq(WeasMigrationErrorFoundSeq seq) {
        this.seq = seq;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WeasMigrationErrorFoundRepeats errorFoundRepeats = (WeasMigrationErrorFoundRepeats) o;
        return Objects.equals(this.seq, errorFoundRepeats.seq);
    }

    @Override
    public int hashCode() {
        return Objects.hash(seq);
    }


    @Override
    public String toString() {

        return "class ErrorRepeats {\n" +
            "    seq: " + toIndentedString(seq) + "\n" +
            "}";
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}
