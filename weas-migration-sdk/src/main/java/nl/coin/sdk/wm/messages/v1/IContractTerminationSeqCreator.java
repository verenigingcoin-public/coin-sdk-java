package nl.coin.sdk.wm.messages.v1;

public interface IContractTerminationSeqCreator<BUILDER, SEQUENCE> {
    BUILDER setBuild(SEQUENCE p);
}
