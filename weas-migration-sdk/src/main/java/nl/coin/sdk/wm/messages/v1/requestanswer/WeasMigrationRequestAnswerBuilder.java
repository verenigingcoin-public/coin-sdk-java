package nl.coin.sdk.wm.messages.v1.requestanswer;

import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.MessageType;
import nl.coin.sdk.wm.messages.v1.common.HeaderCreator;
import org.apache.commons.lang3.builder.Builder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class WeasMigrationRequestAnswerBuilder extends HeaderCreator<WeasMigrationRequestAnswerBuilder> implements Builder<Message<WeasMigrationRequestAnswerMessage>> {
    private final WeasMigrationRequestAnswer contractTerminationRequestAnswer = new WeasMigrationRequestAnswer();

    @Override
    protected WeasMigrationRequestAnswerBuilder getThis() {
        return this;
    }

    public WeasMigrationRequestAnswerBuilder setDossierId(String dossierId) {
        contractTerminationRequestAnswer.setDossierid(dossierId);
        return this;
    }

    public WeasMigrationRequestAnswerBuilder setBlocking(Boolean blocking) {
        contractTerminationRequestAnswer.setBlocking(blocking ? "Y" : "N");
        return this;
    }

    public WeasMigrationRequestAnswerBuilder setNote(String note) {
        contractTerminationRequestAnswer.setNote(note);
        return this;
    }

    public WeasMigrationRequestAnswerBuilder setBlockingcode(String blockingcode) {
        contractTerminationRequestAnswer.setBlockingcode(blockingcode);
        return this;
    }

    public WeasMigrationRequestAnswerBuilder setFirstPossibleDate(String firstpossibledate) {
        contractTerminationRequestAnswer.setFirstpossibledate(firstpossibledate);
        return this;
    }

    public WeasMigrationRequestAnswerBuilder setFirstPossibleDate(LocalDateTime firstpossibledate) {
        contractTerminationRequestAnswer.setFirstpossibledate(firstpossibledate.withNano(0).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        return this;
    }

    public WeasMigrationRequestAnswerBuilder setInfraBlock(String infraprovider, String infratype, String infraid) {
        InfraBlock infra = new InfraBlock();
        infra.setInfraprovider(infraprovider);
        infra.setInfratype(infratype);
        infra.setInfraid(infraid);
        contractTerminationRequestAnswer.setInfrablock(infra);
        return this;
    }

    public Message<WeasMigrationRequestAnswerMessage> build() {
        WeasMigrationRequestAnswerBody body = new WeasMigrationRequestAnswerBody()
                .contractterminationrequestanswer(contractTerminationRequestAnswer);

        WeasMigrationRequestAnswerMessage message = new WeasMigrationRequestAnswerMessage()
                .header(header)
                .body(body);

        return new Message<>(message, MessageType.REQUEST_ANSWER_V1);
    }
}
