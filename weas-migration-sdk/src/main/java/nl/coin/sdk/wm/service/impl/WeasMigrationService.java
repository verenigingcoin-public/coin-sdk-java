package nl.coin.sdk.wm.service.impl;

import nl.coin.sdk.wm.messages.v1.Message;
import nl.coin.sdk.wm.messages.v1.MessageType;
import nl.coin.sdk.wm.messages.v1.common.ConfirmationMessage;
import nl.coin.sdk.wm.messages.v1.common.MessageResponse;
import nl.coin.sdk.wm.service.IWeasMigrationService;
import nl.coin.sdk.common.client.CtpApiRestTemplateSupport;
import nl.coin.sdk.common.crypto.HmacSha256Signer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.PrivateKey;

@Service
public class WeasMigrationService extends CtpApiRestTemplateSupport implements IWeasMigrationService {

    private final String apiUrl;

    @Autowired
    public WeasMigrationService(
            @Value("${api.url}") String apiUrl,
            @Value("${consumer.name}") String consumerName,
            HmacSha256Signer signer,
            PrivateKey privateKey,
            WeasMigrationClientErrorHandler errorHandler) {
        super(consumerName, signer, privateKey);
        this.apiUrl = apiUrl;
        restTemplate.setErrorHandler(errorHandler);
    }

    @Override
    public MessageResponse sendMessage(Message message) {
        return postMessage(message, message.getMessageType());
    }

    @Override
    public void sendConfirmation(String id) {
        ConfirmationMessage confirmationMessage = new ConfirmationMessage().transactionId(id);
        String url = apiUrl + "/dossiers/confirmations/" + id;
        sendWithToken(String.class, HttpMethod.PUT, url, confirmationMessage);
    }

    private MessageResponse postMessage(Message message, MessageType messageType) {
        String url = apiUrl + "/dossiers/" + messageType.getUrlName();
        String responseBody = sendWithToken(String.class, HttpMethod.POST, url, message);
        try {
            return mapper.readValue(responseBody, MessageResponse.class);
        } catch (IOException e) {
            return new MessageResponse();
        }
    }
}
