package nl.coin.sdk.wm.messages.v1.common;

public class ErrorContent {
    private String code = null;

    private String message = null;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return code + ": " + message + "\n";
    }
}
