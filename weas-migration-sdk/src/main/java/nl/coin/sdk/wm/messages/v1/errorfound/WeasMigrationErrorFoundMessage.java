/*
 * COIN CRDB Rest API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: devops@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package nl.coin.sdk.wm.messages.v1.errorfound;

import com.fasterxml.jackson.annotation.JsonProperty;
import nl.coin.sdk.wm.messages.v1.common.Header;
import nl.coin.sdk.wm.messages.v1.WeasMigrationMessage;

import java.util.Objects;

/**
 * ErrorFoundMessage
 */
public class WeasMigrationErrorFoundMessage implements WeasMigrationMessage {
    @JsonProperty("header")
    private Header header = null;

    @JsonProperty("body")
    private WeasMigrationErrorFoundBody body = null;

    public WeasMigrationErrorFoundMessage header(Header header) {
        this.header = header;
        return this;
    }

    /**
     * Get header
     *
     * @return header
     **/
    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    @Override
    public String getDossierid() {
        return body.getErrorfound().getDossierid();
    }

    public WeasMigrationErrorFoundMessage body(WeasMigrationErrorFoundBody body) {
        this.body = body;
        return this;
    }

    /**
     * Get body
     *
     * @return body
     **/
    public WeasMigrationErrorFoundBody getBody() {
        return body;
    }

    public void setBody(WeasMigrationErrorFoundBody body) {
        this.body = body;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WeasMigrationErrorFoundMessage errorFoundMessage = (WeasMigrationErrorFoundMessage) o;
        return Objects.equals(this.header, errorFoundMessage.header) &&
                Objects.equals(this.body, errorFoundMessage.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(header, body);
    }


    @Override
    public String toString() {

        return "class ErrorFoundMessage {\n" +
            "    header: " + toIndentedString(header) + "\n" +
            "    body: " + toIndentedString(body) + "\n" +
            "}";
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}
